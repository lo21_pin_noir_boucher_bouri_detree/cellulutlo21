# Cellulut LO21

Application développée dans le cadre de l’UV LO21 qui permet de répondre à la problématique de simulation d’automates.

## Sommaire

 - [Pour commencer](#pour-commencer)
 - [Installation](#installation)
 - [Démarrage](#demarrage)
 - [Détails sur l'Application](#détails-sur-lapplication)
 - [](#)
 - [](#)
 - [](#)
 - [](#)
 - [](#)
 - [](#)
 - [](#)
 - [](#)
 - [Auteurs et licence](#auteurs)
## Pour commencer

Avoir une version de **Qt** d'au moins 5.9. Projet principalement testé sur Qt 5.15.2.
Le projet a été testé et fonctionne sur Qt 6, mais il est conseillé d'utiliser une version récente de Qt 5.

Testé avec les compilateurs suivants :
```
g++.exe (tdm64-1) 9.2.0
Copyright (C) 2019 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

### Installation

Dans votre répétoire faire: `$ git clone https://gitlab.utc.fr/lo21_pin_noir_boucher_bouri_detree/cellulutlo21.git`

## Démarrage

Ouvrez le projet avec QtCreator, compiler et executer le programme, puis admirez **vos** automates cellulaires :)
![compiler](./images/2021-06-12_00-42.png )

En cas de soucis, assurez-vous de bien exécuter l'application et non pas les tests :
![pas_exec_tests](./images/pas_exec_tests.PNG)

Pour compiler depuis le terminal, sous Linux, dans le dossier contenant cellulut.pro :
```
$ qmake
$ make
$ src/cellulut
```

## Détails sur l'Application
De prime abord, l’application permet à l’utilisateur de paramétrer intégralement les différents types d’**automates cellulaires** : **taille du réseau** (par réglage des dimensions de la grille), l’ ensemble des états (nombre d’états, nom et couleur associée paramétrable directement par l’interface de l’Alphabet), le **type de voisinage** (Von Neumann, Moore, ou tout simplement arbitraire), ainsi que la règle de transition (Jeu de la vie, totalistique, etc..). L’utilisateur a également une totale liberté sur la nature des frontières de la grille : Elles peuvent être aussi bien périodiques (comme le propose le sujet) ou inertes (ajout supplémentaire).
### Alphabet 
En ce qui concerne l’**Alphabet**, ce dernier permet de paramétrer jusqu’à **32 états** (supérieur aux 8 états du cahier des charges). Le nom et la couleur peuvent aussi être paramétrés. Notre application se veut également accessible à tous : En ce sens, nous avons implémenté un mode Daltonien permettant à ces derniers de pouvoir simuler jusqu’à 8 états sans aucun problème, et dont le contraste des couleurs s’adapte aux personnes souffrant de deutéranomalie.
### Voisinage
Du point de vue du **voisinage** et de la règle de transition, l’utilisateur peut à sa guise utiliser des modèles d’automate cellulaires déjà proposés par l’application, ou bien laisser part à sa créativité en créant ou modifiant des règles déjà établies : Ces nouvelles créations pourront être sauvegardées et utilisées par la suite. Une aide est accessible à l’utilisateur dans le cadre de la création de ces règles.

### Configuration initiale 
Au moment de « jouer » avec la grille, l’application propose à l’utilisateur d’utiliser plusieurs structures déjà implémentées : oscillateurs, langton’s loops, etc. Quelques structures « exotiques » ont pu se glisser dans ces patterns, n’hésitez donc pas à toutes les consulter. L’utilisateur peut également paramétrer l’état initial de la grille de façon manuelle en sélectionnant simplement les cellules à modifier (un maintien du clic gauche permet de modifier rapidement un groupe de cellules).

S’il venait à l’utilisateur de devoir interrompre son chef-d’œuvre artistique, l’application sauvegarde automatiquement les différents changements (qu’ils concernent la grille ou les paramètres).
Enfin, l’utilisateur a la possibilité d’utiliser des configurations déjà établies ou d’en créer, de façon similaire aux règles de transition et de voisinage.

Lorsque la configuration de l’automate est complète, l’utilisateur peut réellement démarrer l’application avec le bouton Play. Il peut à n’importe quel instant remettre à zéro l’intégralité de la simulation, modifier à sa guise la vitesse de simulation (mode automatique), aller directement à l’état suivant, ou revenir à l’état précédent (mode pas à pas)

### Extras 
Et oui l'application fonctionne aussi avec Linux !

Enfin, l’application propose des ajouts supplémentaires beaucoup plus « exotiques ». À l’utilisateur de pouvoir les retrouver ! 

 
## Fabriqué avec

* [Qt](https://www.qt.io/) - Bibliothèque logicielle pour développer des interfaces graphiques
* [QtCreator](https://www.qt.io/product/development-tools) - IDE pour utiliser les bibliothèques spécifiques
* [gifenc](https://github.com/lecram/gifenc) - Bibliothèque de sauvegarde de fichiers GIF sous le domaine public

## Versions
Version 1.1

## Auteurs

* **Yann Boucher** _alias_ [@yboucher](https://gitlab.utc.fr/yboucher)
* **Arthur Detree** _alias_ [@detreear](https://gitlab.utc.fr/detreear)
* **Eugene Pin** _alias_ [@pineugen](https://gitlab.utc.fr/pineugen)
* **Anthony Noir** _alias_ [@noiranth](https://gitlab.utc.fr/noiranth)
* **Merwane Bouri** _alias_ [@bourimer](https://gitlab.utc.fr/bourimer)


## License

Ce projet est sous licence ``MIT License`` - voir le fichier [LICENSE](LICENSE) pour plus d'informations

