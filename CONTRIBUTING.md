# Contributions

## Ajouter une issue

Faire Issues->New Issue dans l'interface de Gitlab, insérer un titre et une description, des labels qualifiant l'issue, la milestone si c'est approprié ainsi qu'une personne assignée à l'issue si cela a été convenu. 
Pour spécifier le temps estimé pour la tâche, créer un commentaire de forme :
```
/estimate <heure>h <minute>m
```
, en remplaçant <heure> et <minute> par le nombre d'heures et de minutes estimées pour la tâche.

## Clore une issue

Cliquer sur 'Close issue', ou faire un commentaire avec `/close`.
Pour renseigner le temps pris par la réalisation de la tâche, faire  un commentaire avec `/spend <heure>h <minute>m`.

## Code applicatif

### Fichiers source
Ajouter les fichiers .cpp dans src/, et rajouter une ligne (si elle n'est pas automatiquement ajoutée par QtCreator)
```
SOURCES += \
    <sources précédentes> \
    <votre_nouveau_fichier>.cpp
```

### Fichiers header
Ajouter les fichiers .hpp dans include/, et rajouter une ligne (si elle n'est pas automatiquement ajoutée par QtCreator)
```
HEADERS += \
    <headers précédents> \
    <votre_nouvel_header>.hpp
```

## Tests

Documentation du module de tests de Qt : [https://doc.qt.io/qt-5/qtest-overview.html](https://doc.qt.io/qt-5/qtest-overview.html)

### Pour rajouter un test :

Dans cellulut_tests.hpp, rajouter une méthode `test_votretest()` parmis les `private slots` de la classe `CellulutTests`.
Dans un fichier votretest.cpp (à rajouter dans la liste de SOURCES de tests.pro), rajouter l'implémentation de cette méthode.
Exemple : 

```cpp
#include <QtTest/QtTest>

#include <algorithm>

#include "cellulut_tests.hpp"

#include "structure.hpp"

void CellulutTests::test_structure()
{
    std::vector<std::pair<Coord, int>> coords;
    std::vector<std::pair<Coord, int>> empty;
    coords.push_back({{1, 2}, 0});
    coords.push_back({{-1, 2}, 1});

    {
        Structure s1(coords.begin(), coords.end());
        Structure s2;
        s2.load(coords.begin(), coords.end());
        QCOMPARE(s1.size(), coords.size());
        QCOMPARE(s2.size(), coords.size());
        QVERIFY(std::equal(coords.begin(), coords.end(), s1.begin()));
        QVERIFY(std::equal(coords.begin(), coords.end(), s2.begin()));

        s1.load(empty.begin(), empty.end());
        QVERIFY(s1.size() == 0);

        int cnt = 0;
        for (auto val : s2)
        {
            QVERIFY(val == coords[cnt]);
            ++cnt;
        }
    }
}

```

### Macros utiles

Pour vérifier qu'une valeur est égale à la valeur attendue : 
`QCOMPARE(val_testee, val_attendue)`

Pour vérifier qu'une expression booléenne est `true`:
`QVERIFIY(<votre_expression>)`