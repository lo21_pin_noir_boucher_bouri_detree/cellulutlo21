/**
\file structurereader.cpp
\date 07/05/2021
\author Yann Boucher
\version 1
\brief StructureReader
**/
#include "structurereader.hpp"

#include <QByteArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QFileInfo>
#include "structure.hpp"
#include <vector>
#include <QTextStream>

char StructureReader::peek() const
{
    if (m_idx >= m_data.size())
        throw StructureReaderException("Invalid file, reach end-of-file unexpectedly.");
    return m_data[m_idx];
}

char StructureReader::read()
{
    char val = peek();
    ++m_idx;
    return val;
}

bool StructureReader::eof() const
{
    return m_idx >= m_data.size();
}

void StructureReader::expect(const std::string &str)
{
    read_white();
    if (m_data.substr(m_idx, str.size()) != str)
        throw StructureReaderException("Expected string : " + str);
    m_idx += str.size();
    read_white();
}

bool StructureReader::accept(const std::string &str)
{
    read_white();
    if (m_data.substr(m_idx, str.size()) == str)
    {
        m_idx += str.size();
        read_white();
        return true;
    }
    return false;
}

std::string StructureReader::read_word()
{
    std::string str;
    while (m_idx < m_data.size() && !isspace(m_data[m_idx]))
        str += m_data[m_idx++];

    return str;
}

std::string StructureReader::read_line()
{
    std::string str;
    while (m_idx < m_data.size() && m_data[m_idx] != '\n')
        str += m_data[m_idx++];
    read_white();

    return str;
}

std::string StructureReader::data_left() const
{
    return m_data.substr(m_idx);
}

int StructureReader::read_int()
{
    read_white();

    size_t cars_read;
    int val;
    try
    {
        val = std::stoi(&m_data.c_str()[m_idx], &cars_read, 10);
    }
    catch (std::exception& e)
    {
        throw StructureReaderException("Expected an integer");
    }

    m_idx += cars_read;
    read_white();
    return val;
}

void StructureReader::read_white()
{
    while (m_idx < m_data.size() && isspace(peek()))
        ++m_idx;
}

// ref : http://golly.sourceforge.net/Help/formats.html#rle
unsigned RLEStructureReader::read_state()
{
    char char_1 = read();
    if (char_1 == 'b' || char_1 == '.')
        return 0;
    else if (char_1 == 'o')
        return 1;
    else if (char_1 >= 'A' && char_1 <= 'X')
        return char_1 - 'A' + 1;
    else if (islower(char_1) && isalpha(char_1) && char_1 >= 'p' && char_1 <= 'q')
    {
        char char_2 = read();
        unsigned offset = (char_1 - 'p')*24 + 49;
        return offset + (char_2 - 'A');
    }
    // unknown chars are zero
    return 0;
}

void RLEStructureReader::read_comment_line(Structure &s)
{
    std::string tag = read_word();
    read_white();
    std::string line = read_line();
    if (tag == "C" || tag == "c")
    {
        s.desc   += line + '\n';
    }
    else if (tag == "N")
    {
        s.title  += line;
    }
    else if (tag == "O")
    {
        s.author += line;
    }
}

// ref : http://golly.sourceforge.net/Help/formats.html#rle
Structure RLEStructureReader::read_structure()
{
    Structure s;

    // ignore first comment lines
    while (accept("#"))
        read_comment_line(s);

    // First Line
    {
        expect("x");
        expect("=");
        int x = read_int();
        expect(",");
        expect("y");
        expect("=");
        int y = read_int();
        std::string rule = "";
        if (accept(",") && accept("rule"))
        {
            expect("=");
            rule = read_word();
        }

        (void)x; (void)y; // ignore warnings
    }

    std::vector<std::pair<Coord, unsigned>> data;
    int x = 0, y = 0;

    while (!eof())
    {
        // comment line
        if (accept("#"))
        {
            read_comment_line(s);
        }
        // end of data
        else if (accept("!"))
        {
            break;
        }
        // structure
        else
        {
            unsigned state = 0;
            unsigned run_count = 1;
            // <run_count> is > 1
            if (isdigit(peek()))
                run_count = read_int();
            // end of structure line
            if (accept("$"))
            {
                x = 0;
                y += run_count;
            }
            else
            {
                state = read_state();

                for (unsigned i = 0; i < run_count; ++i)
                {
                    data.push_back({{x, y}, state});
                    ++x;
                }
            }
        }
    }

    // remove the final '\n' of the text fields
    while (!s.author.empty() && s.author.back() == '\n') s.author.pop_back();
    while (!s.date.empty()   && s.date.back()   == '\n') s.date.pop_back();
    while (!s.title.empty()  && s.title.back()  == '\n') s.title.pop_back();
    while (!s.desc.empty()   && s.desc.back()   == '\n') s.desc.pop_back();

    s.load(data.begin(), data.end());
    return s;
}



Structure JSONStructureReader::read_structure()
{
    std::vector<std::pair<Coord, unsigned>> data;

    QByteArray byteArray = QByteArray::fromStdString(data_left());
    QJsonParseError parseError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(byteArray, &parseError);
    if(parseError.error != QJsonParseError::NoError)
    {
        throw StructureReaderException("JSON parsing error at: " + std::to_string(parseError.offset) + ":" + parseError.errorString().toStdString());
    }

    QJsonObject root = jsonDoc.object();

    Structure s;

    if (!root.contains("cells"))
        throw StructureReaderException("No 'cells' field !");
    if (!root["cells"].isArray())
        throw StructureReaderException("'cells' must be an array.");

    s.author = root.value("author").toString().toStdString();
    s.title = root.value("title").toString().toStdString();
    s.date = root.value("date").toString().toStdString();
    s.desc = root.value("desc").toString().toStdString();

    for (const auto& entry : root["cells"].toArray())
    {
        const char* msg = "Each 'cells' entry must contain fields x, y, and state.";

        if (!entry.isObject())
            throw StructureReaderException(msg);

        QJsonObject val = entry.toObject();
        if (!val.contains("x") || !val.contains("y") || !val.contains("state"))
            throw StructureReaderException(msg);
        if (!val["x"].isDouble() || !val["y"].isDouble() || !val["state"].isDouble())
            throw StructureReaderException(msg);

        data.push_back({{val["x"].toInt(), val["y"].toInt()}, val["state"].toInt()});
    }

    s.load(data.begin(), data.end());
    return s;
}

Structure load_structure(const std::string &path)
{
    QString filename = QString::fromStdString(path);
    QFile f(filename);
    if (!f.open(QFile::ReadOnly | QFile::Text))
        throw StructureReaderException("Unable to open file");
    QTextStream in(&f);

    std::string data = in.readAll().toStdString();

    if (QFileInfo(filename).suffix() == "json")
    {
        JSONStructureReader json(data);
        return json.read_structure();
    }
    else if (QFileInfo(filename).suffix() == "rle")
    {
        RLEStructureReader rle(data);
        return rle.read_structure();
    }
    else
        throw StructureReaderException("Invalid structure file format");
}
