/**
\file totalistictransition.cpp
\date 25/05/2021
\author Yann Boucher
\version 1
\brief TotalisticTransitionRule

Cette classe représente une règle de transition totalistique configurable.
        **/

#include "totalistictransition.hpp"

#include <vector>
#include <algorithm>
#include <sstream>

    REGISTER_FACTORY_ENTRY(TransitionRule, TotalisticTransition, "Totalistic rulestring");

static std::vector<std::string> split(std::string str, std::string token){
    std::vector<std::string>result;
    while(str.size()){
        size_t index = str.find(token);
        if(index != std::string::npos){
            result.push_back(str.substr(0,index));
            str = str.substr(index+token.size());
            if(str.size()==0)result.push_back(str);
        }else{
            result.push_back(str);
            str = "";
        }
    }
    return result;
}

static Interval read_interval( std::string str)
{
    // remove whitespace
    str.erase(std::remove_if(str.begin(), str.end(), ::isspace), str.end());

    Interval interval = Interval{}; // full interval by default

    if (str.size() < 1)
        throw TotalisticRuleException("Invalid rule entry format");
    if (str == "*")
        return Interval{}; // lowest bound, no higher bound
    if (str[0] != '[' || str.back() != ']')
        throw TotalisticRuleException("Invalid rule entry format");

    std::string inside = str.substr(1, str.size()-2);
    auto interval_split = split(inside, "..");

    if (interval_split.empty() || interval_split.size() > 2)
        throw TotalisticRuleException("Invalid rule entry format");
    if (interval_split.size() == 1)
    {
        if (interval_split[0] == "*")
            return Interval{}; // lowest bound, no higher bound
        else
            interval = Interval{interval_split[0], interval_split[0]}; // single-point interval
    }
    else
    {
        if (interval_split[1] == "*")
            interval = Interval{interval_split[0]};
        else
            interval = Interval{interval_split[0], interval_split[1]};
    }

    return interval;
}

TotalisticRuleEntry::TotalisticRuleEntry(std::string rule_string)
{
    std::fill(result_cache_bitmap.begin(), result_cache_bitmap.end(), false);
    m_initial_variable = '\0';

    rule_string.erase(std::remove_if(rule_string.begin(), rule_string.end(), ::isspace), rule_string.end());

    auto arrow_tokens = split(rule_string, "->");
    if (arrow_tokens.size() != 2)
        throw TotalisticRuleException("Invalid rule entry format");

    auto comma_tokens = split(arrow_tokens[0], ",");
    if (comma_tokens.empty())
        throw TotalisticRuleException("Invalid rule entry format");

    if (comma_tokens[0].empty())
        throw TotalisticRuleException("Invalid rule entry format");
    if (isdigit(comma_tokens[0][0]))
    {
        m_initial_state_is_variable = false;
        m_initial_state = std::stol(comma_tokens[0]);
    }
    else
    {
        m_initial_state_is_variable = true;
        m_initial_variable = comma_tokens[0][0];
    }

    for (size_t i = 1; i < comma_tokens.size(); ++i)
    {
        auto colon_tokens = split(comma_tokens[i], ":");
        if (colon_tokens.size() != 2)
            throw TotalisticRuleException("Invalid rule entry format");
        m_constraints.push_back({MathExpr(colon_tokens[0]), read_interval(colon_tokens[1])});
    }

    m_result_state = arrow_tokens[1];
}

bool TotalisticRuleEntry::accept(unsigned initial_state, const Neighborhood &neighborhood, unsigned &next) const
{
    if (!m_initial_state_is_variable && initial_state != m_initial_state)
        return false;

    bool entry_accepted = true;
    for (const auto& constraint : m_constraints)
    {
        if (!constraint.valid(m_initial_variable, initial_state, neighborhood))
        {
            entry_accepted = false;
            break;
        }
    }

    if (entry_accepted)
    {
        if (result_cache_bitmap[initial_state])
        {
            next = result_cache[initial_state];
        }
        else
        {
            if (m_initial_state_is_variable)
                m_result_state.set_var(m_initial_variable, initial_state);

            result_cache_bitmap[initial_state] = true;
            next = result_cache[initial_state] = m_result_state.eval();
        }
        return true;
    }
    else
        return false;
}

unsigned int TotalisticTransition::getState(unsigned int initial, const Neighborhood &neighborhood) const
{
    // Si la rule string n'a pas encore été lue, la lire et générer les TotalistricRuleEntry correspondantes
    if (m_entries.empty())
        generate_entries();

    // test all possibles entries related to the current cell's state
    for (const TotalisticRuleEntry& entry : m_entries)
    {
        unsigned next;
        bool entry_accepted = entry.accept(initial, neighborhood, next);
        if (entry_accepted)
            return next;
    }

    // no match, don't change the state
    return initial;
}

void TotalisticTransition::generate_entries() const
{
    std::istringstream iss(rule_string.str);

    // pour chaque ligne = entrée de la chaîne de la règle :
    for (std::string line; std::getline(iss, line); )
    {
        // on enlève les espaces inutiles
        line.erase(std::remove_if(line.begin(), line.end(), ::isspace), line.end());
        if (!line.empty())
            m_entries.emplace_back(line);
    }
}
