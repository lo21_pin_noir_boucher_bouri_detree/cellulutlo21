#include "lifegametransition.hpp"
#include "neighborhoodrule.hpp"

REGISTER_FACTORY_ENTRY(TransitionRule, LifeGameTransition, "Game of Life");

LifeGameTransition::LifeGameTransition()
{

}

bool LifeGameTransition::acceptFormat(const std::vector<NeighborhoodFormat>& formats) const {
    for(auto format = formats.begin(); format != formats.end(); ++format) {
        if(format->positions.size() != 8) {
            return false;
        }
    }
    return true;
}

unsigned int LifeGameTransition::getState(unsigned int state, const Neighborhood & neighborhood) const {
    unsigned int live = neighborhood.getNb(1);
    if (state == 0)
    {
        return live == 3 ? 1 : 0;
    }
    else
    {
        if (live == 2 || live == 3)
            return 1;
        else
            return 0;
    }
}
