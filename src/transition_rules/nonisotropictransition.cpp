#include "nonisotropictransition.hpp"

#include <sstream>

#include "mathexpr.hpp"

REGISTER_FACTORY_ENTRY(TransitionRule, NonIsotropicTransition, "Non isotropic rulestring");

static std::vector<std::string> split(std::string str, std::string token){
    std::vector<std::string>result;
    while(str.size()){
        size_t index = str.find(token);
        if(index != std::string::npos){
            result.push_back(str.substr(0,index));
            str = str.substr(index+token.size());
            if(str.size()==0)result.push_back(str);
        }else{
            result.push_back(str);
            str = "";
        }
    }
    return result;
}

NonIsotropicRuleEntry::NonIsotropicRuleEntry(string rule_string, bool vertical_symmetry, bool horizontal_symmetry, bool rotate4)
    : m_vertical_sym(vertical_symmetry), m_horizontal_sym(horizontal_symmetry), m_rotate4(rotate4)
{
    rule_string.erase(std::remove_if(rule_string.begin(), rule_string.end(), ::isspace), rule_string.end());

    auto arrow_tokens = split(rule_string, "->");
    if (arrow_tokens.size() != 2)
        throw NonIsotropicRuleException("Invalid rule entry format");

    auto comma_tokens = split(arrow_tokens[0], ",");
    if (comma_tokens.empty())
        throw NonIsotropicRuleException("Invalid rule entry format");

    if (comma_tokens[0].empty())
        throw NonIsotropicRuleException("Invalid rule entry format");
    if (isdigit(comma_tokens[0][0]))
    {
        m_initial_state_is_variable = false;
        m_initial_state = std::stol(comma_tokens[0]);
    }
    else
    {
        m_initial_state_is_variable = true;
        m_initial_variable = comma_tokens[0][0];
    }

    for (size_t i = 1; i < comma_tokens.size(); ++i)
    {
        m_constraints.push_back(std::stol(comma_tokens[i]));
    }

    m_result_state = arrow_tokens[1];
}

bool NonIsotropicRuleEntry::accept(unsigned initial_state, const Neighborhood &neighborhood, unsigned &next) const
{
    if (!m_initial_state_is_variable && initial_state != m_initial_state)
        return false;

    unsigned n_count = 0;
    Neighborhood neighborhoods_to_test[8];
    neighborhoods_to_test[n_count++] = neighborhood;
    if (m_rotate4)
    {
        for (unsigned i = 0; i < 3; ++i)
        {
            neighborhoods_to_test[n_count] = neighborhoods_to_test[n_count-1].rotate90();
            ++n_count;
        }
    }
    if (m_vertical_sym)
        neighborhoods_to_test[n_count++] = neighborhood.flip_vertically();
    if (m_horizontal_sym)
        neighborhoods_to_test[n_count++] = neighborhood.flip_horizontally();
    if (m_vertical_sym && m_horizontal_sym)
        neighborhoods_to_test[n_count++] = neighborhood.flip_vertically().flip_horizontally();

    bool entry_accepted = false;
    for (unsigned k = 0; k < n_count; ++k)
    {
        const auto& n = neighborhoods_to_test[k];

        bool constraint_accepted = true;
        for (unsigned i = 0; i < m_constraints.size(); ++i)
        {
            unsigned neigh_index = i;
            unsigned expected_state = m_constraints[i];

            if (neigh_index >= n.size() ||
                n.neighbor_at_index(neigh_index).second != expected_state)
            {
                constraint_accepted = false;
                break;
            }
        }

        if (constraint_accepted)
        {
            entry_accepted = true;
            break;
        }
    }

    if (entry_accepted)
    {
        if (m_initial_state_is_variable)
            m_result_state.set_var(m_initial_variable, initial_state);
        next = m_result_state.eval();
        //next = eval_math(m_result_state, {{m_initial_variable, initial_state}});
        return true;
    }
    else
        return false;
}

unsigned int NonIsotropicTransition::getState(unsigned int initial, const Neighborhood &neighborhood) const
{
    // Si la rule string n'a pas encore été lue, la lire et générer les TotalistricRuleEntry correspondantes
    if (m_entries.empty())
        generate_entries();

    // test all possibles entries related to the current cell's state
    for (const NonIsotropicRuleEntry& entry : m_entries)
    {
        unsigned next;
        bool entry_accepted = entry.accept(initial, neighborhood, next);
        if (entry_accepted)
            return next;
    }

    // no match, don't change the state
    return initial;
}

void NonIsotropicTransition::generate_entries() const
{
    bool vertical_sym = false;
    bool horizontal_sym = false;
    bool rotate4 = false;

    std::istringstream iss(rule_string.str);

    // pour chaque ligne = entrée de la chaîne de la règle :
    for (std::string line; std::getline(iss, line); )
    {
        // on enlève les espaces inutiles
        line.erase(std::remove_if(line.begin(), line.end(), ::isspace), line.end());
        if (line == "nosym")
        {
            vertical_sym = false;
            horizontal_sym = false;
        }
        else if (line == "norot")
        {
            rotate4 = false;
        }
        else if (line == "rotate4")
            rotate4 = true;
        else if (line == "vsym")
            vertical_sym = true;
        else if (line == "hsym")
            horizontal_sym = true;
        else if (line == "4sym")
        {
            vertical_sym = true;
            horizontal_sym = true;
        }
        else if (!line.empty())
            m_entries.emplace_back(line, vertical_sym, horizontal_sym, rotate4);
    }
}

