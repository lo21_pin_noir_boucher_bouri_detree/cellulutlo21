#include "circulartransition.hpp"

REGISTER_FACTORY_ENTRY(TransitionRule, CircularTransition, "Circular");

unsigned CircularTransition::getState(unsigned cell, const Neighborhood &neighborhood) const
{
    unsigned next_state = (cell+1) % states.val;
    unsigned next_state_neighbors = neighborhood.getNb(next_state);
    if ((int)next_state_neighbors >= threshold.val)
        return next_state;
    else
        return cell;
}
