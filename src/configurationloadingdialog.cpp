#include "configurationloadingdialog.hpp"
#include "ui_configurationloadingdialog.h"

#include <QJsonDocument>
#include <QMessageBox>
#include <QTextStream>

ConfigurationLoadingDialog::ConfigurationLoadingDialog(const QString &current_model, QWidget *parent) :
      QDialog(parent),
      ui(new Ui::ConfigurationLoadingDialog),
      m_current_model(current_model)
{
    ui->setupUi(this);

    load_configurations();


    connect(ui->tree, &QTreeWidget::itemClicked, this, &ConfigurationLoadingDialog::update_info);
    connect(&m_watcher, &QFileSystemWatcher::directoryChanged, this, [this](const QString&)
            {
                load_configurations();
            });
}

ConfigurationLoadingDialog::~ConfigurationLoadingDialog()
{
    delete ui;
}

QJsonObject ConfigurationLoadingDialog::configuration() const
{
    return m_current_configuration;
}

void ConfigurationLoadingDialog::load_configurations()
{
    // clear the previously watched directories
    if (!m_watcher.directories().empty())
        m_watcher.removePaths(m_watcher.directories());

    QTreeWidgetItem* model_list = add_directory_contents(QDir("configurations/"));
    model_list->setText(0, "Configurations");
    ui->tree->clear();
    ui->tree->addTopLevelItem(model_list);
    model_list->setExpanded(true);
}

QTreeWidgetItem *ConfigurationLoadingDialog::add_directory_contents(const QDir &dir)
{
    QTreeWidgetItem* root = new QTreeWidgetItem;

    QFileInfoList dirs = dir.entryInfoList(QStringList(), QDir::Dirs | QDir::NoDotAndDotDot);
    Q_FOREACH (QFileInfo dir, dirs)
    {
        QTreeWidgetItem *child = add_directory_contents(QDir(dir.absoluteFilePath()));
        // empty directory
        if (child->childCount() == 0)
            continue;

        child->setText(0, dir.fileName());
        root->addChild(child);
    }

    QFileInfoList files = dir.entryInfoList(QStringList() << "*.json", QDir::Files);
    Q_FOREACH (QFileInfo file, files)
    {
        QJsonObject obj = load_configuration(file.absoluteFilePath());
        // Ignore configurations not associated with the current model
        if (!ui->include_other_models->isChecked() && obj.value("model") != m_current_model)
            continue;

        QTreeWidgetItem *child = new QTreeWidgetItem();
        child->setText(0, file.fileName());
        // data is the complete filepath of the structure, store it so it can be easily accessed when an item is selected
        child->setData(0, Qt::UserRole, file.absoluteFilePath());

        root->addChild(child);
    }

    // register this directory for filesystem watching
    m_watcher.addPath(dir.absolutePath());

    return root;
}

void ConfigurationLoadingDialog::update_info(QTreeWidgetItem *item, int)
{
    if (item->data(0, Qt::UserRole).isNull())
        return;

    const QJsonObject root = load_configuration(item->data(0, Qt::UserRole).toString());

    ui->title->setText(root.value("title").toString());
    ui->model->setText(root.value("model").toString());
    unsigned width = root.value("width").toInt();
    unsigned height = root.value("height").toInt();
    ui->size->setText(QString::number(width) + "x" + QString::number(height));

    m_current_configuration = root;
}

void ConfigurationLoadingDialog::done(int r)
{
    if(QDialog::Accepted == r)  // ok was pressed
    {
        if(!m_current_configuration.empty() && ui->tree->selectedItems().size() != 1)   // validate the data somehow
        {
            QMessageBox::information(this, "Error", "It is required to select a configuration.");
            return;
        }
        else
        {
            QDialog::done(r);
            return;
        }
    }
    else    // cancel, close or exc was pressed
    {
        QDialog::done(r);
        return;
    }
}

QJsonObject ConfigurationLoadingDialog::load_configuration(const QString &filename)
{
    QFile f(filename);
    if (!f.open(QFile::ReadOnly | QFile::Text))
        throw ConfigurationLoadingException("Error reading file");

    QTextStream in(&f);

    QJsonParseError parseError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(in.readAll().toUtf8(), &parseError);
    if (parseError.error != QJsonParseError::NoError)
    {
        throw ConfigurationLoadingException("JSON parsing error at: " + std::to_string(parseError.offset) + ":" + parseError.errorString().toStdString());
    }

    const QJsonObject root = jsonDoc.object();
    return root;
}

void ConfigurationLoadingDialog::on_include_other_models_stateChanged(int)
{
    load_configurations();
}
