/**
\file gridview.cpp
\date 28/04/2021
\author Yann Boucher
\version 1
\brief GridView

Cette classe représente le widget utilisé pour l'affichage et l'interaction avec la grille actuelle.
        **/


#include "gridview.hpp"

#include <QEvent>
#include <QMouseEvent>
#include <QGraphicsSceneHoverEvent>
#include <QApplication>
#include <QGridLayout>
#include <QToolTip>
#include <QLabel>
#include <QMimeData>
#include <QStyleOptionGraphicsItem>

#include <QDebug>

#include <vector>
#include <cstdlib>
#include <cassert>

#include <qmath.h>

#include "structurereader.hpp"

namespace detail
{
Graphics_view_zoom::Graphics_view_zoom(QGraphicsView* view)
    : QObject(view), _view(view)
{
    _view->viewport()->installEventFilter(this);
    _view->setMouseTracking(true);
    _zoom_factor_base = 1.0015;
}

void Graphics_view_zoom::gentle_zoom(double factor) {
    _view->scale(factor, factor);
    _view->centerOn(target_scene_pos);
    QPointF delta_viewport_pos = target_viewport_pos - QPointF(_view->viewport()->width() / 2.0,
                                                               _view->viewport()->height() / 2.0);
    QPointF viewport_center = _view->mapFromScene(target_scene_pos) - delta_viewport_pos;
    _view->centerOn(_view->mapToScene(viewport_center.toPoint()));
    emit zoomed();
}

void Graphics_view_zoom::set_zoom_factor_base(double value) {
    _zoom_factor_base = value;
}

bool Graphics_view_zoom::eventFilter(QObject *object, QEvent *event) {
    if (event->type() == QEvent::MouseMove) {
        QMouseEvent* mouse_event = static_cast<QMouseEvent*>(event);
        QPointF delta = target_viewport_pos - mouse_event->pos();
        if (qAbs(delta.x()) > 5 || qAbs(delta.y()) > 5) {
            target_viewport_pos = mouse_event->pos();
            target_scene_pos = _view->mapToScene(mouse_event->pos());
        }
    } else if (event->type() == QEvent::Wheel) {
        QWheelEvent* wheel_event = static_cast<QWheelEvent*>(event);
        if (wheel_event->angleDelta().y() != 0) {
            double angle = wheel_event->angleDelta().y();
            double factor = qPow(_zoom_factor_base, angle);
            gentle_zoom(factor);
            return true;
        }
    }
    Q_UNUSED(object)
    return false;
}

}

class DragDropHandlerItem : public QGraphicsRectItem
{
public:
    DragDropHandlerItem(GridView& in_gridview, QGraphicsItem *parent = nullptr)
        : QGraphicsRectItem(0, 0, 1, 1, parent), m_grid_view(in_gridview)
    {
        setAcceptDrops(true);

        setPen(Qt::NoPen);
        setBrush(Qt::NoBrush);
    }

protected:
    void dragEnterEvent(QGraphicsSceneDragDropEvent *event) override
    {
        if (event->mimeData()->hasText())
        {
            event->acceptProposedAction();
            update();
        }
    }

    void dropEvent(QGraphicsSceneDragDropEvent *event) override
    {
        event->acceptProposedAction();

        QString file_path = event->mimeData()->text();
        Structure s;
        try
        {
            s = load_structure(file_path.toStdString());
        }
        catch (const StructureReaderException& e)
        {
            //qDebug() << "StructureReaderException : " << e.what() << "\n";
            return;
        }

        Coord coord = Coord{(int)event->pos().x(), (int)event->pos().y()};
        m_grid_view.paste_structure_at(coord, s);
    }

private:
    QString m_state_name;
    GridView& m_grid_view;
};

/**
 * \class GridGraphicsView
 *
 * Classe héritée de GraphicsView permettant les manipulations à la souris et de drag-and-drop.
 * Détail d'implémentation.
 */
class GridGraphicsView : public QGraphicsView
{
public:
    GridGraphicsView(GridView& gridview, QWidget* parent)
        : QGraphicsView(parent), m_gridview(gridview)
    {
        setAcceptDrops(true);
    }

protected:
    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void drawForeground(QPainter * painter, const QRectF & rect);
    void drawBackground(QPainter * painter, const QRectF & rect);

private:
    void draw_bresenham(QPointF from, QPointF to);

private:
    GridView& m_gridview;
    QPoint m_last_mouse_pos;
};


void GridGraphicsView::mousePressEvent(QMouseEvent *event)
{
    QPointF item_pos = mapToScene(event->pos());
    if (!sceneRect().contains(item_pos))
        return;

    Coord coord = Coord{(int)item_pos.x(), (int)item_pos.y()};

    m_last_mouse_pos = event->pos();

    if (QGuiApplication::keyboardModifiers() == Qt::NoModifier && event->button() == Qt::LeftButton)
    {
        m_gridview.push_history();
        m_gridview.click_on(coord);
        m_gridview.update_gridview();
    }
    else if (event->button() == Qt::RightButton)
    {
        if ((QGuiApplication::keyboardModifiers() & Qt::ControlModifier) == 0)
            m_gridview.clear_selection();
        QGraphicsView::mousePressEvent(event);
    }
    else
        QGraphicsView::mousePressEvent(event);

    m_gridview.update_current_mouse_pos(coord);
}

void GridGraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    QPointF item_pos = mapToScene(event->pos());
    if (!sceneRect().contains(item_pos) || !sceneRect().contains(mapToScene(m_last_mouse_pos)))
        return;

    Coord coord = Coord{(int)item_pos.x(), (int)item_pos.y()};

    if (!m_gridview.in_toggle_mode() && QGuiApplication::keyboardModifiers() == Qt::NoModifier && event->buttons() == Qt::LeftButton)
    {
        draw_bresenham(mapToScene(m_last_mouse_pos), item_pos);
        m_gridview.update_gridview();
    }
    else if (event->buttons() == Qt::RightButton)
    {
        QGraphicsView::mouseMoveEvent(event);
        //item->setSelected(true);
    }
    else
        QGraphicsView::mouseMoveEvent(event);

    m_last_mouse_pos = event->pos();
    m_gridview.update_current_mouse_pos(coord);
}

void GridGraphicsView::drawForeground(QPainter *painter, const QRectF &)
{
    qreal lod = QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform());

    if (lod < 10)
        return; // trop zoomé, on ne dessine rien

    painter->setPen(QPen(Qt::gray, 0)); // cosmetic 1-pixel pen

    QVector<QLine> lines;
    // horizontal lines
    for (int i = -1; i < m_gridview.grid_size().height(); ++i)
    {
        const unsigned y_offset = (i+1);
        lines.append(QLine(QPoint(0, y_offset), QPoint(m_gridview.grid_size().width(), y_offset)));
    }
    // vertical lines
    for (int j = -1; j < m_gridview.grid_size().width(); ++j)
    {
        const unsigned x_offset = (j+1);
        lines.append(QLine(QPoint(x_offset, 0), QPoint(x_offset, m_gridview.grid_size().height())));
    }

    painter->drawLines(lines);

    painter->setPen(Qt::NoPen);
}

void GridGraphicsView::drawBackground(QPainter *painter, const QRectF &rect)
{
    QGraphicsView::drawBackground(painter, rect);

    //painter->drawPixmap(0, 0, m_gridview.grid_image());
    painter->drawImage(0, 0, m_gridview.grid_image());
}

// Algorithme de Bresenham pour tracer une ligne entre les deux points et dessiner sur ces items
void GridGraphicsView::draw_bresenham(QPointF from, QPointF to)
{
    int x0 = from.x();
    int y0 = from.y();
    int x1 = to.x();
    int y1 = to.y();

    // cf. https://gist.github.com/bert/1085538
    int dx =  abs (x1 - x0), sx = x0 < x1 ? 1 : -1;
    int dy = -abs (y1 - y0), sy = y0 < y1 ? 1 : -1;
    int err = dx + dy, e2; /* error value e_xy */

    for (;;){  /* loop */

        m_gridview.set_cell_state(Coord{x0, y0}, m_gridview.current_pen());

        if (x0 == x1 && y0 == y1) break;
        e2 = 2 * err;
        if (e2 >= dy) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
        if (e2 <= dx) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
    }
}

GridView::GridView(QWidget *parent)
    : QFrame(parent), m_undo_history(10),  m_grid(10, 10), m_in_toggle_mode(false), m_handling_rubberband(false), m_width(10), m_height(10)
{
    //setMouseTracking(true);

    setFrameStyle(Sunken | StyledPanel);
    setAcceptDrops(true);

    m_scene = new QGraphicsScene(this);

    m_view = new GridGraphicsView(*this, this);
    m_view->setRenderHint(QPainter::Antialiasing, false);
    m_view->setDragMode(QGraphicsView::RubberBandDrag);
    m_view->setOptimizationFlags(QGraphicsView::DontSavePainterState|QGraphicsView::DontAdjustForAntialiasing);
    m_view->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
    m_view->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    m_view->setScene(m_scene);
    m_view->viewport()->setCursor(Qt::ArrowCursor);
    m_view->setBackgroundBrush(QBrush(Qt::gray));
    m_view->setRubberBandSelectionMode(Qt::IntersectsItemBoundingRect); // provides better performance
    m_view->scale(20, 20); // Begin with 20x20 pixels cells

    m_zoom = new detail::Graphics_view_zoom(m_view);

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(m_view);
    m_info_section = new QWidget(this);

    QVBoxLayout* info_layout = new QVBoxLayout;
    m_mouse_pos_label = new QLabel("");
    info_layout->addWidget(m_mouse_pos_label);
    info_layout->addWidget(new QLabel("Left click : edit; Right click : select; F11 for fullscreen mode", this));
    info_layout->addWidget(new QLabel("Hold SHIFT to move across the grid; Scroll wheel or CTRL+/CTRL- to zoom", this));
    info_layout->setContentsMargins(0,0,0,0);
    m_info_section->setLayout(info_layout);

    layout->addWidget(m_info_section);
    setLayout(layout);

    m_drag_drop_handler = new DragDropHandlerItem(*this);
    m_scene->addItem(m_drag_drop_handler);

    // Initialisation la première fois
    m_height = 10;
    m_width = 10;

    Grid default_grid(m_height, m_width);
    load_grid(default_grid);

    set_current_pen(0);
    // Test Alphabet
    Alphabet alph(state{stateColor{255, 255, 255}, "Dead"});
    alph.newEtat(state{stateColor{0, 0, 255}, "Alive"});
    set_alphabet(alph);
    update_current_mouse_pos({0, 0});

    connect(m_zoom, &detail::Graphics_view_zoom::zoomed, this, [this]
            {
                emit zoom_changed(cell_screen_size());
            });
    connect(m_view, &QGraphicsView::rubberBandChanged, this, &GridView::handle_rubberband);
}

void GridView::set_alphabet(const Alphabet &alph)
{
    while (!m_undo_history.isEmpty())
        m_undo_history.popGrid();

    m_alph = alph;
    // Default to using the first non-null state as the current pen
    set_current_pen(1);
    // Reload the current grid to update the colors
    load_grid(get_grid());
    update_gridview();
}

const Alphabet &GridView::alphabet() const
{
    return m_alph;
}

void GridView::set_current_pen(unsigned state)
{
    state %= m_alph.taille();
    m_pen = state;
}

unsigned GridView::current_pen() const
{
    return m_pen;
}

void GridView::enable_toggle_mode(unsigned checked_state, unsigned unchecked_state)
{
    m_in_toggle_mode = true;
    add_toggle_rule(checked_state, unchecked_state);
}

void GridView::add_toggle_rule(unsigned checked_state, unsigned unchecked_state)
{
    m_toggle_states.push_back({checked_state, unchecked_state});
}

void GridView::disable_toggle_mode()
{
    m_in_toggle_mode = false;
}

void GridView::set_clipboard(const Structure &s)
{
    m_copy_paste_buffer = s;
}

unsigned GridView::cell_screen_size() const
{
    return m_view->transform().mapRect(QRectF(0,0,1,1)).size().toSize().width();
}

QSize GridView::grid_size() const
{
    return QSize(m_width, m_height);
}

void GridView::copy_grid(const Grid &grid)
{
    push_history();
    load_grid(grid);
    update_gridview();
}

const Grid& GridView::get_grid() const
{
    return m_grid;
}

void GridView::clear_selection()
{
    for (auto rect : m_selection_rects)
    {
        m_scene->removeItem(rect);
    }
    m_selection_rects.clear();
    m_handling_rubberband = false;
}

Structure GridView::selected_cells() const
{
    std::vector<std::pair<Coord, unsigned>> cells;

    for (const auto& item : m_selection_rects)
    {
        const auto& rect = item->rect().toRect();
        for (int i = rect.top(); i <= rect.bottom(); ++i)
            for (int j = rect.left(); j <= rect.right(); ++j)
            {
                cells.push_back({Coord{j, i}, m_grid.get_state(Coord{j, i})});
            }
    }
    return Structure(cells.begin(), cells.end());
}

void GridView::push_history()
{
    m_undo_history.pushGrid(get_grid());
}

void GridView::undo()
{
    if (!m_undo_history.isEmpty())
    {
        load_grid(m_undo_history.popGrid());
    }
    update_gridview();
}

void GridView::load_grid(const Grid &grid)
{
    m_height = grid.get_rows();
    m_width = grid.get_col();
    m_grid = grid;

    m_image_data.resize(m_width*m_height);
    for (int i = 0; i < (int)m_height; ++i)
    {
        for (int j = 0; j < (int)m_width; ++j)
        {
            unsigned state_id = grid.get_state(Coord{j, i});
            if (state_id < m_alph.taille())
            {
                const auto& state_color = m_alph.getState(state_id).getColor();
                m_image_data[i * m_width + j] = 0xff000000 | ((uint32_t)state_color.getRed() << 16)
                                                | ((uint32_t)state_color.getGreen() << 8)
                                                | ((uint32_t)state_color.getBlue() << 0);
            }
            else
            {
                const auto& state_color = m_alph.getState(state_id % m_alph.taille()).getColor();
                m_image_data[i * m_width + j] = 0xff000000 | ((uint32_t)state_color.getRed() << 16)
                                                | ((uint32_t)state_color.getGreen() << 8)
                                                | ((uint32_t)state_color.getBlue() << 0);
                m_grid.set_cell(Coord{j, i}, state_id % m_alph.taille());
            }
        }
    }

    m_grid_image = QImage((const uchar*)m_image_data.data(), m_width, m_height, QImage::Format_ARGB32);
    m_scene->setSceneRect(QRectF(0, 0, m_width, m_height));
    m_drag_drop_handler->setRect(QRectF(0, 0, m_width, m_height));
    update_current_mouse_pos(m_last_mouse_pos);
}

void GridView::set_cell_state(Coord pos, unsigned state)
{
    state %= m_alph.taille();
    auto state_color = m_alph.getState(state).getColor();
    m_image_data[pos.y * m_width + pos.x] = 0xff000000 | ((uint32_t)state_color.getRed() << 16)
                                            | ((uint32_t)state_color.getGreen() << 8)
                                            | ((uint32_t)state_color.getBlue() << 0);
    m_grid.set_cell(pos, state);
}

void GridView::update_gridview()
{
    m_grid_image = QImage((const uchar*)m_image_data.data(), m_width, m_height, QImage::Format_ARGB32);
    m_scene->update();
}

QGraphicsRectItem *GridView::create_selection_rect(const QRectF &rect)
{
    QGraphicsRectItem* item = new QGraphicsRectItem();
    item->setPen(Qt::NoPen);
    item->setBrush(QBrush(QColor(220, 220, 220, 200)));
    item->setRect(rect);
    m_selection_rects.push_back(item);
    m_scene->addItem(item);

    return item;
}

void GridView::copy_selection()
{
    m_copy_paste_buffer = selected_cells();
}

void GridView::paste_clipboard()
{
    push_history();

    auto scene_pos = m_view->mapToScene(m_view->mapFromGlobal(QCursor::pos()));
    if (!m_scene->sceneRect().contains(scene_pos))
        return;

    Coord origin = {scene_pos.toPoint().x(), scene_pos.toPoint().y()};

    paste_structure_at(origin, m_copy_paste_buffer);
}

void GridView::fill_selection(unsigned state)
{
    push_history();

    for (const auto& item : m_selection_rects)
    {
        const auto& rect = item->rect().toRect();
        for (int i = rect.top(); i <= rect.bottom(); ++i)
            for (int j = rect.left(); j <= rect.right(); ++j)
            {
                set_cell_state(Coord{(int)j, (int)i}, state);
            }
    }

    update_gridview();
}

void GridView::delete_selection()
{
    fill_selection(0);
    clear_selection();
}

void GridView::select_all()
{
    clear_selection();
    create_selection_rect(QRect(0, 0, m_width, m_height));
}

void GridView::click_on(Coord coord)
{
    if (!in_toggle_mode())
    {
        set_cell_state(coord, current_pen());
    }
    else
    {
        unsigned prev_state = get_grid().get_state(coord);
        for (auto toggle_pair : m_toggle_states)
        {
            if (prev_state == toggle_pair.first)
            {
                set_cell_state(coord, toggle_pair.second);
                break;
            }
            else if (prev_state == toggle_pair.second)
            {
                set_cell_state(coord, toggle_pair.first);
                break;
            }
        }
    }
}

void GridView::update_current_mouse_pos(Coord coord)
{
    m_last_mouse_pos = coord;
    auto state_label = m_alph.getState(m_grid.get_state(coord)).getStateLabel();
    m_mouse_pos_label->setText(QString("Mouse position : %1,%2 : \"%3\"").arg(coord.x).arg(coord.y).arg(
        QString::fromStdString(state_label)));
}

void GridView::paste_structure_at(Coord origin, const Structure &s)
{
    push_history();

    for (const auto& cell : s)
    {
        Coord corrected = wrap_coords(cell.first + origin, m_width, m_height);

        set_cell_state(corrected, cell.second);
    }
    update_gridview();
}

const QImage &GridView::grid_image() const
{
    return m_grid_image;
}

void GridView::enter_fullscreen()
{
    m_info_section->hide();
    layout()->removeWidget(this);
    setParent(nullptr);
    raise();
    showFullScreen();
}

void GridView::handle_rubberband(QRect rubberBandRect, QPointF fromScenePoint, QPointF toScenePoint)
{
    QGraphicsRectItem* item;
    if (!m_handling_rubberband)
    {
        m_handling_rubberband = true;
        item = create_selection_rect();
    }
    else
    {
        assert(!m_selection_rects.empty());
        item = m_selection_rects.back();
    }

    // end of event
    if (rubberBandRect.isNull())
    {
        m_handling_rubberband = false;

        return;
    }

    // assign the integer coordinates
    item->setRect(QRectF(fromScenePoint.toPoint(), toScenePoint.toPoint()).normalized());
}


void GridView::keyPressEvent(QKeyEvent *event)
{
    if (event->modifiers() & Qt::ShiftModifier)
    {
        m_view->setDragMode(QGraphicsView::ScrollHandDrag);
    }

    // copier-couper-coller
    if (event->modifiers() & Qt::ControlModifier)
    {
        if (event->key() == Qt::Key_C)
        {
            copy_selection();
        }
        else if (event->key() == Qt::Key_X)
        {
            copy_selection();
            delete_selection();
        }
        else if (event->key() == Qt::Key_V)
        {
            paste_clipboard();
        }
        else if (event->key() == Qt::Key_Z)
        {
            undo();
        }
        else if (event->key() == Qt::Key_A)
        {
            select_all();
        }
        else if (event->key() == Qt::Key_Plus)
        {
            m_zoom->gentle_zoom(1.25);
        }
        else if (event->key() == Qt::Key_Minus)
        {
            m_zoom->gentle_zoom(0.75);
        }
    }


    // suppression de sélection
    if (event->key() == Qt::Key_Backspace || event->key() == Qt::Key_Delete)
    {
        delete_selection();
    }

    // Exit fullscreen ?
    if (parent() == nullptr)
    {
        if (event->key() == Qt::Key_F11 || event->key() == Qt::Key_Escape)
        {
            event->accept();
            m_info_section->show();
            emit exit_fullscreen();
        }
    }
    else
    {
        return QFrame::keyPressEvent(event);
    }
}

void GridView::keyReleaseEvent(QKeyEvent *event)
{
    (void)event;
    if (QGuiApplication::keyboardModifiers() & Qt::ShiftModifier)
    {
        m_view->setDragMode(QGraphicsView::RubberBandDrag);
    }

    return QFrame::keyReleaseEvent(event);
}

