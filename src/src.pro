QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4):
QT += widgets
qtHaveModule(multimedia): DEFINES += HAS_MULTIMEDIA
qtHaveModule(multimedia): QT += multimedia


CONFIG += c++14
TEMPLATE = app
TARGET = cellulut

RC_ICONS = ../images/glider.ico

# Optimisations, flag pour garder les infos de debug
QMAKE_CXXFLAGS += -O3 -g

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += ../include ../include/transition_rules ../include/neighborhood_rules

SOURCES += \
    colorlabel.cpp \
    alphabet.cpp \
    mathexpr.cpp \
    neighborhood_rules/arbitraryneighborhoodrule.cpp \
    neighborhood_rules/margolusNeighborhoodRule.cpp \
    automaton.cpp \
    gridview.cpp \
    neighborhoodDialog.cpp \
    savingdialog.cpp \
    transition_rules/circulartransition.cpp \
    transition_rules/lifegametransition.cpp \
    main.cpp \
    neighborhood_rules/mooreNeighborhoodRule.cpp \
    propertyvisitors.cpp \
    neighborhood.cpp \
    simulation.cpp \
    structurereader.cpp \
    interface.cpp \
    grid.cpp \
    history.cpp \
    gif/gifenc.c \
    neighborhood_rules/vonNeumannNeighborhoodRule.cpp \
    structurewriter.cpp \
    structurelibraryview.cpp \
    modelloadingdialog.cpp \
    configurationloadingdialog.cpp \
    transition_rules/totalistictransition.cpp \
    transition_rules/nonisotropictransition.cpp \
    uibuildervisitor.cpp


HEADERS += \
    ../include/colorlabel.hpp \
    ../include/automaton.hpp \
    ../include/coord.hpp \
    ../include/neighborhood_rules/mooreNeighborhoodRule.hpp \
    ../include/neighborhood_rules/margolusNeighborhoodRule.hpp \
    ../include/savingdialog.hpp \
    ../include/transition_rules/lifegametransition.hpp \
    ../include/neighborhoodrule.hpp \
    ../include/neighborhood_rules/arbitraryneighborhoodrule.hpp \
    ../include/neighborhood_rules/mooreNeighborhoodRule.hpp \
    ../include/simulation.hpp \
    ../include/neighborhood_rules/vonNeumannNeighborhoodRule.hpp \
    ../include/property.hpp \
    ../include/propertyvisitors.hpp \
    ../include/structure.hpp \
    ../include/neighborhood.hpp \
    ../include/factory.hpp \
    ../include/structurereader.hpp \
    ../include/structurewriter.hpp \
    ../include/gridview.hpp \
    ../include/interface.hpp \
    ../include/grid.hpp \
    ../include/alphabet.hpp \
    ../include/stateColor.hpp \
    ../include/state.hpp \
    ../include/history.hpp \
    ../include/constantes.hpp \
    ../include/structurelibraryview.hpp \
    ../include/transitionrule.hpp \
    ../include/transition_rules/circulartransition.hpp \
    ../include/transition_rules/totalistictransition.hpp \
    ../include/modelloadingdialog.hpp \
    ../include/neighborhoodDialog.hpp \
    ../include/configurationloadingdialog.hpp \
    ../include/transition_rules/nonisotropictransition.hpp

FORMS += \
    ../forms/colorlabel.ui \
    ../forms/interface.ui \
    ../forms/savingdialog.ui \
    ../forms/structurelibraryview.ui \
    ../forms/modelloadingdialog.ui \
    ../forms/neighborhoodDialog.ui \
    ../forms/configurationloadingdialog.ui


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

CONFIG(debug, debug|release) {
    DEBUG_OR_RELEASE = debug
}  else {
    DEBUG_OR_RELEASE = release
}

# On copie les fichiers de pattern, de modèle, etc... vers le dossier d'exécution
# https://evileg.com/en/post/476/
CONFIG += file_copies
COPIES += data

data.files = $$files($$PWD/../patterns) $$files($$PWD/../models) $$files($$PWD/../configurations) $$files($$PWD/../extras)
data.path = $$OUT_PWD
