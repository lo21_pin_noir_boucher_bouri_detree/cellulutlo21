/**
\file alphabet.cpp
\date 01/05/2021
\author Arthur Detree
\version 1
\brief Alphabet
        **/

#include "alphabet.hpp"
#include "state.hpp"
#include "stateColor.hpp"

void Alphabet::newEtat(const state& s){ // Définition de la méthode de création d'état
    etats.push_back(s);
}

const state &Alphabet::getState(unsigned int it) const{ // Définition de la méthode de récupération d'un état en fonction de son identifiant
    if (it < etats.size())
    {
        return etats[it];
    }
    else
        throw AlphabetException("Invalid state ID");
}

void Alphabet::setState(unsigned int it, const state &s){ // Méthode permettant de modifier un identifiant d'un état. On se doit de vérifier si l'identifiant que veut donner n'a pas déjà été attribué
    if (it < etats.size())
    {
        etats[it] = s;
    }
    else
        throw AlphabetException("Invalid state ID");
}

