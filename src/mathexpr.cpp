/**
\file mathexpr.cpp
\date 25/05/2021
\author Yann Boucher
\version 1
\brief MathExpr

Ce fichier contient une fonction permettant d'exécuter une expression mathématique à partir d'une chaîne de caractères et d'une liste de variables.
        **/

// Basé sur l'algorithme du Shunting Yard, suivi d'une évaluation de l'expression sous notation polonaise inversée résultante.

#include "mathexpr.hpp"

#include <stack>
#include <vector>
#include <algorithm>
#include <cctype>

    using detail::rpl_token_t;

//! Priorité de l'opérateur
static int precedence(char op)
{
    if (op == '*' || op == '/' || op == '%')
        return 3;
    else if (op == '+' || op == '-')
        return 2;
    else
        return 1;
}

//! Evalue une opération binaire entre x et y, dénotée par op.
static int eval_int_binop(char op, int x, int y)
{
    switch (op)
    {
        case '+':
            return x+y;
        case '-':
            return x-y;
        case '*':
            return x*y;
        case '/':
            if (y == 0)
                throw MathExprException("Divsion by zero error");
            return x/y;
        case '%':
            if (y == 0)
                throw MathExprException("Divsion by zero error");
            return x%y;
        default:
            throw MathExprException("Unknown operator");
    }
}

#define MAX_DATA_STACK_SIZE 256
//! Evalue une expression RPL
static int evaluate_rpl_input(const std::vector<rpl_token_t>& rpl_stack, const std::array<int8_t, 26>& variable_map)
{
    static std::array<int, MAX_DATA_STACK_SIZE> data_stack;
    unsigned stack_len = 0;

    for (unsigned i = 0; i < rpl_stack.size(); ++i)
    {
        if (stack_len+1 >= MAX_DATA_STACK_SIZE)
            throw MathExprException("Internal error: Exceeded the maximum amount of RPL tokens");

        if (rpl_stack[i].type == rpl_token_t::LITERAL)
            data_stack[stack_len++] = rpl_stack[i].value;
        else if (rpl_stack[i].type == rpl_token_t::VARIABLE)
            data_stack[stack_len++] = variable_map[rpl_stack[i].var_alphabet_idx];
        else
        {
            int op = rpl_stack[i].op;
            if (stack_len <= 1)
                throw MathExprException("Invalid math expression error");

            int x = data_stack[stack_len-2];
            int y = data_stack[stack_len-1];
            int result = eval_int_binop(op, x, y);
            --stack_len;
            data_stack[stack_len-1] = result;

        }
    }

    if (stack_len != 1)
        throw MathExprException("Invalid math expression error");

    return data_stack[0];
}

std::vector<rpl_token_t> parse_math(const std::string& in_expr)
{
    std::string expr = in_expr;
    // remove whitespace out of the equation altogether
    expr.erase(std::remove_if(expr.begin(), expr.end(), ::isspace), expr.end());

    std::vector<rpl_token_t> rpl_stack;
    std::stack<char> op_stack;

    rpl_token_t rpl_token;
    rpl_token.type = rpl_token_t::LITERAL; rpl_token.value = 0; // silence warnings

    char* end_ptr;
    unsigned idx = 0;
    while (idx < expr.size())
    {
        switch (expr[idx])
        {
            case '0'...'9':
            {
                int value = std::strtol(&expr.c_str()[idx], &end_ptr, 10);
                int read_len = end_ptr - &expr.c_str()[idx];
                idx += read_len;

                rpl_token.type = rpl_token_t::LITERAL;
                rpl_token.value = value;
                rpl_stack.push_back(rpl_token);
                break;
            }
            case '-':
            case '+':
            case '*':
            case '/':
            case '%':
                // binary operator
                {
                    while  (!op_stack.empty() &&
                           (op_stack.top() != '(') &&
                           (precedence(op_stack.top()) > precedence(expr[idx])))
                    {
                        rpl_token.type = rpl_token_t::OP;
                        rpl_token.op = op_stack.top();
                        rpl_stack.push_back(rpl_token);
                        op_stack.pop();
                    }
                    op_stack.push(expr[idx]);
                    ++idx;
                }
                break;
            case '(':
                op_stack.push('(');
                ++idx;
                break;
            case ')':
                while (!op_stack.empty() &&
                       op_stack.top() != '(')
                {
                    rpl_token.type = rpl_token_t::OP;
                    rpl_token.op = op_stack.top();
                    rpl_stack.push_back(rpl_token);
                    op_stack.pop();
                }

                if (op_stack.empty())
                    throw MathExprException("Parenthesis mismatch");
                op_stack.pop();

                ++idx;
                break;
            default:
                // assume it's a variable name then
                {
                    char var_name = expr[idx++];
                    if (var_name < 'a' || var_name > 'z')
                        throw MathExprException("Invalid variable name");

                    rpl_token.type = rpl_token_t::VARIABLE;
                    rpl_token.var_alphabet_idx = var_name - 'a';
                    rpl_stack.push_back(rpl_token);
                    break;
                }
        }
    }
    // while there are remaining operators on the op stack
    while (!op_stack.empty())
    {
        rpl_token.type = rpl_token_t::OP;
        rpl_token.op   = op_stack.top();
        op_stack.pop();
        rpl_stack.push_back(rpl_token);
    }

    return rpl_stack;
}

int eval_math(const std::string &in_expr, const std::map<char, int> &variables)
{
    auto rpl_stack = parse_math(in_expr);

    std::array<int8_t, 26> variable_map;
    for (const auto& pair : variables)
    {
        if (pair.first < 'a' || pair.first > 'z')
            throw MathExprException("Invalid variable name");
        variable_map[pair.first - 'a'] = pair.second;
    }
    return evaluate_rpl_input(rpl_stack, variable_map);
}

MathExpr::MathExpr(const std::string &expr)
{
    is_constant = false;

    rpl_stack = parse_math(expr);
    // Constant expression, cache the result
    if (std::find_if(expr.begin(), expr.end(), ::isalpha) == expr.end())
    {
        is_constant = true;
        cached_result = evaluate_rpl_input(rpl_stack, variable_map);
    }
}

int MathExpr::eval() const
{
    if (is_constant)
        return cached_result;
    else
    {
        int result = evaluate_rpl_input(rpl_stack, variable_map);
        return result;
    }
}
