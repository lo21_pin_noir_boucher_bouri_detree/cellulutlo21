/**
\file structurelibraryview.cpp
\author Yann Boucher
\version 1
\brief StructureLibraryView

Widget de la bibliothèque de structures.
        **/
#include "structurelibraryview.hpp"
#include "ui_structurelibraryview.h"

#include "structurereader.hpp"

#include <QFile>
#include <QTextStream>
#include <QPixmap>
#include <QImage>
#include <QDrag>
#include <QMimeData>

#include <QDebug>

StructureLibraryView::StructureLibraryView(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::StructureLibraryView)
{
    ui->setupUi(this);

    load_structures();

    connect(ui->tree, &QTreeWidget::itemClicked, this, &StructureLibraryView::update_info);
    connect(ui->tree, &QTreeWidget::itemDoubleClicked, this, &StructureLibraryView::create_drag);
    connect(ui->button_copy, &QPushButton::pressed, this, &StructureLibraryView::copy_button_clicked);
    connect(&m_watcher, &QFileSystemWatcher::directoryChanged, this, [this](const QString&)
    {
        load_structures();
    });
}

StructureLibraryView::~StructureLibraryView()
{
    delete ui;
}

void StructureLibraryView::update_cell_pixel_size(unsigned size)
{
    m_cell_pixel_size = size;
}

void StructureLibraryView::load_structures()
{
    // clear the previously watched directories
    if (!m_watcher.directories().empty())
        m_watcher.removePaths(m_watcher.directories());

    QTreeWidgetItem* pattern_list = add_directory_contents(QDir("patterns/"));
    pattern_list->setText(0, "Patterns");
    ui->tree->clear();
    ui->tree->addTopLevelItem(pattern_list);
    pattern_list->setExpanded(true);
}

QTreeWidgetItem *StructureLibraryView::add_directory_contents(const QDir &dir)
{
    QTreeWidgetItem* root = new QTreeWidgetItem;

    QFileInfoList dirs = dir.entryInfoList(QStringList(), QDir::Dirs | QDir::NoDotAndDotDot);
    Q_FOREACH (QFileInfo dir, dirs)
    {
        QTreeWidgetItem *child = add_directory_contents(QDir(dir.absoluteFilePath()));
        // empty directory
        if (child->childCount() == 0)
            continue;

        child->setText(0, dir.fileName());
        root->addChild(child);
    }

    QFileInfoList files = dir.entryInfoList(QStringList() << "*.rle" << "*.json", QDir::Files);
    Q_FOREACH (QFileInfo file, files)
    {
        Structure s;
        bool valid = try_load_structure(file.absoluteFilePath(), s);
        if (!valid)
            continue;

        QTreeWidgetItem *child = new QTreeWidgetItem();
        child->setText(0, file.fileName());
        // data is the complete filepath of the structure, store it so it can be easily accessed when an item is selected
        child->setData(0, Qt::UserRole, file.absoluteFilePath());

        root->addChild(child);
    }

    // register this directory for filesystem watching
    m_watcher.addPath(dir.absolutePath());

    return root;
}

bool StructureLibraryView::try_load_structure(const QString &filename, Structure &s)
{
    try
    {
        s = load_structure(filename.toStdString());
        return true;
    }
    catch (const StructureReaderException& e)
    {
        //qDebug() << "StructureReaderException : " << e.what() << "\n";
        return false;
    }
}

void StructureLibraryView::update_preview(const Structure &s)
{
    QPixmap pix;

    /** to check wether load ok */
    if(pix.convertFromImage(create_preview_image(s, false))){
        /** scale pixmap to fit in label'size and keep ratio of pixmap */
        pix = pix.scaled(ui->preview->size() - 2*QSize(ui->preview->lineWidth(), ui->preview->lineWidth()),Qt::KeepAspectRatio);
        ui->preview->setPixmap(pix);
    }
}

QImage StructureLibraryView::create_preview_image(const Structure &s, bool transparent)
{
    QImage img(s.width(), s.height(), QImage::Format_ARGB32);
    if (transparent)
        img.fill(Qt::transparent);
    else
        img.fill(Qt::white);
    for (const auto& cell : s)
    {
        stateColor s_c = m_alph.getState(cell.second % m_alph.taille()).getColor();
        img.setPixelColor(cell.first.x, cell.first.y, QColor(s_c.getRed(), s_c.getGreen(), s_c.getBlue()));
    }

    return img;
}

void StructureLibraryView::update_info(QTreeWidgetItem *item, int column)
{
    (void)column;

    Structure s;
    if (!try_load_structure(item->data(0, Qt::UserRole).toString(), s))
    {
        ui->button_copy->setEnabled(false);
        return;
    }
    if (!s.title.empty())
        ui->struct_title->setText(QString::fromStdString(s.title));
    else
    {
        QString filename = QFileInfo(item->data(0, Qt::UserRole).toString()).baseName();
        if (!filename.isEmpty()) // failsafe, in case it actually is empty
            filename.replace(0, 1, filename[0].toUpper());
        ui->struct_title->setText(filename);
    }
    ui->struct_author->setText(QString::fromStdString(s.author));
    ui->struct_date->setText(QString::fromStdString(s.date));
    ui->struct_desc->setPlainText(QString::fromStdString(s.desc));
    ui->struct_size->setText(QString::number(s.width()) + "x" + QString::number(s.height()));

    update_preview(s);

    ui->button_copy->setEnabled(true);
}

void StructureLibraryView::copy_button_clicked()
{
    if (ui->tree->selectedItems().size() == 0)
        return;

    QTreeWidgetItem* item = ui->tree->selectedItems()[0];

    Structure s;
    if (!try_load_structure(item->data(0, Qt::UserRole).toString(), s))
        return;

    emit structure_copied(s);
}

void StructureLibraryView::create_drag(QTreeWidgetItem *item, int column)
{
    (void)column;

    Structure s;
    if (!try_load_structure(item->data(0, Qt::UserRole).toString(), s))
    {
        ui->button_copy->setEnabled(false);
        return;
    }

    QDrag* drag = new QDrag(this);
    QMimeData* mimeData = new QMimeData;

    QString filename = QFileInfo(item->data(0, Qt::UserRole).toString()).absoluteFilePath();
    QPixmap pix;
    QImage img = create_preview_image(s, true);
    pix.convertFromImage(img);


    QSize target_size;
    // set target size so that a single cell has the same size as the grid's cells
    target_size = QSize(s.width(), s.height()) * m_cell_pixel_size;
    // Have a maximum size to avoid drawing huge megastructures all over the screen
    const int max_size = 1200;
    if (target_size.width() >= max_size)
        target_size.setWidth(max_size);
    if (target_size.height() >= max_size)
        target_size.setHeight(max_size);
    pix = pix.scaled(target_size, Qt::KeepAspectRatio);

    mimeData->setText(filename);
    drag->setMimeData(mimeData);
    drag->setPixmap(pix);
    drag->setHotSpot(QPoint(m_cell_pixel_size/2, m_cell_pixel_size/2));

    drag->exec(Qt::CopyAction);
}
