#include "modelloadingdialog.hpp"
#include "ui_modelloadingdialog.h"

#include <QJsonDocument>
#include <QMessageBox>
#include <QTextStream>

ModelLoadingDialog::ModelLoadingDialog(QWidget *parent) :
      QDialog(parent),
      ui(new Ui::ModelLoadingDialog)
{
    ui->setupUi(this);

    load_models();


    connect(ui->tree, &QTreeWidget::itemClicked, this, &ModelLoadingDialog::update_info);
    connect(&m_watcher, &QFileSystemWatcher::directoryChanged, this, [this](const QString&)
            {
                load_models();
            });
}

ModelLoadingDialog::~ModelLoadingDialog()
{
    delete ui;
}

QJsonObject ModelLoadingDialog::model() const
{
    return m_current_model;
}

void ModelLoadingDialog::load_models()
{
    // clear the previously watched directories
    if (!m_watcher.directories().empty())
        m_watcher.removePaths(m_watcher.directories());

    QTreeWidgetItem* model_list = add_directory_contents(QDir("models/"));
    model_list->setText(0, "Models");
    ui->tree->clear();
    ui->tree->addTopLevelItem(model_list);
    model_list->setExpanded(true);
}

QTreeWidgetItem *ModelLoadingDialog::add_directory_contents(const QDir &dir)
{
    QTreeWidgetItem* root = new QTreeWidgetItem;

    QFileInfoList dirs = dir.entryInfoList(QStringList(), QDir::Dirs | QDir::NoDotAndDotDot);
    Q_FOREACH (QFileInfo dir, dirs)
    {
        QTreeWidgetItem *child = add_directory_contents(QDir(dir.absoluteFilePath()));
        // empty directory
        if (child->childCount() == 0)
            continue;

        child->setText(0, dir.fileName());
        root->addChild(child);
    }

    QFileInfoList files = dir.entryInfoList(QStringList() << "*.json", QDir::Files);
    Q_FOREACH (QFileInfo file, files)
    {
        QTreeWidgetItem *child = new QTreeWidgetItem();
        child->setText(0, file.fileName());
        // data is the complete filepath of the structure, store it so it can be easily accessed when an item is selected
        child->setData(0, Qt::UserRole, file.absoluteFilePath());

        root->addChild(child);
    }

    // register this directory for filesystem watching
    m_watcher.addPath(dir.absolutePath());

    return root;
}

void ModelLoadingDialog::update_info(QTreeWidgetItem *item, int column)
{
    (void)column;

    if (item->data(0, Qt::UserRole).isNull())
        return;

    QFile f(item->data(0, Qt::UserRole).toString());
    if (!f.open(QFile::ReadOnly | QFile::Text))
        throw ModelLoadingException("Unable to read file");
    QTextStream in(&f);

    QJsonParseError parseError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(in.readAll().toUtf8(), &parseError);
    if(parseError.error != QJsonParseError::NoError)
    {
        throw ModelLoadingException("JSON parsing error at: " + std::to_string(parseError.offset) + ":" + parseError.errorString().toStdString());
    }

    const QJsonObject root = jsonDoc.object();

    ui->title->setText(root.value("title").toString());
    ui->desc->setPlainText(root.value("desc").toString());
    ui->date->setText(root.value("date").toString());
    ui->author->setText(root.value("author").toString());

    m_current_model = root;
}

void ModelLoadingDialog::done(int r)
{
    if(QDialog::Accepted == r)  // ok was pressed
    {
        if(!m_current_model.empty() && ui->tree->selectedItems().size() != 1)   // validate the data somehow
        {
            QMessageBox::information(this, "Error", "It is required to select a model.");
            return;
        }
        else
        {
            QDialog::done(r);
            return;
        }
    }
    else    // cancel, close or exc was pressed
    {
        QDialog::done(r);
        return;
    }
}
