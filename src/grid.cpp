/**
\file grid.cpp
\date 24/04/2021
\author Merwane Bouri
\version 1
\brief Grid

Cette classe représente un réseau de cellules.

        **/

#include "grid.hpp"

#include "structure.hpp"

Grid::Grid(size_t l,size_t c):nb_rows(l),nb_col(c){
    this->matrix.resize(l*c);
    policy = BoundaryPolicy::Periodic;
}

ostream& operator<<(ostream& f, const Grid& g){
    if(g.get_col() != 0 && g.get_rows() != 0){
        for(size_t i=0;i<g.get_rows();i++){
            for(size_t j=0;j<g.get_col();j++){
                Coord pos;
                pos.x = i;
                pos.y = j;
                f<<" "<<g.get_state(pos);
            }
            f<<std::endl;
        }
    }
    return f;
}


Grid& Grid::operator=(const Grid& g){
    if(this ==&g)
        return *this;

    this->nb_rows=g.get_rows();
    this->nb_col=g.get_col();
    this->matrix = g.matrix;
    return *this;
}

bool Grid::operator==(const Grid & G) const {
    return nb_rows==G.nb_rows && nb_col==G.nb_col && matrix==G.matrix;
}

Structure Grid::to_structure() const
{
    std::vector<std::pair<Coord, unsigned>> data;
    for (size_t i = 0; i < get_rows(); ++i)
        for (size_t j = 0; j < get_col(); ++j) {
            Coord pos;
            pos.x = j;
            pos.y = i;
            data.push_back(std::make_pair(Coord{(int)j, (int)i}, get_state(pos)));
        }


    return Structure{data.begin(), data.end()};
}
