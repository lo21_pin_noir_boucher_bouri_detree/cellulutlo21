#include "colorlabel.hpp"
#include "ui_colorlabel.h"
#include "state.hpp"
#include "stateColor.hpp"
#include "alphabet.hpp"
#include "interface.hpp"
#include <stdlib.h>
#include <string>
#include <cassert>
#include <QListWidget>
#include <QColorDialog>
#include <QMessageBox>

#include "constantes.hpp"

ColorLabel::ColorLabel(const Alphabet &in_a, QWidget *parent) :
      QDialog(parent),
      ui(new Ui::ColorLabel)
{
    ui->setupUi(this);

    ui->color_label->setAutoFillBackground(true);

    connect(ui->generate_button, SIGNAL(clicked()), this, SLOT(generateState()));
    connect(ui->random_button, SIGNAL(clicked()), this, SLOT(randomGenerator()));
    connect(ui->daltonian_button, SIGNAL(clicked()), this, SLOT(daltonianMode())); // Pas sûr pour cette ligne, mon but ici est d'aller chercher la valeur
    // présente dans nbrStatesComboBox, c'est à dire la valeur du nombre d'états que l'on peut modifier sur l'interface interface.ui
    connect(ui->reset_button, SIGNAL(clicked()), this, SLOT(resetMode()));
    connect(ui->remove_button, SIGNAL(clicked()), this, SLOT(removeState()));
    connect(ui->pick_button, SIGNAL(clicked()), this, SLOT(pickColor()));
    connect(ui->state_list, &QListWidget::currentRowChanged, this, [this](int id)
            {
                current_id = id;
                if (id >= 0)
                    loadStateID(id);
            });

    a = in_a;
    current_id = 0;
    ui->state_list->clear();
    for (unsigned i = 0; i < a.taille(); ++i)
        ui->state_list->addItem(QString::fromStdString(a.getState(i).getStateLabel()));
    loadStateID(0);
}

ColorLabel::~ColorLabel()
{
    delete ui;
}

void ColorLabel::generateState(){
    if(a.taille() >= MAX_ETATS) // On vérifie qu'il reste encore de la place dans l'alphabet
    {
        QMessageBox::warning(this, "Error", "Reached the maximal amount of states");
    }
    else {
        std::string string_name_state = ui->state_label->text().toStdString(); // On passe notre QString en string pour pouvoir utiliser le constructeur
        state newstate = state(stateColor(0,0,0),string_name_state); // Création de notre nouvel état
        a.newEtat(newstate); // Ajout du nouvel état dans l'alphabet
        ui->state_list->addItem(ui->state_label->text()); // Ajout du nouvel état dans la liste
    }
}


void ColorLabel::randomGenerator(){
    unsigned int r = rand() % 256; // On génère aléatoirement nos couleurs
    unsigned int g = rand() % 256;
    unsigned int b = rand() % 256;

    a.setState(current_id, state(stateColor(r, g, b), a.getState(current_id).getStateLabel()));

    QPalette pal = ui->color_label->palette();
    pal.setColor(QPalette::Window, QColor(r, g, b));
    ui->color_label->setPalette(pal);
}


void ColorLabel::daltonianMode(){
    ui->state_list->clear(); // On vide toujours la liste quand on active le mode Daltonien
    a.clearAlphabet();

    // Création des états à partir du constructeur
    state black = state(stateColor(0,0,0),"Black");
    state white = state(stateColor(255,255,255),"White");
    state blue = state(stateColor(0,0,255),"Blue");
    state pink = state(stateColor(255,0,255),"Pink");
    state red = state(stateColor(255,0,0),"Red");
    state green = state(stateColor(0,255,0),"Green");
    state orange = state(stateColor(255,127,0),"Orange");
    state grey = state(stateColor(127,127,127),"Grey");

    std::vector<state> daltonianstates;
    daltonianstates.resize(8, state{stateColor(0, 0, 0)});
    daltonianstates[0]=black;
    daltonianstates[1]=white;
    daltonianstates[2]=blue;
    daltonianstates[3]=pink;
    daltonianstates[4]=red;
    daltonianstates[5]=green;
    daltonianstates[6]=orange;
    daltonianstates[7]=grey;

    for(unsigned int i=0; i< daltonianstates.size() ; i++){ // Boucle qui crée le nombre d'états en fonction du nombre désiré
        a.newEtat(daltonianstates[i]); // Crée et ajoute l'état dans l'alphabet
        ui->state_list->addItem(QString::fromStdString(daltonianstates[i].getStateLabel())); // Ajoute l'état dans la liste
    }
}

void ColorLabel::resetMode(){
    ui->state_list->clear(); // On vide la liste
    ui->state_list->addItem("Default state");
    a.clearAlphabet();
    a.newEtat(state{stateColor{255, 255, 255}}); //On recrée toujours l'état par défaut
}

void ColorLabel::removeState(){
    if (a.taille() > 1)
    {
        ui->state_list->takeItem(a.taille()-1); // On retire le dernier élément ajouté
        a.deleleLastState();
    }
}

void ColorLabel::loadStateID(unsigned int current_id){
    assert(current_id < a.taille());

    state state = a.getState(current_id);
    ui->state_label->setText(QString::fromStdString(state.getStateLabel())); // On modifie le state_label en fonction du nom de l'état

    QColor color = QColor(state.getColor().getRed(), state.getColor().getGreen(), state.getColor().getBlue());
    QPalette pal = ui->color_label->palette();
    pal.setColor(QPalette::Window, color);
    ui->color_label->setPalette(pal);
}

void ColorLabel::pickColor()
{
    QColor color = QColorDialog::getColor();
    a.setState(current_id, state(stateColor(color.red(), color.green(), color.blue()), a.getState(current_id).getStateLabel()));

    QPalette pal = ui->color_label->palette();
    pal.setColor(QPalette::Window, color);
    ui->color_label->setPalette(pal);
}

void ColorLabel::on_state_label_textEdited(const QString &arg1)
{
    state s = a.getState(current_id);
    a.setState(current_id, state(s.getColor(), arg1.toStdString()));
    ui->state_list->item(current_id)->setText(arg1);
}
