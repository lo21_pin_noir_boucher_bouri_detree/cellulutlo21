/**
\file history.h
\date 24/04/2021
\author Merwane Bouri
\version 1
\brief History

Cette classe représente un historique de Grids.

        **/

#include "history.hpp"

#include <cassert>

History::History(unsigned int nbM){
    nbMax=nbM;
}

void History::pushGrid(const Grid &g){
    if(tab.size()==nbMax){
        tab.pop_front();
        tab.push_back(g);
    }
    else{
        tab.push_back(g);
    }
}

Grid History::popGrid(){
    if(tab.size()>0){
        Grid tmp = tab.back();
        tab.pop_back();
        return tmp;
    }
    throw HistoryException("Empty stack");
}
