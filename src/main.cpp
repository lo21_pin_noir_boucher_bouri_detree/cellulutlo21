#include <QApplication>

#include "interface.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    try
    {
        MainWindow m;
        m.show();

        return a.exec();
    }
    catch (const std::exception& e)
    {
        QMessageBox::critical(nullptr, "Exception", e.what());
        return -1;
    }
}
