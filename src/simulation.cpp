#include "simulation.hpp"

Simulation::Simulation() : canRun(false), time(0), period(0), automaton(), startGrid(0,0), hist(10) {}

Simulation::~Simulation() {}

void Simulation::setNeighborhoodRule(NeighborhoodRule* NR) {
    automaton.setNeighborhoodRule(NR);
    canRun = automaton.getNeighborhoodRule()&&automaton.getTransitionRule()&&automaton.getTransitionRule()->acceptFormat(automaton.getNeighborhoodRule()->getFormats());
}

void Simulation::setTransitionRule(TransitionRule* TR) {
    automaton.setTransitionRule(TR);
    canRun = automaton.getNeighborhoodRule()&&automaton.getTransitionRule()&&automaton.getTransitionRule()->acceptFormat(automaton.getNeighborhoodRule()->getFormats());
}

void Simulation::setAlphabet(const Alphabet& A) {
    automaton.setAlphabet(A);
    for(int i=0; i<static_cast<int>(startGrid.get_rows()); ++i) {
        for(int j=0; j<static_cast<int>(startGrid.get_col()); ++j) {
            startGrid.set_cell({i,j}, 0);
        }
    }
}

const Grid& Simulation::getGrid() const {
    return automaton.getGrid();
}

void Simulation::setGrid(const Grid & grid) {
    if(!time) {
        startGrid = grid;
        time = 0;
        hist = History(hist.get_nbMax());
    } else if((grid.get_col() != automaton.getGrid().get_col())&&(grid.get_rows() != automaton.getGrid().get_rows())) {
        return;
    } else if(!(automaton.getGrid() == grid)) { //S'il y a modification manuelle de la grille pendant la simulation
        period = -1;
    }
    automaton.setGrid(grid);
}

/*void Simulation::setCell(const Coord& coord, unsigned int val) {
    automaton.setCell(coord, val);
    startGrid.set_cell(coord, val);
}

void Simulation::resize(size_t l,size_t c) {
    Grid newGrid(l, c);
    int min = static_cast<int>(startGrid.get_rows()<=l?startGrid.get_rows():l);
    int max = static_cast<int>(startGrid.get_col()<=c?startGrid.get_col():c);

    for(int i=0; i<min; ++i) {
        for(int j=0; j<max; ++j) {
            newGrid.set_cell({i,j}, startGrid.get_state({i,j}));
        }
    }

    startGrid = newGrid;
    reset();
}*/

    void Simulation::reset() {
    automaton.setGrid(startGrid);
    time = 0;
    period = 0;
    hist = History(hist.get_nbMax());
}

void Simulation::step() {
    if(!canRun) {
        return;
    }
    ++time;
    hist.pushGrid(automaton.getGrid());

    automaton.runOnce();

    automaton.getNeighborhoodRule()->update_generation(time);
    if(period==0 && startGrid == automaton.getGrid()) {
        period = time;
    }
}

bool Simulation::back() {
    bool r=false;
    if(!hist.isEmpty()) {
        --time;
        automaton.setGrid(hist.topGrid());
        automaton.getNeighborhoodRule()->update_generation(time);
        hist.popGrid();
        r=true;
    }
    return r;
}
