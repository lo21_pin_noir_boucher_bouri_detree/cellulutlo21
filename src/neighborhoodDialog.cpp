#include "neighborhoodDialog.hpp"
#include "ui_neighborhoodDialog.h"

#include <QDate>
#include <QMessageBox>

NeighborhoodDialog::NeighborhoodDialog(Neighborhood& n, QWidget *parent) :
      QDialog(parent), ui(new Ui::NeighborhoodDialog)
{
    ui->setupUi(this);
    GridView& gv = *(*ui).grid_view;
    Alphabet a;

    // Création des états à partir du constructeur
    state* black = new state(stateColor(0,0,0),"Black");
    state* blue = new state(stateColor(0,0,255),"Blue");
    state* grey = new state(stateColor(127,127,127),"Grey");

    // Ajout des états dans l'Alphabet
    a.newEtat(*black);
    a.newEtat(*blue);
    a.newEtat(*grey);

    gv.set_alphabet(a);

    // Initialisation taille de la grille
    int xMax = 11;
    int yMax = 11;
    int nbrVoisins = n.size();

    for(int i = 0; i < nbrVoisins; i++) {
        int currentX = n.neighbor_at_index(i).first.x;
        int currentY = n.neighbor_at_index(i).first.y;
        if(abs(currentX) >= xMax /2) xMax = (abs(currentX)+1) * 2;
        if(abs(currentY) >= yMax /2) yMax = (abs(currentY)+1) * 2;
    }

    ui->widthSpinBox_2->setValue(xMax);
    ui->heightSpinBox_2->setValue(yMax);
    ResizeCreateGrid(xMax, yMax, n);
    gv.enable_toggle_mode(1, 0);
    gv.add_toggle_rule(3, 2);
}

void NeighborhoodDialog::ResizeCreateGrid(int x, int y, Neighborhood& n)
{
    GridView& gv = *(*ui).grid_view;
    int nbrVoisins = n.size();

    Grid newGrid(y, x);
    currentPoint = {x/ 2, y / 2};
    newGrid.set_cell(currentPoint, 2);
    for(int i = 0; i < nbrVoisins; i++) {
        Coord newNeighborRelative = n.neighbor_at_index(i).first;
        if(newNeighborRelative == Coord {0, 0}) {
            newGrid.set_cell(currentPoint, 3);
        }
        else {
            Coord newNeighborAbsolute = {currentPoint.x + newNeighborRelative.x, currentPoint.y + newNeighborRelative.y};
            if( (newNeighborAbsolute.x < x && newNeighborAbsolute.y < y) &&
                    (newNeighborAbsolute.x >= 0 && newNeighborAbsolute.y >= 0)) {
                newGrid.set_cell(newNeighborAbsolute, 1);
            }
        }
    }
    gv.copy_grid(newGrid);
}

NeighborhoodDialog::~NeighborhoodDialog()
{
    delete ui;
}

void NeighborhoodDialog::done(int r)
{
    QDialog::done(r);
    return;
}

Neighborhood* NeighborhoodDialog::getNeighborhood() const
{
    Neighborhood* newNeighborhood = new Neighborhood;
    Grid currentGrid = ui->grid_view->get_grid();
    QSize currentSizeGrid = ui->grid_view->grid_size();
    int height = currentSizeGrid.height();
    int width = currentSizeGrid.width();
    for(int j = 0; j < height ; j++) {
        for(int i = 0; i < width ; i++) {
            if( currentGrid.get_state({i,j}) == 1 || currentGrid.get_state({i,j}) == 3) {
                Coord newCoord = {i - currentPoint.x, j - currentPoint.y};
                newNeighborhood->addNeighbor(newCoord, 1);
                fflush(stdout);
            }
        }
    }
    return newNeighborhood;
}

void NeighborhoodDialog::on_validateGridDim_2_clicked()
{
    Grid oldGrid = ui->grid_view->get_grid();

    unsigned int nbrRow = ui->heightSpinBox_2->value(); // nbr de lignes => axe y
    unsigned int nbrCol = ui->widthSpinBox_2->value(); // nbr de colonne => axe x
    Grid newGrid(nbrCol, nbrRow);

    Neighborhood* oldNeighborhood = this->getNeighborhood();

    ResizeCreateGrid(nbrCol, nbrRow, *oldNeighborhood);

    ui->validateGridDim_2->setEnabled(false);

    delete oldNeighborhood;
}

void NeighborhoodDialog::on_heightSpinBox_2_valueChanged(int)
{
    ui->validateGridDim_2->setEnabled(true);
}

void NeighborhoodDialog::on_widthSpinBox_2_valueChanged(int)
{
    ui->validateGridDim_2->setEnabled(true);
}
