#include "neighborhood.hpp"

Neighborhood Neighborhood::flip_vertically() const
{
    Neighborhood n;
    for (unsigned i = 0; i < size(); ++i)
    {
        const auto& val = neighborPositions[i];
        n.addNeighbor(Coord{val.pos_x, -val.pos_y}, val.state);
    }
    return n;
}

Neighborhood Neighborhood::flip_horizontally() const
{
    Neighborhood n;
    for (unsigned i = 0; i < size(); ++i)
    {
        const auto& val = neighborPositions[i];
        n.addNeighbor(Coord{-val.pos_x, val.pos_y}, val.state);
    }
    return n;
}

Neighborhood Neighborhood::rotate90() const
{
    Neighborhood n = *this;
    for (unsigned i = 0; i < neighbors; ++i)
    {
        n.neighborPositions[i].pos_x = -this->neighborPositions[i].pos_y;
        n.neighborPositions[i].pos_y = this->neighborPositions[i].pos_x;
    }

    return n;
}

