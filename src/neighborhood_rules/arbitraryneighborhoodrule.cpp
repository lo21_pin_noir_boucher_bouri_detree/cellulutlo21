/**
\file arbitraryneighborhoodrule.cpp
\date 11/05/2021
\author Yann Boucher
\version 1
\brief ArbitraryNeighborhoodRule

Représente un voisinage arbitraire, défini par l'utilisateur.
                                            **/

#include "arbitraryneighborhoodrule.hpp"

#include "neighborhood.hpp"
#include "grid.hpp"

REGISTER_FACTORY_ENTRY(NeighborhoodRule, ArbitraryNeighborhoodRule, "Arbitrary");

Neighborhood ArbitraryNeighborhoodRule::getNeighborhood(const Grid &grid, Coord pos) const
{
    Neighborhood n;

    for (const auto& item : neighbors.contents)
    {
        CoordinateProperty coord = static_cast<CoordinateProperty&>(*item);
        n.addNeighbor(coord.c + pos, grid.get_state(coord.c + pos));
    }

    return n;
}

std::vector<NeighborhoodFormat> ArbitraryNeighborhoodRule::getFormats() const
{
    NeighborhoodFormat format;
    for (const auto& item : neighbors.contents)
    {
        CoordinateProperty coord = static_cast<CoordinateProperty&>(*item);
        format.positions.push_back(coord.c);
    }

    return {format};
}
