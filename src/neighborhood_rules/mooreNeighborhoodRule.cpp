#include "mooreNeighborhoodRule.hpp"

REGISTER_FACTORY_ENTRY(NeighborhoodRule, mooreNeighborhoodRule, "Moore");

mooreNeighborhoodRule::mooreNeighborhoodRule(int _radius)
{
    current_format_radius = radius.val = _radius;

    if(_radius == 0)
        throw NeighborhoodException("Radius can't be zero");

    update_format();
}

Neighborhood mooreNeighborhoodRule::getNeighborhood(const Grid& grid, Coord pos) const
{
    if (current_format_radius != radius.val)
        update_format();

    Neighborhood newNeighborhood;

    // fast path
    if (radius.val == 1)
    {
        newNeighborhood.addNeighbor(pos + Coord{-1, -1}, grid.get_state(pos + Coord{-1, -1}));
        newNeighborhood.addNeighbor(pos + Coord{0, -1}, grid.get_state(pos + Coord{0, -1}));
        newNeighborhood.addNeighbor(pos + Coord{+1, -1}, grid.get_state(pos + Coord{+1, -1}));

        newNeighborhood.addNeighbor(pos + Coord{-1, 0}, grid.get_state(pos + Coord{-1, 0}));
        newNeighborhood.addNeighbor(pos + Coord{+1, 0}, grid.get_state(pos + Coord{+1, 0}));

        newNeighborhood.addNeighbor(pos + Coord{-1, +1}, grid.get_state(pos + Coord{-1, +1}));
        newNeighborhood.addNeighbor(pos + Coord{0, +1}, grid.get_state(pos + Coord{0, +1}));
        newNeighborhood.addNeighbor(pos + Coord{+1, +1}, grid.get_state(pos + Coord{+1, +1}));

    }
    else
    {
        // Coordonnées des voisins dans la grille
        Coord gridCoord;
        std::vector<Coord>::const_iterator it = format.positions.begin();
        for (it = format.positions.begin() ; it != format.positions.end(); ++it) {
            // Calcul des coordonnées du voisin sur la grille
            gridCoord.x = pos.x + it->x;
            gridCoord.y = pos.y + it->y;
            // Ajout du nouveau voisin <Coordonnée_relative, état>
            newNeighborhood.addNeighbor(*it , grid.get_state(gridCoord));
        }
    }


    return newNeighborhood;
}

std::vector<NeighborhoodFormat> mooreNeighborhoodRule::getFormats() const
{
    if (current_format_radius != radius.val)
        update_format();

    std::vector<NeighborhoodFormat> vector;
    vector.push_back(format);
    return vector;
}

void mooreNeighborhoodRule::update_format() const
{
    format.positions.clear();

    // Les coordonnées sont copiées dans le vecteur
    // N(i,j) = {(k, l)|abs(k−i) ≤ r et abs(l−j) ≤ r} ;
    Coord newCord;
    for(int i = -radius.val; i <= radius.val; i++){
        for(int j = -radius.val; j <= radius.val; j++){
            newCord.x = i;
            newCord.y = j;
            if(!(newCord.x == 0 && newCord.y == 0)) {
                format.positions.push_back(newCord);
            }
        }
    }

    current_format_radius = radius.val;
}

