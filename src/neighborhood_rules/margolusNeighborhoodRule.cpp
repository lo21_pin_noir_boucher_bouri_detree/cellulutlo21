#include "margolusNeighborhoodRule.hpp"

REGISTER_FACTORY_ENTRY(NeighborhoodRule, MargolusNeighborhoodRule, "Margolus");

MargolusNeighborhoodRule::MargolusNeighborhoodRule() :
      parity(0)
{
    formats.resize(2);

    formats[0].positions.push_back({-1, -1});
    formats[0].positions.push_back({-1, 0});
    formats[0].positions.push_back({0, -1});

    NeighborhoodFormat format_2;
    formats[1].positions.push_back({+1, +1});
    formats[1].positions.push_back({+1, 0});
    formats[1].positions.push_back({0, +1});
}

Neighborhood MargolusNeighborhoodRule::getNeighborhood(const Grid& grid, Coord pos) const
{
    Neighborhood newNeighborhood;
    // Coordonnées des voisins dans la grille
    Coord gridCoord;
    const NeighborhoodFormat& format = formats[parity];
    std::vector<Coord>::const_iterator it = format.positions.begin();
    for (it = format.positions.begin() ; it != format.positions.end(); ++it) {
        // Calcul des coordonnées du voisin sur la grille
        gridCoord.x = pos.x + it->x;
        gridCoord.y = pos.y + it->y;
        // Ajout du nouveau voisin <Coordonnée_relative, état>
        newNeighborhood.addNeighbor(*it , grid.get_state(gridCoord));
    }
    return newNeighborhood;
}

std::vector<NeighborhoodFormat> MargolusNeighborhoodRule::getFormats() const
{
    return formats;
}
