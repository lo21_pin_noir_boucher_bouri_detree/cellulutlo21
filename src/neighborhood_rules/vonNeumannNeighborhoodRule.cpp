#include "vonNeumannNeighborhoodRule.hpp"

REGISTER_FACTORY_ENTRY(NeighborhoodRule, vonNeumannNeighborhoodRule, "Von Neumann");

vonNeumannNeighborhoodRule::vonNeumannNeighborhoodRule(int _radius)
{
    current_format_radius = radius.val = _radius;

    if(_radius == 0)
        throw NeighborhoodException("Radius can't be zero");

    update_format();
}

Neighborhood vonNeumannNeighborhoodRule::getNeighborhood(const Grid& grid, Coord pos) const
{
    if (current_format_radius != radius.val)
        update_format();

    Neighborhood newNeighborhood;
    // Coordonnées des voisins dans la grille
    Coord gridCoord;
    std::vector<Coord>::const_iterator it = format.positions.begin();
    for (it = format.positions.begin() ; it != format.positions.end(); ++it) {
        // Calcul des coordonnées du voisin sur la grille
        gridCoord.x = pos.x + it->x;
        gridCoord.y = pos.y + it->y;
        // Ajout du nouveau voisin <Coordonnée_relative, état>
        newNeighborhood.addNeighbor(*it , grid.get_state(gridCoord));
    }
    return newNeighborhood;
}

std::vector<NeighborhoodFormat> vonNeumannNeighborhoodRule::getFormats() const
{
    if (current_format_radius != radius.val)
        update_format();

    std::vector<NeighborhoodFormat> vector;
    vector.push_back(format);
    return vector;
}

void vonNeumannNeighborhoodRule::update_format() const
{
    // Les coordonnées sont copiées dans le vecteur
    // J'ai repris l'implémentation du voisinage de Moore, sauf que les cellules qui ne respectent pas la formule sont exclues
    // N(i,j) = {(k, l)|abs(k − i) + abs(l − j) ≤ r}
    Coord newCord;
    for(int i = -radius.val; i <= radius.val; i++){
        for(int j = -radius.val; j <= radius.val; j++){
            if((abs(i) + abs(j)) <= radius.val) {
                newCord.x = i;
                newCord.y = j;
                if(!(newCord.x == 0 && newCord.y == 0)) {
                    format.positions.push_back(newCord);
                }
            }
        }
    }
}

