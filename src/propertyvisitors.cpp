#include "propertyvisitors.hpp"

#include <QJsonArray>

PropertySaverVisitor::PropertySaverVisitor()
{
    // create the top level json object
    push_object();
}

QJsonObject PropertySaverVisitor::save()
{
    return current().toJsonObject();
}

QVariant &PropertySaverVisitor::current()
{
    assert(!m_current_hierarchy.empty());
    return m_current_hierarchy.top();
}

void PropertySaverVisitor::save_value(Property &prop, const QJsonValue &val)
{
    if (current().canConvert<QJsonArray>())
    {
        QJsonArray arr = current().toJsonArray();
        arr.append(val);
        current() = arr;
    }
    else
    {
        QJsonObject obj = current().toJsonObject();
        obj[QString::fromStdString(prop.identifier())] = val;
        current() = obj;
    }
}

void PropertySaverVisitor::push_object()
{
    m_current_hierarchy.push(QJsonObject{});
}

void PropertySaverVisitor::push_array()
{
    m_current_hierarchy.push(QJsonArray{});
}

QJsonValue PropertySaverVisitor::pop_value()
{
    QJsonValue val;
    if (current().canConvert<QJsonArray>())
        val = QJsonValue(current().toJsonArray());
    else
        val = QJsonValue(current().toJsonObject());

    m_current_hierarchy.pop();
    return val;
}

void PropertySaverVisitor::visit(StringProperty &prop)
{
    save_value(prop, QJsonValue(QString::fromStdString(prop.str)));
}

void PropertySaverVisitor::visit(IntegerProperty &prop)
{
    save_value(prop, QJsonValue(prop.val));
}

void PropertySaverVisitor::visit(CoordinateProperty &prop)
{
    QJsonArray arr;
    arr.append(prop.c.x); arr.append(prop.c.y);
    save_value(prop, QJsonValue(arr));
}

void PropertySaverVisitor::visit(PropertyList &list)
{
    push_array();

    for (const auto& ptr : list.contents)
        ptr->accept(*this);

    QJsonValue array = pop_value();
    save_value(list, array);
}

PropertyLoaderVisitor::PropertyLoaderVisitor(const QJsonObject& root)
{
    m_current_hierarchy.push(root);
}

QJsonValue &PropertyLoaderVisitor::current()
{
    assert(!m_current_hierarchy.empty());
    return m_current_hierarchy.top();
}

void PropertyLoaderVisitor::enter_value(const QJsonValue &value)
{
    m_current_hierarchy.push(value);
}

QJsonValue PropertyLoaderVisitor::exit_value()
{
    QJsonValue val = m_current_hierarchy.top();
    m_current_hierarchy.pop();
    return val;
}

QJsonValue PropertyLoaderVisitor::fetch_value(Property &prop)
{
    // in the case of an array, fetch the front value
    if (current().isArray())
    {
        QJsonArray arr = current().toArray();
        QJsonValue val = arr.first();
        arr.pop_front();
        current() = arr;
        return val;
    }
    else
        return current().toObject().value(prop.identifier().c_str());
}

void PropertyLoaderVisitor::visit(StringProperty &prop)
{
    QJsonValue val = fetch_value(prop);
    if (val.isString())
    {
        prop.str = val.toString().toStdString();
    }
}

void PropertyLoaderVisitor::visit(IntegerProperty &prop)
{
    QJsonValue val = fetch_value(prop);
    if (val.isDouble())
    {
        prop.val = val.toInt();
        if (prop.val > prop.prop_max || prop.val < prop.prop_min)
        {
            throw PropertyVisitorException("Invalid property value");
        }
    }
}

void PropertyLoaderVisitor::visit(CoordinateProperty &prop)
{
    QJsonValue val = fetch_value(prop);
    if (val.isArray() && val.toArray().size() == 2)
    {
        prop.c.x = val.toArray()[0].toInt();
        prop.c.y = val.toArray()[1].toInt();
    }
}


void PropertyLoaderVisitor::visit(PropertyList &prop)
{
    QJsonValue val = fetch_value(prop);
    if (val.isArray())
    {
        enter_value(val);

        if (!prop.dynamic && val.toArray().size() != (int)prop.contents.size())
            throw PropertyVisitorException("Invalid array size");

        prop.clear();
        for (int i = 0; i < val.toArray().size(); ++i)
        {
            Property& back = prop.push_back();
            back.accept(*this);
        }

        exit_value();
    }
}
