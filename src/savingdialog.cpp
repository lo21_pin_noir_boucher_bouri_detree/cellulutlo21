#include "savingdialog.hpp"
#include "ui_savingdialog.h"

#include <QDate>
#include <QMessageBox>

SavingDialog::SavingDialog(QWidget *parent) :
      QDialog(parent),
      ui(new Ui::SavingDialog)
{
    ui->setupUi(this);

    // récupérer le nom de l'utilisateur actuel
    QString name;
    name = qgetenv("USER"); // get the user name in Linux
    if (name.isEmpty())
        name = qgetenv("USERNAME"); // get the name in Windows

    ui->auteur->setText(name);
    ui->date->setDate(QDate::currentDate());
}

SavingDialog::~SavingDialog()
{
    delete ui;
}

void SavingDialog::done(int r)
{
    if(QDialog::Accepted == r)  // ok was pressed
    {
        if(ui->nom->text().isEmpty())   // validate the data somehow
        {
            QMessageBox::information(this, "Error", "The 'Title' field needs to be completed.");
            return;
        }
        else
        {
            QDialog::done(r);
            return;
        }
    }
    else    // cancel, close or exc was pressed
    {
        QDialog::done(r);
        return;
    }
}

QString SavingDialog::auteur() const
{
    return ui->auteur->text();
}

QString SavingDialog::titre() const
{
    return ui->nom->text();
}

QString SavingDialog::desc() const
{
    return ui->desc->toPlainText();
}

QDate SavingDialog::date() const
{
    return ui->date->date();
}
