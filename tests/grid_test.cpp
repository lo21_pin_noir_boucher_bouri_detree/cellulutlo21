#include <QtTest/QtTest>
#include <algorithm>
#include "cellulut_tests.hpp"
#include "grid.hpp"

void CellulutTests::test_grid()
{
    Grid grid(7,12);
    // Test méthode "Set cell" et "Get state"
    Coord pos1 = {4,4};
    grid.set_cell(pos1,2);
    Coord pos2 = {1,1};
    grid.set_cell(pos2,10);
    Coord pos3 = {1,2};
    grid.set_cell(pos3,1);
    int state = grid.get_state(pos1);
    QCOMPARE(state, 2);
    QVERIFY(grid.get_state(pos2) == 10);
    QVERIFY(grid.get_state(pos3) == 1);

    // Test operator=
    Grid grid2(5,6);
    QVERIFY(grid2.get_rows() == 5);
    QVERIFY(grid2.get_col() == 6);

    grid2 = grid;
    QVERIFY(grid2.get_rows() == 7);
    QVERIFY(grid2.get_col() == 12);
    QVERIFY(grid2.get_state(pos1) == 2);
    QVERIFY(grid2.get_state(pos2) == 10);
    QVERIFY(grid2.get_state(pos3) == 1);
}
