/*
 * <32021 by Stellaris. Copying Art is an act of love. Love is not subject to law.
 */

#include "cellulut_tests.hpp"

#include "totalistictransition.hpp"
#include "propertyvisitors.hpp"

void CellulutTests::test_totalistictransition()
{
    QCOMPARE(eval_math("5 + 6*3") , 23);
    QCOMPARE(eval_math("5 * 6+3"), 33);
    QCOMPARE(eval_math("(5+6) * 3"), 33);

    QCOMPARE(eval_math("(i+1)%4", {{'i', 2}}), 3);
    QCOMPARE(eval_math("(i+1)%4", {{'i', 3}}), 0);


    {
        TotalisticRuleEntry entry("i, (i+1)%3:[3] -> (i+1)%3");

        Neighborhood neighborhood;
        neighborhood.addNeighbor({0, +1}, 1);
        neighborhood.addNeighbor({+1, 0}, 1);
        neighborhood.addNeighbor({0, -1}, 1);
        neighborhood.addNeighbor({-1, 0}, 2);
        unsigned next = 0;
        bool accepted = entry.accept(0, neighborhood, next);
        QVERIFY(accepted);
        QCOMPARE(next, 1u);
    }

    {
        TotalisticRuleEntry entry("i, (i+1)%3:[3] -> (i+1)%3");

        Neighborhood neighborhood;
        neighborhood.addNeighbor({0, +1}, 2);
        neighborhood.addNeighbor({+1, 0}, 2);
        neighborhood.addNeighbor({0, -1}, 2);
        neighborhood.addNeighbor({-1, 0}, 0);
        unsigned next = 0;
        bool accepted = entry.accept(1, neighborhood, next);
        QVERIFY(accepted);
        QCOMPARE(next, 2u);
    }

    {
        TotalisticRuleEntry entry("i, (i+1)%3:[3] -> (i+1)%3");

        Neighborhood neighborhood;
        neighborhood.addNeighbor({0, +1}, 0);
        neighborhood.addNeighbor({+1, 0}, 0);
        neighborhood.addNeighbor({0, -1}, 0);
        neighborhood.addNeighbor({-1, 0}, 1);
        unsigned next = 0;
        bool accepted = entry.accept(2, neighborhood, next);
        QVERIFY(accepted);
        QCOMPARE(next, 0u);
    }

    {
        TotalisticRuleEntry entry("1, 0:[0..1] , 1:* ->0");

        Neighborhood neighborhood;
        neighborhood.addNeighbor({0, +1}, 0);
        neighborhood.addNeighbor({+1, 0}, 0);
        neighborhood.addNeighbor({0, -1}, 1);
        neighborhood.addNeighbor({-1, 0}, 2);
        unsigned next = 0;
        bool accepted = entry.accept(1, neighborhood, next);
        QVERIFY(accepted == false);
    }

    {
        TotalisticRuleEntry entry("1, 0:[0..1] , 1:* ->0");

        Neighborhood neighborhood;
        neighborhood.addNeighbor({0, +1}, 0);
        neighborhood.addNeighbor({+1, 0}, 1);
        neighborhood.addNeighbor({0, -1}, 1);
        neighborhood.addNeighbor({-1, 0}, 2);
        unsigned next = 1;
        bool accepted = entry.accept(1, neighborhood, next);
        QVERIFY(accepted);
        QCOMPARE(next, 0u);
    }

    {
        TotalisticTransition rule;

        QJsonObject obj;
        obj["rule_string"] = "1->2\n"
                             "2->3\n"
                             "3,1:[1..2]->1\n";
        PropertyLoaderVisitor visit(obj);
        for (auto& prop : rule.get_properties())
            prop->accept(visit);

        {
            Neighborhood neighborhood;
            neighborhood.addNeighbor({0, +1}, 0);
            neighborhood.addNeighbor({+1, 0}, 1);
            neighborhood.addNeighbor({0, -1}, 1);
            neighborhood.addNeighbor({-1, 0}, 2);
            unsigned next = rule.getState(0, neighborhood);
            QCOMPARE(next, 0u);
            next = rule.getState(1, neighborhood);
            QCOMPARE(next, 2u);
            next = rule.getState(2, neighborhood);
            QCOMPARE(next, 3u);
            next = rule.getState(3, neighborhood);
            QCOMPARE(next, 1u);
        }

        {
            Neighborhood neighborhood;
            neighborhood.addNeighbor({0, +1}, 0);
            neighborhood.addNeighbor({+1, 0}, 1);
            neighborhood.addNeighbor({0, -1}, 1);
            neighborhood.addNeighbor({-1, 0}, 1);
            unsigned next = rule.getState(0, neighborhood);
            QCOMPARE(next, 0u);
            next = rule.getState(1, neighborhood);
            QCOMPARE(next, 2u);
            next = rule.getState(2, neighborhood);
            QCOMPARE(next, 3u);
            next = rule.getState(3, neighborhood);
            QCOMPARE(next, 3u);
        }
    }

    {
        TotalisticTransition rule;

        QJsonObject obj;
        obj["rule_string"] = "0,1:[3]->1\n"
                             "1,1:[0..1]->0\n"
                             "1,1:[4..*]->0\n";
        PropertyLoaderVisitor visit(obj);
        for (auto& prop : rule.get_properties())
            prop->accept(visit);

        {
            Neighborhood neighborhood;
            neighborhood.addNeighbor({0, +1}, 0);
            neighborhood.addNeighbor({+1, 0}, 1);
            neighborhood.addNeighbor({0, -1}, 1);
            neighborhood.addNeighbor({-1, 0}, 1);
            unsigned next = rule.getState(0, neighborhood);
            QCOMPARE(next, 1u);
            next = rule.getState(1, neighborhood);
            QCOMPARE(next, 1u);
        }

        {
            Neighborhood neighborhood;
            neighborhood.addNeighbor({0, +1}, 0);
            neighborhood.addNeighbor({+1, 0}, 1);
            neighborhood.addNeighbor({0, -1}, 0);
            neighborhood.addNeighbor({-1, 0}, 1);
            unsigned next = rule.getState(0, neighborhood);
            QCOMPARE(next, 0u);
            next = rule.getState(1, neighborhood);
            QCOMPARE(next, 1u);
        }

        {
            Neighborhood neighborhood;
            neighborhood.addNeighbor({0, +1}, 0);
            neighborhood.addNeighbor({+1, 0}, 1);
            neighborhood.addNeighbor({0, -1}, 0);
            neighborhood.addNeighbor({-1, 0}, 0);
            unsigned next = rule.getState(0, neighborhood);
            QCOMPARE(next, 0u);
            next = rule.getState(1, neighborhood);
            QCOMPARE(next, 0u);
        }

        {
            Neighborhood neighborhood;
            neighborhood.addNeighbor({0, +1}, 1);
            neighborhood.addNeighbor({+1, 0}, 1);
            neighborhood.addNeighbor({0, -1}, 1);
            neighborhood.addNeighbor({-1, 0}, 1);
            unsigned next = rule.getState(0, neighborhood);
            QCOMPARE(next, 0u);
            next = rule.getState(1, neighborhood);
            QCOMPARE(next, 0u);
        }
    }
}
