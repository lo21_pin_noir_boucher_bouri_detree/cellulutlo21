#include <QtTest/QtTest>
#include <algorithm>
#include "cellulut_tests.hpp"
#include "alphabet.hpp"

//test sur la classe alphabet et aussi stateColor et State

void CellulutTests::test_alphabet(){
    Alphabet a;
    stateColor c1(1,5,126);
    state s1(c1,"Alpha");
    a.newEtat(s1);

    stateColor c2(144,245,74);
    state s2(c2,"beta");
    a.newEtat(s2);

    /*stateColor c3('g','h','i');
    state s3(c3,"gamma");
    a.newEtat(s3);*/

    // L'alphabet possède toujours un état 0, chaque état qui est ajouté commence au numéro 1
    state test=a.getState(1);
    stateColor sc =test.getColor();
    unsigned char cb=sc.getBlue();
    unsigned char cr=sc.getRed();
    unsigned char cg=sc.getGreen();
    QCOMPARE(cb,(unsigned char)126);
    QCOMPARE(cg,(unsigned char)5);
    QCOMPARE(cr,(unsigned char)1);

    state test2=a.getState(2);
    stateColor sc2 =test2.getColor();
    unsigned char cb2=sc2.getBlue();
    unsigned char cr2=sc2.getRed();
    unsigned char cg2=sc2.getGreen();
    QCOMPARE(cb2,(unsigned char)74);
    QCOMPARE(cg2,(unsigned char)245);
    QCOMPARE(cr2,(unsigned char)144);

}
