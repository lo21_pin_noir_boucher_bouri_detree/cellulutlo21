#include <QtTest/QtTest>

#include <algorithm>

#include "cellulut_tests.hpp"

#include "structure.hpp"

void CellulutTests::test_structure()
{
    std::vector<std::pair<Coord, unsigned>> coords;
    std::vector<std::pair<Coord, unsigned>> normalized_coords;
    std::vector<std::pair<Coord, unsigned>> empty;
    coords.push_back({{1, 3}, 3}); normalized_coords.push_back({{2, 1}, 3});
    coords.push_back({{-1, 2}, 1}); normalized_coords.push_back({{0, 0}, 1});

    {
        Structure s1(coords.begin(), coords.end());
        Structure s2;
        s2.load(coords.begin(), coords.end());
        QCOMPARE(s1.size(), coords.size());
        QCOMPARE(s2.size(), coords.size());

        QVERIFY(std::is_permutation(normalized_coords.begin(), normalized_coords.end(), s1.begin()));
        QVERIFY(std::is_permutation(normalized_coords.begin(), normalized_coords.end(), s2.begin()));

        QCOMPARE(s1.width(), 3u);
        QCOMPARE(s1.height(), 2u);

        s1.load(empty.begin(), empty.end());
        QVERIFY(s1.size() == 0);
    }

    std::vector<std::pair<Coord, unsigned>> coords_with_zero;
    coords_with_zero = coords;
    coords_with_zero.push_back({{-2, 2}, 0});

    {
        Structure s1(coords_with_zero.begin(), coords_with_zero.end());
        QCOMPARE(s1.size(), coords.size()); // 2 cellules non nulles, on ignore les cellules à zéro qui sont toujours implicites
        QVERIFY(std::is_permutation(normalized_coords.begin(), normalized_coords.end(), s1.begin()));

        QCOMPARE(s1.width(), 3u);
        QCOMPARE(s1.height(), 2u);
    }
}

