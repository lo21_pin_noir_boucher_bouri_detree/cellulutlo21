
#include "cellulut_tests.hpp"

#include "nonisotropictransition.hpp"
#include "propertyvisitors.hpp"

void CellulutTests::test_nonisotropictransition()
{

    {
        NonIsotropicRuleEntry entry("0, 0, 1, 1, 2 -> 1", false, false, false);

        Neighborhood neighborhood;
        neighborhood.addNeighbor({0, +1}, 2); // index 3
        neighborhood.addNeighbor({+1, 0}, 1); // index 2
        neighborhood.addNeighbor({0, -1}, 0); // index 0
        neighborhood.addNeighbor({-1, 0}, 1); // index 1
        unsigned next = 0;
        bool accepted = entry.accept(0, neighborhood, next);
        QVERIFY(accepted);
        QCOMPARE(next, 1u);
    }

    {
        NonIsotropicRuleEntry entry("0, 0, 1, 1, 2 -> 1", false, false, false);

        Neighborhood neighborhood;
        neighborhood.addNeighbor({0, +1}, 2); // index 3
        neighborhood.addNeighbor({+1, 0}, 1); // index 2
        neighborhood.addNeighbor({0, -1}, 1); // index 0
        neighborhood.addNeighbor({-1, 0}, 1); // index 1
        unsigned next = 0;
        bool accepted = entry.accept(0, neighborhood, next);
        QVERIFY(!accepted);
    }


    {
        NonIsotropicRuleEntry entry("0, 0, 1, 0, 1 -> 1", false, false, false);

        Neighborhood neighborhood;
        neighborhood.addNeighbor({0, +1}, 1); // index 3
        neighborhood.addNeighbor({+1, 0}, 0); // index 2
        neighborhood.addNeighbor({0, -1}, 0); // index 0
        neighborhood.addNeighbor({-1, 0}, 1); // index 1
        unsigned next = 0;
        bool accepted = entry.accept(0, neighborhood, next);
        QVERIFY(accepted);
        QCOMPARE(next, 1u);
    }

    {
        NonIsotropicRuleEntry entry("0, 0, 1, 0, 1 -> 1", false, false, false);

        Neighborhood neighborhood;
        neighborhood.addNeighbor({0, +1}, 0); // index 3
        neighborhood.addNeighbor({+1, 0}, 1); // index 2
        neighborhood.addNeighbor({0, -1}, 1); // index 0
        neighborhood.addNeighbor({-1, 0}, 0); // index 1
        unsigned next = 0;
        bool accepted = entry.accept(0, neighborhood, next);
        QVERIFY(!accepted);
    }

    {
        NonIsotropicRuleEntry entry("0, 0, 1, 0, 1 -> 1", true, true, false);

        Neighborhood neighborhood;
        neighborhood.addNeighbor({0, +1}, 0); // index 3
        neighborhood.addNeighbor({+1, 0}, 1); // index 2
        neighborhood.addNeighbor({0, -1}, 1); // index 0
        neighborhood.addNeighbor({-1, 0}, 0); // index 1
        unsigned next = 0;
        bool accepted = entry.accept(0, neighborhood, next);
        QVERIFY(accepted);
        QCOMPARE(next, 1u);
    }

    {
        NonIsotropicTransition rule;

        QJsonObject obj;
        obj["rule_string"] = "rotate4\n"
                             "0,1,0,0,0->1\n";
        PropertyLoaderVisitor visit(obj);
        for (auto& prop : rule.get_properties())
            prop->accept(visit);

        {
            Neighborhood neighborhood;
            neighborhood.addNeighbor({0, +1}, 0); // index 3
            neighborhood.addNeighbor({+1, 0}, 0); // index 2
            neighborhood.addNeighbor({0, -1}, 1); // index 0
            neighborhood.addNeighbor({-1, 0}, 0); // index 1
            unsigned next = rule.getState(0, neighborhood);
            QCOMPARE(next, 1u);
        }

        {
            Neighborhood neighborhood;
            neighborhood.addNeighbor({0, +1}, 0); // index 3
            neighborhood.addNeighbor({+1, 0}, 0); // index 2
            neighborhood.addNeighbor({0, -1}, 0); // index 0
            neighborhood.addNeighbor({-1, 0}, 1); // index 1
            unsigned next = rule.getState(0, neighborhood);
            QCOMPARE(next, 1u);
        }
        {
            Neighborhood neighborhood;
            neighborhood.addNeighbor({0, +1}, 0); // index 3
            neighborhood.addNeighbor({+1, 0}, 1); // index 2
            neighborhood.addNeighbor({0, -1}, 0); // index 0
            neighborhood.addNeighbor({-1, 0}, 0); // index 1
            unsigned next = rule.getState(0, neighborhood);
            QCOMPARE(next, 1u);
        }
        {
            Neighborhood neighborhood;
            neighborhood.addNeighbor({0, +1}, 1); // index 3
            neighborhood.addNeighbor({+1, 0}, 0); // index 2
            neighborhood.addNeighbor({0, -1}, 0); // index 0
            neighborhood.addNeighbor({-1, 0}, 0); // index 1
            unsigned next = rule.getState(0, neighborhood);
            QCOMPARE(next, 1u);
        }
        {
            Neighborhood neighborhood;
            neighborhood.addNeighbor({0, +1}, 1); // index 3
            neighborhood.addNeighbor({+1, 0}, 0); // index 2
            neighborhood.addNeighbor({0, -1}, 1); // index 0
            neighborhood.addNeighbor({-1, 0}, 0); // index 1
            unsigned next = rule.getState(0, neighborhood);
            QCOMPARE(next, 0u);
        }
    }
}
