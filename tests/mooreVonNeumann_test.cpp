#include <QtTest/QtTest>
#include <algorithm>
#include "cellulut_tests.hpp"

#include "vonNeumannNeighborhoodRule.hpp"

void CellulutTests::test_VonNeumann(){
    Grid g(11,11);
    Coord cellule={5,5};
    Coord pos1={5,6};
    Coord pos2={6,5};
    Coord pos3={5,4};
    Coord pos4={4,5};
    g.set_cell(cellule,1);
    g.set_cell(pos1,2);
    g.set_cell(pos2,3);
    g.set_cell(pos3,3);
    g.set_cell(pos4,5);

    vonNeumannNeighborhoodRule voi(1);
    Neighborhood v=voi.getNeighborhood(g,cellule);
    int nb_voi=v.getNb(3);
    int nb_voi2=v.getNb(5);
    int nb_voi3=v.getNb(2);
    int nb_voi4=v.getNb(8);
    QCOMPARE(nb_voi,2);
    QCOMPARE(nb_voi2,1);
    QCOMPARE(nb_voi3,1);
    QCOMPARE(nb_voi4,0);

}
