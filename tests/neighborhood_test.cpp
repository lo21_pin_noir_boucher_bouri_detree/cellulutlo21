#include <QtTest/QtTest>
#include "cellulut_tests.hpp"
#include "neighborhood.hpp"
#include "state.hpp"
#include "stateColor.hpp"
#include "coord.hpp"

void CellulutTests::test_neighborhood(){

    Neighborhood voisins_vide; // Objet Neighborhood qui est une liste de voisins vide
    Neighborhood voisins_c_1; // Objet Neighborhood auquel on va lui affecter un voisin
    Neighborhood voisins_full; // Objet Neighborhood auquel on va lui affecter l'ensemble de ses voisins

    Coord c_1; // On va définir une coordonée c_1
    c_1.x=0;
    c_1.y=1;
    Coord c_2; // On va définir une coordonée c_2
    c_2.x=1;
    c_2.y=0;
    Coord c_3; // On va définir une coordonée c_1
    c_1.x=0;
    c_1.y=-1;
    Coord c_4; // On va définir une coordonée c_2
    c_2.x=-1;
    c_2.y=0;


    stateColor green_color(0,128,0);
    stateColor blue_color(0,0,255);
    stateColor red_color(255,0,0);
    stateColor yellow_color(255,255,0);

    state green(green_color, "vert"); // On crée notre état
    state blue(blue_color, "bleu");
    state red(red_color, "rouge");
    state yellow(yellow_color, "jaune");

    voisins_c_1.addNeighbor(c_1,green); // On affecte à notre objet voisins_c_1 un voisin de coordonnées c_1 et d'état "vert"

    voisins_full.addNeighbor(c_1,green); // On affecte à notre objet voisins_full nos 4 voisins
    voisins_full.addNeighbor(c_2,red);
    voisins_full.addNeighbor(c_3,green);
    voisins_full.addNeighbor(c_4,blue);



    // Test "isUnique"
    QVERIFY(voisins_vide.isUnique(c_1) == true);
    QVERIFY(voisins_c_1.isUnique(c_1) == false);
    QVERIFY(voisins_c_1.isUnique(c_2) == true);

    // Test "getNb"
    QVERIFY(voisins_full.getNb(blue) == 1);
    QVERIFY(voisins_full.getNb(green) == 2);
    QVERIFY(voisins_full.getNb(yellow) == 0);

    // Test "getAt"
    QVERIFY(voisins_full.getAt(c_4) == blue);
    QVERIFY(voisins_full.getAt(c_1) == green);


}
