#include <QtTest/QtTest>

#include <algorithm>

#include "cellulut_tests.hpp"

#include "coord.hpp"

void CellulutTests::test_coord()
{
    Coord a{1, 2};
    Coord b{3, 4};

    QCOMPARE(a.x, 1);
    QCOMPARE(a.y, 2);

    QVERIFY(a == a);
    QVERIFY(b == b);
    QVERIFY(a != b);
    QVERIFY(!(a == b));

    QCOMPARE(wrap_coords(Coord{3, 4}, 2, 2), (Coord{1, 0}));
    QCOMPARE((Coord{1, 2} + Coord{3, 4}), (Coord{4, 6}));
    QCOMPARE((Coord{1, 2} - Coord{3, 4}), (Coord{-2, -2}));
}

