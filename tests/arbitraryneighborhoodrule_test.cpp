#include "cellulut_tests.hpp"

#include "arbitraryneighborhoodrule.hpp"
#include "propertyvisitors.hpp"

void CellulutTests::test_arbitrary_neighborhood_rule()
{
    std::vector<Coord> coords =
        {
            {1, 2},
            {-1, 2},
            {0, 3},
            {2, 4},
            };

    QJsonObject obj;
    QJsonArray json_array;
    for (auto coord : coords)
    {
        QJsonArray entry;
        entry.push_back(coord.x);
        entry.push_back(coord.y);
        json_array.push_back(entry);
    }
    obj["neighbors"] = json_array;

    ArbitraryNeighborhoodRule rule;
    {
        PropertyLoaderVisitor visit(obj);
        for (auto& prop : rule.get_properties())
            prop->accept(visit);
    }

    QCOMPARE(rule.getFormats().size(), 1u);
    QCOMPARE(rule.getFormats()[0].positions.size(), coords.size());

    auto list = rule.getFormats()[0].positions;

    QVERIFY(std::is_permutation(coords.begin(), coords.end(), rule.getFormats()[0].positions.begin()));
}
