#include "cellulut_tests.hpp"

#include "lifegametransition.hpp"

void CellulutTests::test_lifegametransition()
{
    LifeGameTransition rule;

    // Pas de naissance
    {
        Neighborhood n;
        n.addNeighbor({0, 1}, 1);
        n.addNeighbor({0, -1}, 0);
        n.addNeighbor({1, 0}, 0);
        n.addNeighbor({-1, 0}, 1);

        int next = rule.getState(0, n);
        QCOMPARE(next, 0);
    }

    // Naissance
    {
        Neighborhood n;
        n.addNeighbor({0, 1}, 1);
        n.addNeighbor({0, -1}, 1);
        n.addNeighbor({1, 0}, 0);
        n.addNeighbor({-1, 0}, 1);

        int next = rule.getState(0, n);
        QCOMPARE(next, 1);
    }

    // Survie
    {
        Neighborhood n;
        n.addNeighbor({0, 1}, 1);
        n.addNeighbor({0, -1}, 1);
        n.addNeighbor({1, 0}, 0);
        n.addNeighbor({-1, 0}, 0);

        int next = rule.getState(1, n);
        QCOMPARE(next, 1);
    }

    // Sur-population
    {
        Neighborhood n;
        n.addNeighbor({0, 1}, 1);
        n.addNeighbor({0, -1}, 1);
        n.addNeighbor({1, 0}, 1);
        n.addNeighbor({-1, 0}, 1);
        n.addNeighbor({-1, -1}, 1);

        int next = rule.getState(1, n);
        QCOMPARE(next, 0);
    }

    // Sous-population
    {
        Neighborhood n;
        n.addNeighbor({0, 1}, 0);
        n.addNeighbor({0, -1}, 0);
        n.addNeighbor({1, 0}, 1);
        n.addNeighbor({-1, 0}, 0);

        int next = rule.getState(1, n);
        QCOMPARE(next, 0);
    }
}
