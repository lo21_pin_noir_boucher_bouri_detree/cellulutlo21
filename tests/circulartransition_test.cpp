#include "cellulut_tests.hpp"

#include "circulartransition.hpp"
#include "propertyvisitors.hpp"

void CellulutTests::test_circulartransition()
{
    QJsonObject obj;
    obj["states"] = 4;
    obj["threshold"] = 3;

    CircularTransition rule;    // load
    {
        PropertyLoaderVisitor visit(obj);
        for (auto& prop : rule.get_properties())
            prop->accept(visit);
    }

    // En dessous du seuil
    {
        Neighborhood n;
        n.addNeighbor({0, 1}, 1);
        n.addNeighbor({0, -1}, 0);
        n.addNeighbor({1, 0}, 0);
        n.addNeighbor({-1, 0}, 1);

        int next = rule.getState(0, n);
        QCOMPARE(next, 0);
    }

    // >= au seuil
    {
        Neighborhood n;
        n.addNeighbor({0, 1}, 1);
        n.addNeighbor({0, -1}, 1);
        n.addNeighbor({1, 0}, 1);
        n.addNeighbor({-1, 0}, 0);

        int next = rule.getState(0, n);
        QCOMPARE(next, 1);
    }

    // 1 -> 2
    {
        Neighborhood n;
        n.addNeighbor({0, 1}, 2);
        n.addNeighbor({0, -1}, 2);
        n.addNeighbor({1, 0}, 2);
        n.addNeighbor({-1, 0}, 0);

        int next = rule.getState(1, n);
        QCOMPARE(next, 2);
    }

    // Vérification du modulo pour le nombre d'états
    {
        Neighborhood n;
        n.addNeighbor({0, 1}, 0);
        n.addNeighbor({0, -1}, 0);
        n.addNeighbor({1, 0}, 0);
        n.addNeighbor({-1, 0}, 0);

        int next = rule.getState(3, n);
        QCOMPARE(next, 0);
    }
}
