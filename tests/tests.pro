QT       += core gui testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++14 

TEMPLATE = app


# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += ../include  ../include/transition_rules ../include/neighborhood_rules

SOURCES += \
    ../src/propertyvisitors.cpp \
    ../src/structurereader.cpp \
    ../src/structurewriter.cpp \
    ../src/neighborhood.cpp \
    ../src/neighborhood_rules/arbitraryneighborhoodrule.cpp \
    ../src/history.cpp \
    ../src/grid.cpp \
    ../src/neighborhood_rules/mooreNeighborhoodRule.cpp \
    ../src/neighborhood_rules/vonNeumannNeighborhoodRule.cpp \
    ../src/transition_rules/lifegametransition.cpp \
    ../src/transition_rules/circulartransition.cpp \
    ../src/transition_rules/totalistictransition.cpp \
    ../src/transition_rules/nonisotropictransition.cpp \
    ../src/mathexpr.cpp \
    ../src/alphabet.cpp \
    alphabet_test.cpp \
    arbitraryneighborhoodrule_test.cpp \
    circulartransition_test.cpp \
    coord_tests.cpp \
    factory_tests.cpp \
    grid_test.cpp \
    history_test.cpp \
    lifegametransition_test.cpp \
    mooreNeighborhoodRule_test.cpp \
    mooreVonNeumann_test.cpp \
    nonisotropictransition_test.cpp \
    property_test.cpp \
    propertyvisitors_test.cpp \
    cellulut_tests.cpp \
    structure_test.cpp \
    structurereader_tests.cpp \
    structurewriter_tests.cpp \
    totalistictransition_test.cpp

HEADERS += \
    cellulut_tests.hpp


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
