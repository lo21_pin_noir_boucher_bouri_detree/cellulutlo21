#include "cellulut_tests.hpp"

#include "factory.hpp"

class Transition
{
public:
    virtual std::string say_hi() = 0;
};

class LifeLikeTransition : public Transition
{
public:

    virtual std::string say_hi()
    { return "Life hi!\n"; }
};
REGISTER_FACTORY_ENTRY(Transition, LifeLikeTransition, "life-like");

class GenerationsTransition : public Transition
{
public:

    virtual std::string say_hi()
    { return "Generations hi!\n"; }
};
REGISTER_FACTORY_ENTRY(Transition, GenerationsTransition, "generations");


void CellulutTests::test_factory()
{
    QCOMPARE(Factory<Transition>::list_choices().size(), 2ull);
    const auto choices = Factory<Transition>::list_choices();
    QCOMPARE(std::count(choices.begin(), choices.end(), "life-like"), 1);
    QCOMPARE(std::count(choices.begin(), choices.end(), "generations"), 1);


    std::unique_ptr<Transition> life_trans = Factory<Transition>::make("life-like");
    QVERIFY(life_trans != nullptr);
    QCOMPARE(life_trans->say_hi(), std::string("Life hi!\n"));

    std::unique_ptr<Transition> gens_trans = Factory<Transition>::make("generations");
    QVERIFY(gens_trans != nullptr);
    QCOMPARE(gens_trans->say_hi(), std::string("Generations hi!\n"));


    QVERIFY(nullptr == Factory<Transition>::make("<invalid>"));
}
