#include "cellulut_tests.hpp"

#include "property.hpp"
#include "propertyvisitors.hpp"

struct Example : public HasUserProperties
{
public:

public:
    DEFINE_CONFIGURABLE_PROPERTY(StringProperty, champ_test, "Champ");
    DEFINE_CONFIGURABLE_PROPERTY(StringProperty, model_name, "Model name");
    DEFINE_CONFIGURABLE_PROPERTY(IntegerProperty, test_int, "Prop int", 0, 15);
    DEFINE_CONFIGURABLE_PROPERTY(CoordinateProperty, test_coord, "Position");
    DEFINE_CONFIGURABLE_DYNLIST (CoordinateProperty, test_list, "List", (), 1, 3);
};


void CellulutTests::test_loader_saver_visitor()
{
    Example ex;
    QVERIFY(ex.get_properties().size() == 5);
    ex.champ_test.str = "champ_test";
    ex.model_name.str = "model_name";
    ex.test_int.val = 5;
    ex.test_coord.c.x = 1; ex.test_coord.c.y = 2;
    CoordinateProperty& prop1 = static_cast<CoordinateProperty&>(ex.test_list.push_back());
    CoordinateProperty& prop2 = static_cast<CoordinateProperty&>(ex.test_list.push_back());
    prop1.c.x = -1; prop1.c.y = 3;
    prop2.c.x = 0; prop2.c.y = -3;


    QJsonObject json;
    // save
    {
        PropertySaverVisitor visit;
        for (auto& prop : ex.get_properties())
            prop->accept(visit);

        json = visit.save();
    }

    Example ex_loaded;
    // load
    {
        PropertyLoaderVisitor visit(json);
        for (auto& prop : ex_loaded.get_properties())
            prop->accept(visit);
    }

    QVERIFY(ex_loaded.champ_test.str == ex.champ_test.str);
    QVERIFY(ex_loaded.model_name.str == ex.model_name.str);
    QVERIFY(ex_loaded.test_int.val == ex.test_int.val);
    QVERIFY(ex_loaded.test_coord.c == ex.test_coord.c);

    QVERIFY(ex_loaded.test_list.size() == ex.test_list.size());
    for (size_t i = 0; i < ex_loaded.test_list.size(); ++i)
    {
        CoordinateProperty& prop1 = static_cast<CoordinateProperty&>(ex_loaded.test_list.at(i));
        CoordinateProperty& prop2 = static_cast<CoordinateProperty&>(ex.test_list.at(i));
        QVERIFY(prop1.c == prop2.c);
    }
}

