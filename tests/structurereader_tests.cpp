#include <QtTest/QtTest>

#include <algorithm>

#include "cellulut_tests.hpp"

#include "structure.hpp"
#include "structurereader.hpp"

static const char* rle_glider =
    "#C This is a glider.\n"
    "x = 3, y = 3\n"
    "bo$2bo$3o!\n";

static const char* rle_glider_gun =
    "#N Gosper glider gun\n"
    "#C This was the first gun discovered.\n"
    "#C As its name suggests, it was discovered by Bill Gosper.\n"
    "    x = 36, y = 9, rule = B3/S23\n"
    "24bo$22bobo$12b2o6b2o12b2o$11bo3bo4b2o12b2o$2o8bo5bo3b2o$2o8bo3bob2o4b\n"
    "obo$10bo5bo7bo$11bo3bo$12b2o!";

static const char* rle_wireworld =
    "x = 6, y = 7, rule = WireWorld\n"
    ".A$.C$4C$C2.3C$4C$.C$.A!";

static const char* json_test_1 =
    "{\n"
    "    \"cells\" : [\n"
    "        { \"x\" : 0, \"y\" : 0, \"state\" : 1},\n"
    "        { \"x\" : 2, \"y\" : 3, \"state\" : 3}\n"
    "        ]\n"
    "}";

void CellulutTests::test_rle_structurereader()
{
    {
        RLEStructureReader rle(rle_glider);

        try {
            Structure stru = rle.read_structure();
            QCOMPARE(stru.size(), 5u);
            QVERIFY(std::count(stru.begin(), stru.end(), std::make_pair<Coord, unsigned>(Coord{1, 0}, 1)) == 1);
        }
        catch (const std::exception& ex)
        {
            fprintf(stderr, "Error is %s\n", ex.what());
            QFAIL("Exception thrown");
        }
    }

    {
        RLEStructureReader rle(rle_glider_gun);

        try {

            Structure stru = rle.read_structure();
            QCOMPARE(stru.size(), 36u);
        }
        catch (const std::exception& ex)
        {
            fprintf(stderr, "Error is %s\n", ex.what());
            QFAIL("Exception thrown");
        }
    }

    {
        RLEStructureReader rle(rle_wireworld);

        try {

            Structure stru = rle.read_structure();
            QCOMPARE(stru.size(), 16u);

            std::map<int, int> state_count;
            for (auto pair : stru)
            {
                state_count[pair.second]++;
            }
            QCOMPARE(state_count[1], 2);
            QCOMPARE(state_count[2], 0);
            QCOMPARE(state_count[3], 14);
        }
        catch (const std::exception& ex)
        {
            fprintf(stderr, "Error is %s\n", ex.what());
            QFAIL("Exception thrown");
        }
    }
}

void CellulutTests::test_json_structurereader()
{
    {
        JSONStructureReader rle(json_test_1);

        try {
            Structure stru = rle.read_structure();
            QCOMPARE(stru.size(), 2u);
            QVERIFY(std::count(stru.begin(), stru.end(), std::make_pair<Coord, unsigned>(Coord{0, 0}, 1)) == 1);
            QVERIFY(std::count(stru.begin(), stru.end(), std::make_pair<Coord, unsigned>(Coord{2, 3}, 3)) == 1);
        }
        catch (const std::exception& ex)
        {
            fprintf(stderr, "Error is %s\n", ex.what());
            QFAIL("Exception thrown");
        }
    }
}
