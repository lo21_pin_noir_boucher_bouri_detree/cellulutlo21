#include <QtTest/QtTest>
#include <algorithm>
#include "cellulut_tests.hpp"

#include "history.hpp"

void CellulutTests::test_history()
{
    History historyTest(2);
    QVERIFY(historyTest.get_nbMax() == 2);
    QVERIFY(historyTest.isEmpty() == true);

    Grid gridTest1(10,10);
    Grid gridTest2(10,10);
    Grid gridTest3(10,10);
    Coord pos1 {2 ,2};
    Coord pos2 {3 ,3};
    Coord pos3 {4 ,4};
    gridTest1.set_cell(pos1,2);
    gridTest2.set_cell(pos2,3);
    gridTest3.set_cell(pos3,4);

    // Push de 2 grilles
    historyTest.pushGrid(gridTest1);
    historyTest.pushGrid(gridTest2);
    QVERIFY(historyTest.isEmpty() == false);

    // On pop la première grille puis la deuxième
    Grid popGrid = historyTest.popGrid();
    QCOMPARE(popGrid.get_state(pos2), 3u);
    Grid topGrid = historyTest.topGrid();
    QVERIFY(topGrid.get_state(pos1) == 2);

    popGrid = historyTest.popGrid();
    QCOMPARE(popGrid.get_state(pos1), 2u);
    QVERIFY(historyTest.isEmpty() == true);

    // Push de trois grille (taille max de la pile de l'history = 2)
    historyTest.pushGrid(gridTest1);
    historyTest.pushGrid(gridTest2);
    historyTest.pushGrid(gridTest3); // La première grille est supprimée

    popGrid = historyTest.popGrid();
    QCOMPARE(popGrid.get_state(pos3), 4u);
    topGrid = historyTest.topGrid();
    QVERIFY(topGrid.get_state(pos2) == 3);

    popGrid = historyTest.popGrid();
    QCOMPARE(popGrid.get_state(pos2), 3u);
    QVERIFY(historyTest.isEmpty() == true);
}
