#include <QtTest/QtTest>
#include <algorithm>
#include "cellulut_tests.hpp"

#include "mooreNeighborhoodRule.hpp"
#include "propertyvisitors.hpp"

void CellulutTests::test_mooreNeighborhoodRule() {

    Grid g(11,11);
    Coord cellule={5,5};
    Coord pos1={5,6};
    Coord pos2={6,5};
    Coord pos3={5,4};
    Coord pos4={4,5};
    Coord pos={5,7};
    Coord pos5={7,5};
    Coord pos6={6,6};
    Coord pos7={4,4};
    Coord pos8={5,3};
    Coord pos9={3,5};
    Coord pos10={7,7};
    Coord pos11={4,3};
    Coord pos12={6,7};
    Coord pos13={7,6};
    Coord pos14={3,6};

    g.set_cell(cellule,1);
    g.set_cell(pos1,2);
    g.set_cell(pos2,3);
    g.set_cell(pos3,3);
    g.set_cell(pos4,5);
    g.set_cell(pos,5);
    g.set_cell(pos5,6);
    g.set_cell(pos6,8);
    g.set_cell(pos7,8);
    g.set_cell(pos8,8);
    g.set_cell(pos9,4);
    g.set_cell(pos11,4);
    g.set_cell(pos10,7);
    g.set_cell(pos12,7);
    g.set_cell(pos13,4);
    g.set_cell(pos14,2);

    // Tester que le chargement des propriétés depuis un JSON fonctionne bien
    QJsonObject obj;
    obj["radius"] = 2;

    mooreNeighborhoodRule newMoore(1);
    PropertyLoaderVisitor visit(obj);
    for (auto& prop : newMoore.get_properties())
        prop->accept(visit);

    Neighborhood v=newMoore.getNeighborhood(g,cellule);
    int nb_voi=v.getNb(3);
    int nb_voi2=v.getNb(5);
    int nb_voi3=v.getNb(2);
    int nb_voi4=v.getNb(8);
    int nb_voi5=v.getNb(7);
    int nb_voi6=v.getNb(4);

    QCOMPARE(nb_voi,2);
    QCOMPARE(nb_voi2,2);
    QCOMPARE(nb_voi3,2);
    QCOMPARE(nb_voi4,3);
    QCOMPARE(nb_voi5,2);
    QCOMPARE(nb_voi6,3);


}
