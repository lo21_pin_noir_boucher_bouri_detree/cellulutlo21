#include <QtTest/QtTest>

#include <algorithm>

#include "cellulut_tests.hpp"

#include "structure.hpp"
#include "structurewriter.hpp"
#include "structurereader.hpp"

static const char* rle_glider =
    "x = 3, y = 3\n"
    "bo$2bo$3o!";

static const char* rle_glider_gun =
    "x = 36, y = 9\n"
    "24bo$22bobo$12b2o6b2o12b2o$11bo3bo4b2o12b2o$2o8bo5bo3b2o$2o8bo3bob2o4b"
    "obo$10bo5bo7bo$11bo3bo$12b2o!";

void CellulutTests::test_rle_structurewriter()
{
    try
    {
        Structure stru;

        RLEStructureReader reader(rle_glider);
        stru = reader.read_structure();

        RLEStructureWriter writer;
        auto out = writer.save_structure(stru);

        QCOMPARE(out, std::string(rle_glider));
    }
    catch (const std::exception& ex)
    {
        fprintf(stderr, "Error is %s\n", ex.what());
        QFAIL("Exception thrown");
    }

    try
    {
        Structure stru;

        RLEStructureReader reader(rle_glider_gun);
        stru = reader.read_structure();

        RLEStructureWriter writer;
        auto out = writer.save_structure(stru);

        QCOMPARE(out, std::string(rle_glider_gun));
    }
    catch (const std::exception& ex)
    {
        fprintf(stderr, "Error is %s\n", ex.what());
        QFAIL("Exception thrown");
    }

    std::vector<std::pair<Coord, unsigned>> coords;
    coords.push_back({{0, 0}, 3});
    coords.push_back({{1, 2}, 1});
    coords.push_back({{5, 8}, 5});
    coords.push_back({{1, 3}, 2});
    coords.push_back({{5, 6}, 6});
    coords.push_back({{3, 7}, 7});

    try
    {
        Structure s{coords.begin(), coords.end()};
        s.author = "Bob";
        s.title = "Foo";
        s.date = "28/07/2001";
        s.desc = "Lorem ipsum";

        RLEStructureWriter writer;
        auto out = writer.save_structure(s);

        //printf("%s\n", out.c_str());

        RLEStructureReader reader(out);
        Structure struct_out = reader.read_structure();

        QVERIFY(std::is_permutation(struct_out.begin(), struct_out.end(), coords.begin()));
        //QCOMPARE(struct_out.author, s.author);
        //QCOMPARE(struct_out.date, s.date);
        QCOMPARE(struct_out.desc, s.desc);
        QCOMPARE(struct_out.title, s.title);
    }
    catch (const std::exception& ex)
    {
        fprintf(stderr, "Error is %s\n", ex.what());
        QFAIL("Exception thrown");
    }
}

void CellulutTests::test_json_structurewriter()
{
    std::vector<std::pair<Coord, unsigned>> coords;
    coords.push_back({{0, 0}, 3});
    coords.push_back({{1, 2}, 1});
    coords.push_back({{5, 8}, 5});

    try
    {
        Structure s{coords.begin(), coords.end()};
        s.author = "Bob";
        s.title = "Foo";
        s.date = "28/07/2001";
        s.desc = "Lorem ipsum";

        JSONStructureWriter writer;
        auto out = writer.save_structure(s);

        //printf("%s\n", out.c_str());

        JSONStructureReader reader(out);
        Structure struct_out = reader.read_structure();

        QVERIFY(std::is_permutation(struct_out.begin(), struct_out.end(), coords.begin()));
        QCOMPARE(struct_out.author, s.author);
        QCOMPARE(struct_out.date, s.date);
        QCOMPARE(struct_out.desc, s.desc);
        QCOMPARE(struct_out.title, s.title);
    }
    catch (const std::exception& ex)
    {
        fprintf(stderr, "Error is %s\n", ex.what());
        QFAIL("Exception thrown");
    }
}
