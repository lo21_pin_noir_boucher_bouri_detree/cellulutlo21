#ifndef CELLULUT_TESTS_HPP
#define CELLULUT_TESTS_HPP

#include <QtTest/QtTest>

class CellulutTests: public QObject
{
    Q_OBJECT
private slots:
    void test_property();
    void test_loader_saver_visitor();
    void test_structure();
    void test_factory();
    void test_coord();
    void test_history();
    void test_grid();
    void test_mooreNeighborhoodRule();
    //void test_neighborhood();

    void test_rle_structurereader();
    void test_json_structurereader();
    void test_rle_structurewriter();
    void test_json_structurewriter();

    void test_arbitrary_neighborhood_rule();

    void test_alphabet();

    void test_circulartransition();
    void test_lifegametransition();
    void test_totalistictransition();
    void test_nonisotropictransition();
    void test_VonNeumann();
};

#endif // CELLULUT_TESTS_HPP
