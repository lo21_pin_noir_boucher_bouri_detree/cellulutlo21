#include <QtTest/QtTest>

#include "cellulut_tests.hpp"

#include "property.hpp"

class PropertyClass : public HasUserProperties
{
public:
    DEFINE_CONFIGURABLE_PROPERTY(StringProperty, test_str, "Test String", "hello world");
    DEFINE_CONFIGURABLE_DYNLIST(CoordinateProperty, coords, "Coordinates", ());
};

void CellulutTests::test_property()
{
    PropertyClass test;

    QVERIFY(test.test_str.str == "hello world");
    QVERIFY(test.test_str.identifier() == "test_str");
    QVERIFY(test.test_str.display_name() == "Test String");
    QVERIFY(test.get_properties().size() == 2);
    QVERIFY(test.get_properties()[0].get() == &test.test_str);

    Neighborhood n;
    n.addNeighbor({0, 1}, 1);
    n.addNeighbor({-2, 1}, 1);
    n.addNeighbor({1, 1}, 2);

    test.coords.load_from_neighborhood(n);

    QCOMPARE(test.coords.size(), 3u);
    for (unsigned i = 0; i < test.coords.size(); ++i)
    {
        CoordinateProperty& coord = static_cast<CoordinateProperty&>(test.coords.at(i));
        QCOMPARE(coord.c, n.neighbor_at_index(i).first);
    }
}

