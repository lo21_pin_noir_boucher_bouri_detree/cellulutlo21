#ifndef NONISOTROPICTRANSITIONRULE_HPP
#define NONISOTROPICTRANSITIONRULE_HPP

#include <exception>

#include "transitionrule.hpp"
#include "mathexpr.hpp"

//! \brief Exception lancée lors de l'évaluation d'une règle totalistique.
class NonIsotropicRuleException : public std::exception
{
public:
    NonIsotropicRuleException(const std::string& str)
        : m_what(str)
    {}

    const char * what() const noexcept override
    { return m_what.c_str(); }

private:
    std::string m_what;
};

//! \brief Représente une entrée de règle de transition non isotropique configurable.
//! C'est à dire une entrée dans la table de transition permettant de déterminer si une transition s'effectue selon un état de départ et un voisinage donnés.
class NonIsotropicRuleEntry
{
public:
    //! Construit la règle à partir d'une chaîne contenant la définition de la règle.
    NonIsotropicRuleEntry(std::string rule_string, bool vertical_symmetry, bool horizontal_symmetry, bool rotate4);

    //! Détermine si l'entrée accepte l'état et le voisinage donnés.
    //! \param initial_state L'état de la cellule concernée.
    //! \param neighborhood Le voisinage de la cellule concernée.
    //! \param next Référence vers la valeur du prochain état de la cellule si l'entrée est acceptée.
    //! \returns true si accepté, false sinon.
    bool accept(unsigned initial_state, const Neighborhood& neighborhood, unsigned& next) const;

private:
    bool m_vertical_sym, m_horizontal_sym, m_rotate4;

    bool m_initial_state_is_variable;
    // could use a union here, but it's a bit tricky as we'd have to call the correct destructor
    char m_initial_variable;
    unsigned m_initial_state;
    std::vector<unsigned> m_constraints;
    MathExpr m_result_state;
};

//! \brief Représente une règle de transition totalistique configurable textuellement.
//! Le format d'une règle est tel que suit:
/**
## Format d'une règle de transition générale (non-isotropique)

    On associe un index (commençant) à 0 à chaque cellule du voisinnage, dans un ordre de gauche à droite et de haut en bas.
    Le format est alors presque identique, à la seule différence que au lieu d'intervalles associés à des états, on a juste un état attendu à chaque position du voisinage:

    etat_cellule, état_voisin_de_position_1, ..., état_voisin_de_position_1-> nouvel_etat

        par ex le voisinage :

    **
    *c*
    * *

    est énuméré ainsi:

    01
    2c3
    4 5

    et on peut créer par exemple cette règle:
    0, 0, 1, 0, 0, 1, 1 -> 1

Les règles sont traitées dans l'ordre de leur écriture, si deux règles pourraient s'appliquer à une cellule et à un voisinage, on applique la première dans l'ordre des lignes, du haut vers le bas.
*/
class NonIsotropicTransition : public TransitionRule
{
public:
    bool acceptFormat(const std::vector<NeighborhoodFormat>&) const override
    { return true; }
    unsigned int getState(unsigned int initial, const Neighborhood &neighborhood) const override;

private:
    void generate_entries() const;

private:
    mutable std::vector<NonIsotropicRuleEntry> m_entries;
    DEFINE_CONFIGURABLE_PROPERTY(StringProperty, rule_string, "Rule String");
    DEFINE_CONFIGURABLE_PROPERTY(HelpProperty, help_string, "Help",
                                 R"(
## Format d'une règle de transition générale (non-isotropique)

On associe un index (commençant) à 0 à chaque cellule du voisinnage, dans un ordre de gauche à droite et de haut en bas.
Le format est alors presque identique, à la seule différence que au lieu d'intervalles associés à des états, on a juste un état attendu à chaque position du voisinage:

```
etat_cellule, état_voisin_de_position_1, ..., état_voisin_de_position_1-> nouvel_etat
```

par exemple le voisinage :

```
**
*c*
* *
```

est énuméré ainsi:

```
01
2c3
4 5
```

et on peut créer par exemple cette règle:
```
0, 0, 1, 0, 0, 1, 1 -> 1
```

Les règles sont traitées dans l'ordre de leur écriture, si deux règles pourraient s'appliquer à une cellule et à un voisinage, on applique la première dans l'ordre des lignes, du haut vers le bas.
)");
};


#endif // NONISOTROPICTRANSITIONRULE_HPP
