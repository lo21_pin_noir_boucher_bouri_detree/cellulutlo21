#ifndef CIRCULARTRANSITION_HPP
#define CIRCULARTRANSITION_HPP

#include "transitionrule.hpp"

//! \brief Transition circulaire.
//! cf. http://joatdev.fr/CyclicAutomaton
class CircularTransition : public TransitionRule
{
public:
    bool acceptFormat(const std::vector<NeighborhoodFormat>&) const override
    { return true; }

    unsigned getState(unsigned cell, const Neighborhood& neighborhood) const override;

private:
    DEFINE_CONFIGURABLE_PROPERTY(IntegerProperty, states, "State count", 1);
    DEFINE_CONFIGURABLE_PROPERTY(IntegerProperty, threshold, "Threshold");
};

#endif // CIRCULARTRANSITION_HPP
