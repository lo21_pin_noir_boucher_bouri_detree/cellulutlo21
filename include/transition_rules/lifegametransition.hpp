#ifndef LIFEGAMETRANSITION_H
#define LIFEGAMETRANSITION_H

#include "transitionrule.hpp"

//! \brief Transition correspondant au jeu de la vie de Conway.
class LifeGameTransition : public TransitionRule
{
public:
    LifeGameTransition();
    bool acceptFormat(const std::vector<NeighborhoodFormat>&) const;
    unsigned int getState(unsigned int, const Neighborhood &) const;
};

#endif // LIFEGAMETRANSITION_H
