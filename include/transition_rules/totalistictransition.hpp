/**
\file totalistictransition.hpp
\date 25/05/2021
\author Yann Boucher
\version 1
\brief TotalisticTransitionRule

Cette classe représente une règle de transition totalistique configurable.
        **/
#ifndef TOTALISTICTRANSITIONRULE_HPP
#define TOTALISTICTRANSITIONRULE_HPP

#include <exception>
#include <string>
#include <map>
#include <limits>

#include "neighborhood.hpp"
#include "mathexpr.hpp"
#include "transitionrule.hpp"
#include "constantes.hpp"

//! \brief Exception lancée lors de l'évaluation d'une règle totalistique.
class TotalisticRuleException : public std::exception
{
public:
    TotalisticRuleException(const std::string& str)
        : m_what(str)
    {}

    const char * what() const noexcept override
    { return m_what.c_str(); }

private:
    std::string m_what;
};

//! \brief Représente un intervalle [low;high] composé d'expressions mathématiques
struct Interval
{
public:
    Interval(const MathExpr& i_low = MathExpr("0"))
    {
        has_upper_bound = false;
        low = i_low;

    }
    Interval(const MathExpr& i_low, const MathExpr& i_high)
    {
        has_upper_bound = true;
        low = i_low;
        high = i_high;
    }

    //! Evalue l'intervalle pour vérifier si val est inclus dedans
    //! \returns true si inclus, false sinon
    bool contains(unsigned val) const
    {
        unsigned low_v  =  low.eval();
        unsigned high_v =  has_upper_bound ? high.eval() : std::numeric_limits<unsigned>::max();

        // sanity check
        if (low_v > high_v)
            std::swap(low_v, high_v);

        return val >= low_v && val <= high_v;
    }

private:
    bool has_upper_bound;
    MathExpr low, high;
};

//! \brief Représente une entrée de règle de transition totalistique configurable.
//! C'est à dire une entrée dans la table de transition permettant de déterminer si une transition s'effectue selon un état de départ et un voisinage donnés.
class TotalisticRuleEntry
{
public:
    //! Construit la règle à partir d'une chaîne contenant la définition de la règle.
    TotalisticRuleEntry(std::string rule_string);

    //! Détermine si l'entrée accepte l'état et le voisinage donnés.
    //! \param initial_state L'état de la cellule concernée.
    //! \param neighborhood Le voisinage de la cellule concernée.
    //! \param next Référence vers la valeur du prochain état de la cellule si l'entrée est acceptée.
    //! \returns true si accepté, false sinon.
    bool accept(unsigned initial_state, const Neighborhood& neighborhood, unsigned& next) const;

private:
    class Constraint
    {
        public:
        Constraint(const MathExpr& e, const Interval& i)
            : state(e), interval(i)
        {
            std::fill(cache_bitmap.begin(), cache_bitmap.end(), false);
        }

        // initial_variable == '\0' si il n'y a pas de variable initiale
        bool valid(char initial_variable, unsigned initial_state, const Neighborhood& n) const
        {
            unsigned state_to_test;
            // valeur d'état à tester déjà dans le cache
            if (cache_bitmap[initial_state])
            {
                state_to_test = cache[initial_state];
            }
            else
            {
                cache_bitmap[initial_state] = true;
                if (initial_variable != '\0')
                    state.set_var(initial_variable, initial_state);
                state_to_test = cache[initial_state] = state.eval();
            }
            return interval.contains(n.getNb(state_to_test));
        }

        private:
        MathExpr state;
        Interval interval;
        mutable std::array<bool, MAX_ETATS> cache_bitmap;
        mutable std::array<uint8_t, MAX_ETATS> cache;
    };

private:
    bool m_initial_state_is_variable;
    // could use a union here, but it's a bit tricky as we'd have to call the correct destructor
    char m_initial_variable;
    unsigned m_initial_state;
    std::vector<Constraint> m_constraints;
    MathExpr m_result_state;
    mutable std::array<bool, MAX_ETATS> result_cache_bitmap;
    mutable std::array<uint8_t, MAX_ETATS> result_cache;
};

//! \brief Représente une règle de transition totalistique configurable textuellement.
//! Le format d'une règle est tel que suit:
/**
## Format d'une règle de transition isotropique:

etat_cellule, x:intervalle_voisins_d'etat_x, ... -> nouvel_etat

expression := <expression mathématique pouvant contenir une variable qu'on associerait à l'état initial>
intervalle := [expression..expression] | [expression] | '*'

Exemple automate cellulaire cyclique de Griffith:

i, (i+1)%3:[3] -> (i+1)%3

On pourrait aussi ajouter une constante 'N' qui correspondrait au nombre d'états de la règle:

i, (i+1)%N:[3] -> (i+1)%N

Exemple jeu de la vie:

0,0:*     ,1:[3]->1 (une cellule morte devient vivante si elle a exactement 3 voisisin vivants)
1,0:[0..1],1:*  ->0 (une cellule vivante meurt si elle a 0 ou 1 voisins vivants)
1,0:[4..*],1:*  ->0 (une cellule vivante meurt si elle a 4 ou plus voisins vivants)

Exemple Wireworld: (https://xalava.github.io/WireWorld/)

1->2
2->3
3,1:[1..2]->1

Exemple Brain's Brain: (https://www.conwaylife.com/wiki/OCA:Brian%27s_Brain)
0,1:[2]->1
2->0

Si il n'y a pas de règle correspondat à une cellule et son voisinage, alors cette cellule ne change pas d'état.
Les intervalles non mentionnés dans la règle sont considérés par défaut comme des '*'.

Les règles sont traitées dans l'ordre de leur écriture, si deux règles pourraient s'appliquer à une cellule et à un voisinage, on applique la première dans l'ordre des lignes, du haut vers le bas.
*/
class TotalisticTransition : public TransitionRule
{
public:
    bool acceptFormat(const std::vector<NeighborhoodFormat>&) const override
    { return true; }
    unsigned int getState(unsigned int initial, const Neighborhood &neighborhood) const override;

private:
    void generate_entries() const;

private:
    mutable std::vector<TotalisticRuleEntry> m_entries;
    DEFINE_CONFIGURABLE_PROPERTY(StringProperty, rule_string, "Rule String");
    DEFINE_CONFIGURABLE_PROPERTY(HelpProperty, help_string, "Help",
                                 R"(
## Format d'une règle de transition isotropique:

etat_cellule, x:intervalle_voisins_d'etat_x, ... -> nouvel_etat

expression := <expression mathématique pouvant contenir une variable qu'on associerait à l'état initial>
intervalle := [expression..expression] | [expression] | '*'

Exemple automate cellulaire cyclique de Griffith:

```
i, (i+1)%3:[3] -> (i+1)%3
```

Exemple jeu de la vie:

```
0,0:*     ,1:[3]->1 (une cellule morte devient vivante si elle a exactement 3 voisisin vivants)
1,0:[0..1],1:*  ->0 (une cellule vivante meurt si elle a 0 ou 1 voisins vivants)
1,0:[4..*],1:*  ->0 (une cellule vivante meurt si elle a 4 ou plus voisins vivants)
```

Exemple Wireworld: (https://xalava.github.io/WireWorld/)

```
1->2
2->3
3,1:[1..2]->1
```

Exemple Brain's Brain: (https://www.conwaylife.com/wiki/OCA:Brian%27s_Brain)

```
0,1:[2]->1
2->0
```

Si il n'y a pas de règle correspondat à une cellule et son voisinage, alors cette cellule ne change pas d'état.
Les intervalles non mentionnés dans la règle sont considérés par défaut comme des '*'.

Les règles sont traitées dans l'ordre de leur écriture, si deux règles pourraient s'appliquer à une cellule et à un voisinage, on applique la première dans l'ordre des lignes, du haut vers le bas.

)");
};

#endif // TOTALISTICTRANSITIONRULE_HPP
