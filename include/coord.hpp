/**
\file coord.hpp
\date 15/04/2021
\author Yann Boucher
\version 1
\brief Coord

Contient la structure Coord, représentant une coordonnée sur la grille de cellules.
**/

#ifndef COORD_HPP
#define COORD_HPP

/**
\struct Coord
\brief Représente une coordonnée

Cette structure représente une coordonnée de format (x, y) sur la grille de cellules.\n
Cette coordonée est une paire d'entiers potentiellement négatifs. Charge au code de gérer les cas de composantes négatives, pour s'assurer d'un cormportement de grille similaire à un tore.
**/
struct Coord
{
    int x, y;
};

//! \brief Teste si deux coordonnées sont égales (même 'x' et 'y')
inline bool operator==(const Coord& lhs, const Coord& rhs)
{
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

//! \brief Teste si deux coordonnées sont différentes
inline bool operator!=(const Coord& lhs, const Coord& rhs)
{
    return !(lhs == rhs);
}

//! \brief Comparaison lexicographique de deux coordonnées (pour std::map et compagnie)
inline bool operator<(const Coord& lhs, const Coord& rhs)
{
    if (lhs.y == rhs.y)
        return lhs.x < rhs.x;
    return lhs.y < rhs.y;
}

//! \brief Addition de coordonnées
inline Coord operator+(const Coord& lhs, const Coord& rhs)
{
    return {lhs.x + rhs.x, lhs.y + rhs.y};
}

//! \brief Soustraction de coordonnées
inline Coord operator-(const Coord& lhs, const Coord& rhs)
{
    return {lhs.x - rhs.x, lhs.y - rhs.y};
}

//! \brief Retourne une coordonnée ajustée par rapport à un réseau périodique de taille (cols, rows)
inline Coord wrap_coords(const Coord& coord, unsigned cols, unsigned rows)
{
    return Coord{ (int)((coord.x%cols+cols)%cols),
                  (int)((coord.y%rows + rows)%rows) };
}

#endif // COORD_HPP
