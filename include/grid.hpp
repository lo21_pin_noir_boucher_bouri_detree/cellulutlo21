/**
\file grid.h
\date 24/04/2021
\author Merwane Bouri
\version 1
\brief Grid

Cette classe représente un réseau de cellules.

        **/

#ifndef GRID_H
#define GRID_H

#include <iostream>
#include <vector>

#include "coord.hpp"

    using namespace std;

/**
\class Grid
\brief Représente une grille d'états.

Cette structure représente une grille d'états.
        **/

    class Structure;

//! \brief Enum spécifiant la stratégie à adopter aux frontières du réseau.
enum class BoundaryPolicy
{
    //! \brief Frontière torique.
    Periodic,
    //! \brief Frontière toujours égale à l'état 0.
    Inert
};

class Grid{
    int nb_rows;
    int nb_col;
    std::vector<unsigned char> matrix;
    BoundaryPolicy policy;
public:
    //! \brief Constructeur par défaut, avec le nombre de ligne et de colonne souhaités
    Grid(size_t l,size_t c);

    //! \brief Retourne le nombre de lignes
    //! \return Retourne le nombre de lignes de la Grille
    size_t get_rows() const {return nb_rows;}

    //! \brief Retourne le nombre de colonnes
    //! \return Retourne le nombre de colonnes de la Grille
    size_t get_col() const{return nb_col;}

    //! \brief Configure la stratégie à adopter par rapport aux frontières du réseau.
    void set_boundary_policy(BoundaryPolicy p)
    { policy = p; }

    //! \brief Initialise la cellule de coordonnée pos à l'état 'state'
    void set_cell(Coord pos, unsigned int state)
    {
        // fast path, no boundaries concerned
        if (pos.x >= 0 && pos.y >= 0 && pos.x < nb_col && pos.y < nb_rows)
        {
            matrix[pos.y*nb_col + pos.x] = state;
            return;
        }
        else
        {
            if (policy == BoundaryPolicy::Inert)
                return;
            else
            {
                int i = pos.y;
                int j = pos.x;
                matrix[((i%nb_rows + nb_rows)%nb_rows)*nb_col
                       +((j%nb_col+nb_col)%nb_col)]
                    =state;
            }
        }
    }

    //! \brief Retourne l'état d'une cellule
    //! \return Retourne l'état d'une cellule de coordonnées pos
    unsigned int get_state(Coord pos) const {
        // fast path, no boundaries concerned
        if (pos.x >= 0 && pos.y >= 0 && pos.x < nb_col && pos.y < nb_rows)
        {
            return matrix[pos.y*nb_col + pos.x];
        }
        else
        {
            if (policy == BoundaryPolicy::Inert)
                return 0;
            else
            {
                int i = pos.y;
                int j = pos.x;
                return matrix[((i%nb_rows + nb_rows)%nb_rows)*nb_col
                              +((j%nb_col+nb_col)%nb_col)];
            }
        }
    }
    //! \brief Recopie de la grille
    Grid& operator=(const Grid& g);

    //! \brief Vérifie si deux grilles sont identiques
    bool operator==(const Grid&) const;

    //! \brief Retourne une structure représentant le contenu non-nul de la Grid (les cellules d'état != 0), pour un enregistrement dans un fichier par exemple.
    Structure to_structure() const;
};
ostream& operator<<(ostream& f, const Grid& g);

#endif // GRID_H
