/**
\file vonNeumannNeighborhoodRule.hpp
\date 13/05/2021
\author Eugene Pin
\version 1
\brief vonNeumannNeighborhoodRule

Contient la classe de la règle de Von Neumann.
**/

#ifndef VONNEUMANNNEIGHBORHOODRULE_HPP
#define VONNEUMANNNEIGHBORHOODRULE_HPP

#include "neighborhoodrule.hpp"
#include "neighborhood.hpp"

//! \brief Voisinage de Von Neumann.
class vonNeumannNeighborhoodRule : public NeighborhoodRule
{
    mutable NeighborhoodFormat format;
    mutable int current_format_radius;
public:
    vonNeumannNeighborhoodRule(int _radius = 1);

    //! \brief Retourne l'ensemble des voisins d'une cellule de position 'pos' sur la grille 'grid' sous la forme d'une structure 'Neighborhood'
    //! \return L'ensemble des voisins de la cellule à 'pos'
    Neighborhood getNeighborhood(const Grid& grid, Coord pos) const;

    //! \brief Retourne 'format' de VonNeumann dans un vecteur (méthode dérivée)
    //! \return Retourne les formats de voisinage possible dans un std::vector.
    std::vector<NeighborhoodFormat> getFormats() const;

private:
    void update_format() const;

private:
    //! \brief Rayon du voisinage (en nombre de cellule)
    DEFINE_CONFIGURABLE_PROPERTY(IntegerProperty, radius, "Radius", 1);
};

#endif // VONNEUMANNNEIGHBORHOODRULE_HPP
