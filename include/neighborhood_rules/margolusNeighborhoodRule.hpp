/**
\file vonNeumannNeighborhoodRule.hpp
\date 13/05/2021
\author Eugene Pin
\version 1
\brief vonNeumannNeighborhoodRule

Contient la classe de la règle de Von Neumann.
**/

#ifndef MARGOLUSNEIGHBORHOODRULE_HPP
#define MARGOLUSNEIGHBORHOODRULE_HPP

#include "neighborhoodrule.hpp"
#include "neighborhood.hpp"

//! \brief Voisinage de Margolus.
class MargolusNeighborhoodRule : public NeighborhoodRule
{
public:
    MargolusNeighborhoodRule();

    //! \brief Retourne l'ensemble des voisins d'une cellule de position 'pos' sur la grille 'grid' sous la forme d'une structure 'Neighborhood'
    //! \return L'ensemble des voisins de la cellule à 'pos'
    Neighborhood getNeighborhood(const Grid& grid, Coord pos) const;

    //! \brief Retourne 'format' de VonNeumann dans un vecteur (méthode dérivée)
    //! \return Retourne les formats de voisinage possible dans un std::vector.
    std::vector<NeighborhoodFormat> getFormats() const;

    virtual void update_generation(unsigned gen)
    { parity = gen%2; }

private:
    std::vector<NeighborhoodFormat> formats;
    unsigned parity;
};

#endif // MARGOLUSNEIGHBORHOODRULE_HPP
