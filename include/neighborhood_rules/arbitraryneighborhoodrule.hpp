/**
\file arbitraryneighborhoodrule.hpp
\date 11/05/2021
\author Yann Boucher
\version 1
\brief ArbitraryNeighborhoodRule

Représente un voisinage arbitraire, défini par l'utilisateur.
        **/

#ifndef ARBITRARYNEIGHBORHOODRULE_HPP
#define ARBITRARYNEIGHBORHOODRULE_HPP

#include "neighborhoodrule.hpp"
#include "property.hpp"

//! \brief Règle de voisinage permettant l'utilisation de voisins arbitraires.
class ArbitraryNeighborhoodRule : public NeighborhoodRule
{
public:
    Neighborhood getNeighborhood(const Grid& grid, Coord pos) const override;

    std::vector<NeighborhoodFormat> getFormats() const override;

private:
    DEFINE_CONFIGURABLE_DYNLIST (CoordinateProperty, neighbors, "Neighbors", ());
};

#endif // ARBITRARYNEIGHBORHOODRULE_HPP
