/**
\file property.hpp
\date 17/04/2021
\author Yann Boucher
\version 1
\brief Property

Fichier définissant la classe Property représentant une propriété chargable, sauvegardable et affichable d'un objet.

**/


#ifndef PROPERTY_HPP
#define PROPERTY_HPP

#include <string>
#include <vector>
#include <memory>
#include <limits>
#include <exception>
#include <cassert>

#include <QWidget>
#include <QLayout>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QMessageBox>
#include <QFormLayout>

#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>

#include "coord.hpp"
#include "neighborhood.hpp"
#include "neighborhoodDialog.hpp"

struct HelpProperty;
struct StringProperty;
struct IntegerProperty;
struct CoordinateProperty;
struct PropertyList;

/**
\class PropertyVisitor
\brief Représente un visiteur de propriété

Visiteur des différentes classes filles de Property. Destinée à être héritée et concrétisée pour par exemple sauvegarder, charger ou afficher une propriété.
**/
class PropertyVisitor
{
public:
    virtual ~PropertyVisitor() = default;

public:
    virtual void visit(StringProperty& str) = 0;
    virtual void visit(IntegerProperty& str) = 0;
    virtual void visit(CoordinateProperty& coord) = 0;
    virtual void visit(PropertyList& str) = 0;
    virtual void visit(HelpProperty& str) = 0;
};

/**
\class Property
\brief Représente une propriété

Une Property est un attribut membre d'une classe, pouvant être facilement visité par des PropertyVisitor afin de permettre facilement par exemple un affichage à l'aide de widgets des propriétés d'un objet, et facilitant ainsi le paramètrage par l'utilisateur d'un tel objet.
**/
class Property
{
    friend class HasUserProperties;

public:
    //! \brief Constructeur virtuel par défaut.
    virtual ~Property() = default;

    //! \brief Accepte un visiteur. Cette fonction est automatiquement redéfinie correctement en héritant de PropertyImpl<Classe>?
    virtual void accept(PropertyVisitor& v) = 0;

    //! \brief Retourne le nom identifiant propriété.
    std::string identifier() const
    { return m_identifier; }

    //! \brief Retourne le nom d'affichage de la propriété, qui est purement cosmétique.
    std::string display_name() const
    { return m_display_name; }

private:
    std::string m_identifier;
    std::string m_display_name;
};

/**
\class PropertyImpl
\brief Classe utilitaire pour les accepteur de PropertyVisitor.
Classe utilitaire permettant de définir automatiquement 'accept' parmi les classes implémentant un type de Property.
 */
template <typename Child>
class PropertyImpl : public Property
{
public:
    /**
     \internal
     \brief Accepte le vitieur passé en argument afin de faire fonctionner le double-dispatch.
    **/
    virtual void accept(PropertyVisitor& v)
    {
        v.visit(static_cast<Child&>(*this));
    }
    //! \endinternal
};

/**
\class HasUserProperties
\brief Représente une classe possédant des Property.
Permet à une classe héritant de HasUSerProperties d'avoir des objets Property fonctionnels.
 **/
class HasUserProperties
{
public:
    //! \brief Retourne l'ensemble des Property associées à l'objet.
    const std::vector<std::unique_ptr<Property>>& get_properties() const
    {
        return m_properties;
    }

protected:
    //! \brief Appelée internalement par les macros DEFINE_CONFIGURABLE_*.
    template <typename PropertyType, typename... Args>
    PropertyType& register_property(const std::string& prop_name, const std::string& display_name, Args&&... args)
    {
        m_properties.emplace_back(std::make_unique<PropertyType>(std::forward<Args>(args)...));
        m_properties.back()->m_identifier = prop_name;
        m_properties.back()->m_display_name = display_name;
        return static_cast<PropertyType&>(*m_properties.back());
    }

private:
    std::vector<std::unique_ptr<Property>> m_properties;
};

/**
\class StringProperty

\brief Représente une propriété textuelle.
**/
struct StringProperty : public PropertyImpl<StringProperty>
{
public:
    //! \brief Constructeur par défaut.
    StringProperty() = default;
    /** \brief Constructeur avec valeur initiale.
        \param initval Valeur initiale.
    **/
    StringProperty(const std::string& initval)
        : str(initval)
    {}

public:
    //! \brief La chaîne de caractères.
    std::string str;
};

/**
\class HelpProperty

\brief Représente un bouton permettant l'affichage d'un manuel.
**/
struct HelpProperty : public PropertyImpl<HelpProperty>
{
public:
    /** \brief Constructeur avec valeur initiale.
        \param initval Valeur initiale.
    **/
    HelpProperty(const std::string& initval = "")
        : str(initval)
    {}

public:
    //! \brief La chaîne de caractères contenant l'aide.
    std::string str;
};

/**
\class IntegerProperty

\brief Représente une propriété entière bornée.
**/
struct IntegerProperty : public PropertyImpl<IntegerProperty>
{
public:
    //! \brief Construit un objet IntegerProperty avec une valeur min et une valeur max, non borné par défaut.
    IntegerProperty(int min_value = std::numeric_limits<int>::min(), int max_value = std::numeric_limits<int>::max())
        : val(min_value), prop_min(min_value), prop_max(max_value)
    {}

public:
    //! \brief La valeur de la propriété.
    int val = 0;
    //! \brief Les valeurs minimales et maximales de la propriété.
    const int prop_min, prop_max;
};

/**
\class CoordinateProperty

\brief Représente une coordonnée relative 2D.
**/
struct CoordinateProperty : public PropertyImpl<CoordinateProperty>
{
public:
    CoordinateProperty() = default;
    CoordinateProperty(Coord arg) : c(arg)
    {}

    //! \brief La valeur de la coordonnée.
    Coord c;
};

/**
\class PropertyList

\brief Représente une liste de propriétés.
**/
struct PropertyList : public PropertyImpl<PropertyList>
{
public:
    //! \brief Appelée internalement par la macro de création de liste, les arguments après is_dynamic sont accessibles depuis la macro, en arguments variables.
    //! \param in_min_size La taille minimale de la liste.
    //! \param in_max_size La taille maximale de la liste.
    PropertyList(std::unique_ptr<Property>(*create_function)(void), bool is_dynamic = false, int in_min_size = 0, int in_max_size = std::numeric_limits<int>::max())
        : dynamic(is_dynamic), min_size(in_min_size), max_size(in_max_size), m_create_function(create_function)
    {
        assert(m_create_function);
        for (int i = 0; i < min_size; ++i)
            push_back();
    }

    //! \brief Retourne true si la liste est vide, false sinon.
    //! \returns true si la liste est vide, false sinon.
    bool empty() const
    { return contents.empty(); }
    //! \brief Vide la liste.
    void clear()
    { contents.clear(); }
    //! \brief Retourne la taille actuelle de la liste.
    //! \returns La taille de la liste.
    size_t size() const
    { return contents.size(); }
    //! \brief Insère un élément dans la liste.
    //! \returns Une référence vers la propriété insérée.
    Property& push_back()
    {
        if ((int)size() < max_size)
            contents.emplace_back(m_create_function());
        return back();
    }
    //! \brief Enlève l'élément le plus à droite de la liste.
    void pop_back()
    {
        if (dynamic && !contents.empty() && (int)size() > min_size)
            contents.pop_back();
    }
    //! \brief Récupère l'élément le plus à droite de la liste.
    //! \returns Une référence vers cet élément.
    Property& back()
    { return *contents.back(); }
    //! \brief Récupère l'élément à un indice donné.
    //! \param i L'indice.
    //! \returns Une référence vers cet élément.
    //! \exception std::out_of_range si l'indice est invalide.
    Property& at(size_t i)
    {
        if (i < size())
            return *contents[i];
        else
            throw std::out_of_range("Invalid index value");
    }

    //! \brief Charge un voisinage dans la PropertyList
    void load_from_neighborhood(const Neighborhood& n)
    {
        clear();
        for (unsigned i = 0; i < n.size(); ++i)
        {
            Property& prop = push_back();
            CoordinateProperty& coord_p = static_cast<CoordinateProperty&>(prop);
            coord_p = n.neighbor_at_index(i).first;
        }
    }

    //! \brief Retourne les coordonées comme un Neighborhood, les cellules voisines étant d'état 1
    Neighborhood to_neighborhood() const
    {
        Neighborhood n;
        for (const auto& ptr : contents)
        {
            Property& prop = *ptr;
            CoordinateProperty& coord_p = static_cast<CoordinateProperty&>(prop);
            n.addNeighbor(coord_p.c, 1);
        }

        return n;
    }

public:
    //! \brief Vrai si la liste est extensible, faux si elle est fixe.
    const bool dynamic;
    //! \brief Taille minimale.
    const int min_size;
    //! \brief Taille maximale.
    const int max_size;
    //! \brief Le vector contenant les éléments de la liste.
    std::vector<std::unique_ptr<Property>> contents;
private:
    std::unique_ptr<Property>(*m_create_function)(void);
};

/** Permet de déclarer une variable propriété 'identifier' de type 'prop_type', avec comme nom affichable 'name'.
 \param prop_type Type de la propriété.
 \param identifier Nom de la variable dans le code.
 \param name Nom affichable de la propriété.
 \param ... Arguments supplémentaires à passer au constructeur de 'prop_type'
**/
#define DEFINE_CONFIGURABLE_PROPERTY(prop_type, identifier, display_name, ...) \
prop_type& identifier = register_property<prop_type>(#identifier, display_name, ##__VA_ARGS__)

/** Permet de déclarer une liste fixée de propriétés 'identifier' de type 'prop_type', avec comme nom affichable 'name'.
 \param prop_type Type des propriétés.
 \param identifier Nom de la variable dans le code.
 \param name Nom affichable de la liste de propriétés.
 \param ... Arguments supplémentaires à passer au constructeur de 'prop_type' lors de la créatin d'une propriété dans la liste
**/
#define DEFINE_CONFIGURABLE_LIST(prop_type, identifier, display_name, ...) \
PropertyList& identifier = register_property<PropertyList>(#identifier, display_name, []()->std::unique_ptr<Property>{ return std::make_unique<prop_type>(__VA_ARGS__);  false, ##__VA_ARGS__ })
/** Permet de déclarer une liste dynamique de propriétés 'identifier' de type 'prop_type', avec comme nom affichable 'name'.
 \param prop_type Type des propriétés.
 \param identifier Nom de la variable dans le code.
 \param name Nom affichable de la liste de propriétés.
 \param inner_args Arguments supplémentaires à passer au constructeur de 'prop_type' lors de la créatin d'une propriété dans la liste
 \param ... Arguments supplémentaires à passer au constructeur de PropertyList.
**/
#define DEFINE_CONFIGURABLE_DYNLIST(prop_type, identifier, display_name, inner_args, ...) \
PropertyList& identifier = register_property<PropertyList>(#identifier, display_name, []()->std::unique_ptr<Property>{ return std::make_unique<prop_type> inner_args; }, true, ##__VA_ARGS__ )

#endif // PROPERTY_HPP
