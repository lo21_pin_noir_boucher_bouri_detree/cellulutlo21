#ifndef MODELLOADINGDIALOG_HPP
#define MODELLOADINGDIALOG_HPP

#include <QDialog>

#include <QDir>
#include <QTreeWidgetItem>
#include <QFileSystemWatcher>
#include <QJsonObject>

namespace Ui {
class ModelLoadingDialog;
}

//! \brief Exception lancée lors d'une erreur de chargement de modèle.
class ModelLoadingException : public std::exception
{
    std::string _msg;
public:
    ModelLoadingException(const std::string& msg) : _msg(msg){}

    virtual const char* what() const noexcept override
    {
        return _msg.c_str();
    }
};

//! \brief Fenêtre de dialogue permettant de charger un modèle.
class ModelLoadingDialog : public QDialog
{
    Q_OBJECT

public:
    //! Construit le ModelLoadingDialog à partir d'un parent.
    explicit ModelLoadingDialog(QWidget *parent = nullptr);
    ~ModelLoadingDialog();

    //! \brief Retoure le modèle choisi en tant qu'objet JSON.
    QJsonObject model() const;

protected:
    void done(int r);

private:
    void load_models();
    QTreeWidgetItem *add_directory_contents(const QDir& dir);

private slots:
    void update_info(QTreeWidgetItem* item, int column);

private:
    Ui::ModelLoadingDialog *ui;
    QFileSystemWatcher m_watcher;
    QJsonObject m_current_model;
};

#endif // MODELLOADINGDIALOG_HPP
