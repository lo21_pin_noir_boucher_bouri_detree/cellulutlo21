/**
\file alphabet.hpp
\date 01/05/2021
\author Arthur Detree
\version 1
\brief Alphabet

Représente l'ensemble des états et de leurs informations (couleur, label) d'une configuration.
                                                             **/

#ifndef ALPHABET_H
#define ALPHABET_H

#include "state.hpp"
#include "stateColor.hpp"

#include <exception>
#include <vector>

//! \brief Exception lancée lors d'une erreur de manipulation de l'alphabet.
class AlphabetException : public std::exception
{
public:
    AlphabetException(const std::string& what)
        : m_what(what)
    {}

    const char* what() const noexcept
    { return m_what.c_str(); }

private:
    const std::string m_what;
};

//! \brief Représente un alphabet, c'est à dire un ensemble d'état et leurs propriétés.
class Alphabet {
    //! Tableau des états
    std::vector<state> etats;

public:
    //! Constructeur de l'Alphabet
    //! Un alphabet possède toujours au minimum un état nul, indiqué par null_state
    //! \param null_state L'état nul
    Alphabet(const state& null_state = state{stateColor{255, 255, 255}}){
        newEtat(null_state); // Un alphabet possède toujours un état nul
    }

    //! Renvoie l'alphabet
    static Alphabet& getAlphabet() {static Alphabet a; return a;}

    //! Renvoie le nombre d'états de l'alphabet
    unsigned taille() const { return etats.size(); }

    //! Crée un nouvel état
    void newEtat(const state& s);

    //! Renvoie l'état en fonction de son identifiant
    const state& getState(unsigned int it) const;

    //! Affecte l'identifiant d'état à un état
    void setState(unsigned int i, const state& s);

    //! \brief Supprime les états de l'alphabet.
    void clearAlphabet() {etats.clear();}

    //! \brief Supprime le dernier élément ajouté.
    void deleleLastState() {etats.pop_back();}


};

#endif // ALPHABET_H
