/**
\file structurelibraryview.hpp
\author Yann Boucher
\version 1
\brief StructureLibraryView

Widget de la bibliothèque de structures.
        **/
#ifndef STRUCTURELIBRARYVIEW_HPP
#define STRUCTURELIBRARYVIEW_HPP

#include <QFrame>

#include <QDir>
#include <QTreeWidgetItem>
#include <QFileSystemWatcher>

#include "structure.hpp"
#include "alphabet.hpp"

namespace Ui {
class StructureLibraryView;
}

//! \brief Widget de la bibliothèque de structures.
//! Ce widget représente la bibliothèque de structure, ses contrôles et l'affichage des données ex. description et titre.
class StructureLibraryView : public QFrame
{
    Q_OBJECT

public:
    //! \brief Construit le widhet à partir du parent.
    explicit StructureLibraryView(QWidget *parent = nullptr);
    ~StructureLibraryView();

    //! \brief Spécifie l'alphabet à utiliser pour les couleurs de la preview
    void set_alphabet(const Alphabet& alph)
    { m_alph = alph; }

signals:
    //! \brief Signal émis lorsqu'une structure de la bibliothèque est copiée.
    //! \param s La structure copiée.
    void structure_copied(const Structure& s);

public slots:
    //! \brief Permet de mettre à jour la taille en pixels d'une cellule, pour que l'affichage d'un drag&drop soit correct
    //! \param size Côté en pixels.
    void update_cell_pixel_size(unsigned size);

private:
    void load_structures();
    QTreeWidgetItem *add_directory_contents(const QDir& dir);
    bool try_load_structure(const QString& filename, Structure& s);
    void update_preview(const Structure& s);
    QImage create_preview_image(const Structure &s, bool transparent);
    void create_drag(QTreeWidgetItem* item, int column);

private slots:
    void update_info(QTreeWidgetItem* item, int column);
    void copy_button_clicked();

private:
    Ui::StructureLibraryView *ui;
    QFileSystemWatcher m_watcher;
    unsigned m_cell_pixel_size;
    Alphabet m_alph;
};

#endif // STRUCTURELIBRARYVIEW_HPP
