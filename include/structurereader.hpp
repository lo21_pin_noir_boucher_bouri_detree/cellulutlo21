/**
\file structurereader.hpp
\date 07/05/2021
\author Yann Boucher
\version 1
\brief StructureReader

Fichier contenant la classe StructureReader et ses filles, permettant de lire une Structure à partir de différents formats de fichier.

**/

#ifndef STRUCTUREREADER_HPP
#define STRUCTUREREADER_HPP

#include <string>
#include <exception>

class Structure;

//! \brief Exception lancée lors d'un chargement de structure.
class StructureReaderException : public std::exception
{
public:
    StructureReaderException(const std::string& what)
        : m_what(what)
    {}

    const char* what() const noexcept
    { return m_what.c_str(); }

private:
    const std::string m_what;
};

//! \class StructureReader
//! \brief Classe abstraite permettant de lire une Structure à partir de différents formats de fichier.
class StructureReader
{
public:
    //! \brief Constructeur prenant en argument les données du fichier à lire sous forme de std::string
    StructureReader(const std::string& in_data):
          m_idx(0), m_data(in_data)
    {}
    //! \brief Retourne la structure lue.
    //! \exception StructureReaderException si il y a une erreur lors de la lecture
    virtual Structure read_structure() = 0;

protected:
    //! \brief Retourne le caractère au niveau du curseur actuel.
    char peek() const;
    //! \brief Lit le caractère actuel sous le curseur, et avance le curseur.
    char read();
    //! \brief Retourne vrai si l'entièreté des données a été lue, faux sinon.
    bool eof() const;
    //! \brief Renvoie une exception si les données sous le curseur actuelles sont différentes de str, avance le curseur sinon.
    void expect(const std::string& str);
    //! \brief Retourne vrai si ls données sous le curseur correspondent à str et avance le curseur, retourne faux sinon.
    bool accept(const std::string& str);
    //! \brief Lit un mot délimité par des espaces dans l'entrée.
    std::string read_word();
    //! \brief Lit la ligne actuelle dans l'entrée, à partir du curseur.
    std::string read_line();
    //! \brief Retourne toutes les données situées à partir du curseur.
    std::string data_left() const;
    //! \brief Lit un entier dans l'entrée.
    //! \exception StructureReaderException si il y a une erreur lors de la lecture de l'entier
    int read_int();
    //! \brief Avance le curseur jusqu'au premier caractère qui n'est pas un espace.
    void read_white();

private:
    size_t m_idx;
    std::string m_data;
};

//! \brief Lit une structure au format Extended RLE de Golly (https://www.conwaylife.com/wiki/Run_Length_Encoded)
class RLEStructureReader : public StructureReader
{
public:
    //! \brief Constructeur prenant en argument les données du fichier à lire sous forme de std::string
    RLEStructureReader(const std::string& in_data):
          StructureReader(in_data)
    {}
    virtual Structure read_structure();

private:
    unsigned read_state();
    void read_comment_line(Structure& s);
};

//! \brief Lit une structure au format JSON.
class JSONStructureReader : public StructureReader
{
public:
    //! \brief Constructeur prenant en argument les données du fichier à lire sous forme de std::string
    JSONStructureReader(const std::string& in_data):
          StructureReader(in_data)
    {}
    virtual Structure read_structure();
};

//! \brief Lit une structure à partir d'un chemin de fichier, en déterminant automatiquement le type de format de structure
//! \exception StructureReaderException en cas de problème
Structure load_structure(const std::string& path);

#endif // STRUCTUREREADER_HPP
