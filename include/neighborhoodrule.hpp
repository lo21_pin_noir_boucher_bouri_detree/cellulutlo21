/**
\file neighborhoodrule.hpp
\date 15/04/2021
\author Yann Boucher
\version 1
\brief NeighborhoodRule

Cette classe représente le concept d'une règle de voisinage comme Von Neumann ou Margolus. Pour une grille et une position données, elle renvoie l'ensemble de ses voisins sous la forme d'une structure 'Neighbor'.
Cette classe peut aussi fournir l'ensemble des positions de voisins qu'elle peut potentiellement retourner.

**/

#ifndef NEIGHBORHOODRULE_HPP
#define NEIGHBORHOODRULE_HPP

#include <vector>

#include "coord.hpp"
#include "grid.hpp"
#include "neighborhood.hpp"
#include "factory.hpp"
#include "property.hpp"

/**
\struct NeighborhoodFormat
\brief Représente un format de voisinage

Cette structure représente un format possible de voisinage pouvant être renvoyé par NeigbhorhoodRule.
Son unique membre 'positions' contient la liste des coordonées des voisins d'une cellule donnée, selon cette règle.
**/
struct NeighborhoodFormat
{
    std::vector<Coord> positions;
};

//! \brief Représente une règle de voisinage.
class NeighborhoodRule : public HasUserProperties
{
public:
    virtual ~NeighborhoodRule() = default;
    //! \brief Retourne l'ensemble des voisins d'une cellule de position 'pos' sur la grille 'grid' sous la forme d'une structure 'Neighborhood'
    //! \return L'ensemble des voisins de la cellule à 'pos'
    virtual Neighborhood getNeighborhood(const Grid& grid, Coord pos) const = 0;

    //! \brief Retourne l'ensemble des formats pouvant être renvoyés par cette règle. Dans la majorité des cas, une règle ne retourne qu'un seul voisinage; cette méthode existe pour pouvoir gérer les cas comme le voisinage de Margolus, qui varie selon la parité de la génération.
    //! \return Retourne les formats de voisinage possible dans un std::vector.
    virtual std::vector<NeighborhoodFormat> getFormats() const = 0;

    //! \brief Change le numéro de génération de la règle de voisinage d'une génération
    //! Fait avancer ou reculer l'état de la règle de voisinage d'une génération. Cette fonction est utilisée pour les voisinages tels que celui de Margolus, qui dépendent de la parité de la génération actuelle.
    //! \remark L'implémentation par défaut ne fait rien.
    virtual void update_generation(unsigned gen) { (void)gen; }
};

#endif // NEIGHBORHOODRULE_HPP
