#ifndef CONSTANTES_HPP
#define CONSTANTES_HPP

#define MAX_ETATS 32
#if MAX_ETATS > 255
#error MAX_ETATS ne tient pas dans un unsigned char
#endif

#define MAX_VOISINS 256

#endif // CONSTANTES_HPP
