/**
\file simulation.hpp
\brief Simulation

Cette classe représente un automate cellulaire dans le temps.

**/

#ifndef SIMULATION_HPP
#define SIMULATION_HPP

#include "history.hpp"
#include "grid.hpp"
#include "automaton.hpp"

//! \brief Cette classe représente un automate cellulaire dans le temps.
class Simulation {
private:
    bool canRun;
    unsigned int time;
    int period;
    Automaton automaton;
    Grid startGrid;
    History hist;

public:
    Simulation();
    virtual ~Simulation();

    //! \brief Définit la règle de voisinage
    //!
    //! La règle est passée à l'automate de la simulation qui se charge de la destruction de ses règles. Aucune copie n'est faite.
    //! \param NR La nouvelle règle de voisinage
    void setNeighborhoodRule(NeighborhoodRule*);

    //! \brief Définit la règle de transition
    //!
    //! La règle est passée à l'automate de la simulation qui se charge de la destruction de ses règles. Aucune copie n'est faite.
    //! \param TR La nouvelle règle de transition
    void setTransitionRule(TransitionRule*);

    //! \brief Configure la stratégie à adopter par rapport aux frontières du réseau.
    void set_boundary_policy(BoundaryPolicy p)
    { automaton.set_boundary_policy(p); }

    //! \brief Définit l'aplhabet de l'automate de la simulation
    //!
    //! La grille de départ et la grille de l'automate sont vidées
    //! \param A Le nouvel alphabet
    void setAlphabet(const Alphabet&);

    //! \brief Accesseur sur l'alphabet de l'automate de la simulation
    //!
    //! \return L'alphabet de l'automate
    const Alphabet& getAlphabet() const {return automaton.getAlphabet();}



    //! \brief Modificateur de la taille de l'historique
    //!
    //! Vide également l'historique
    void setHistorySize(unsigned int size) {hist = History(size);}

    //! \brief Retourne une référence constante vers l'History actuel.
    const History& getHistory() const
    { return hist; }

    //! \brief Définit la grille
    //!
    //! Si la simulation est lancée, cela ne modifie pas la grille de départ. Cela définit la grille de départ dans le cas contraire.
    //! \param grid La nouvelle grille
    void setGrid(const Grid&);

    //! \brief Retourne la grille actuelle
    //!
    //! \return Grille de l'automate
    const Grid& getGrid() const;
    //void setCell(const Coord&, unsigned int);
    //void resize(size_t,size_t);



    //! \brief Indique si la simulation peut tourner
    //! \return vrai si la simulation peut tourner
    bool runnable() {return canRun;}

    //! \brief Indique si la simulation est gelée
    //! \return vrai si le nouvel état est le même que le dernier
    bool frozen() {return hist.topGrid()==automaton.getGrid();}

    //! \brief Donne la période de la simulation
    //!
    //! Si elle n'est pas encore obtenue : vaut 0
    //! Si elle ne pourra jamais l'être : vaut -1 (s'il y a eu une modification de la grille en cours d'éxécution)
    //! \return période de la simulation
    int getPeriod() {return period;}

    //! \brief Indique l'étape actuelle de la simulation
    //! \return nombre de pas depuis le début
    unsigned int getTime() {return time;}


    //! \brief Remet la simulation dans son état de départ
    void reset();

    //! \brief Fait avancer la simulation d'un pas
    void step();

    //! \brief Fais reculer la simulation d'un pas si possible
    //! \return vrai si la simulation a reculé
    bool back();
};

#endif // SIMULATION_HPP
