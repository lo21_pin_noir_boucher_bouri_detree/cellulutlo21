/**
\file structurewrite.hpp
\date 13/05/2021
\author Yann Boucher
\version 1
\brief StructureWriter

Fichier contenant la classe StructureWrite et ses filles, permettant de sauvegarder une Structure sous différents formats de fichier.

                                                                   **/


#ifndef STRUCTUREWRITER_HPP
#define STRUCTUREWRITER_HPP

#include <string>

#include "structure.hpp"

//! \class StructureWriter
//! \brief Classe abstraite permettant de sauvegarder une Structure sous différents formats de fichier.
class StructureWriter
{
public:
    //! \brief Constructeur instanciant un StructureWriter.
    StructureWriter() = default;
    //! \brief Sauvegarde la structure 's' sous forme d'un std::string.
    //! \param s Structure à sauvegarde
    virtual std::string save_structure(const Structure& s) const = 0;
};

//! \class RLEStructureWriter
//! \brief Sauvegarde une structure au format Extended RLE de Golly (https://www.conwaylife.com/wiki/Run_Length_Encoded)
class RLEStructureWriter : public StructureWriter
{
public:
    virtual std::string save_structure(const Structure& s) const;

private:
    std::string state_to_str(unsigned state) const;
    std::string output_line(Structure::iterator& it, Structure::iterator end) const;
    std::string output_rle_state(Structure::iterator& it, Structure::iterator end) const;
    std::string rle(const std::string& val, unsigned count) const;

private:
    mutable unsigned m_x, m_y;
};

//! \class JSONStructureWriter
//! \brief Sauvegarde une structure au format JSON
class JSONStructureWriter : public StructureWriter
{
public:
    virtual std::string save_structure(const Structure& s) const;
};

#endif // STRUCTUREWRITER_HPP
