#ifndef NEIGHBORHOODDIALOG_HPP
#define NEIGHBORHOODDIALOG_HPP

#include <QDialog>
#include "neighborhood.hpp"
#include "gridview.hpp"

namespace Ui {
    class NeighborhoodDialog;
}

//! \brief Fenêtre de dialogue permettant de choisir les voisins d'une cellule de manière graphique.
class NeighborhoodDialog : public QDialog
{
    Q_OBJECT

    Coord currentPoint;
public:
    explicit NeighborhoodDialog(Neighborhood& n, QWidget *parent = nullptr);
    ~NeighborhoodDialog();
    void ResizeCreateGrid(int x, int y, Neighborhood& n);

public slots:
    Neighborhood* getNeighborhood() const;

protected:
    void done(int r);

private slots:

    void on_validateGridDim_2_clicked();

    void on_heightSpinBox_2_valueChanged(int arg1);

    void on_widthSpinBox_2_valueChanged(int arg1);

private:
    Ui::NeighborhoodDialog *ui;
};


#endif // NEIGHBORHOODDIALOG_HPP
