/**
\file structure.hpp
\date 15/04/2021
\author Yann Boucher
\version 1
\brief Structure

Fichier contenant la classe Structure, représentant un ensemble de cellules constituant une structure.
                                               **/

#ifndef STRUCTURE_HPP
#define STRUCTURE_HPP

#include <functional>
#include <map>
#include <type_traits>
#include <limits>

#include "coord.hpp"

                                           /**
\class Structure
\brief Représente une structure

                                           Cette classe permet de représenter un ensemble de cellules constituant une structure, comme un oscillateur ou un glider.
            **/
        class Structure
{
public:
    //! Titre de la structure
    std::string title;
    //! Description de la structure et de ses particularités
    std::string desc;
    //! Auteur de la structure, si connu
    std::string author;
    //! Date de création de la structure, si connue
    std::string date;
    //! Position de la structure dans la grille, si elle est connue (utilisé pour la sauvegarde de configurations
    Coord top_left;

public:
    //! \brief Constructeur par défaut, représente une structure vide.
    Structure()
    {
        top_left = {0, 0};
    }

    //! \brief Constructeur acceptant deux itérateurs en argument, pointant vers des std::pair de coordonées et de valeur d'état. Appelle 'load<It>(begin, end)'.
    //! \pre It doit pointer vers un std::pair<Coord, int>.
    template <typename It>
    Structure(It begin, It end)
    {
        load(begin, end);
    }

    //! \brief Fonction acceptant deux itérateurs en argument, pointant vers des std::pair de coordonées et de valeur d'état.
    //! \pre It doit pointer vers un std::pair<Coord, int>.
    template <typename It>
    void load(It begin, It end)
    {
        static_assert (std::is_same<typename std::remove_reference<decltype(*begin)>::type, std::pair<Coord, unsigned>>::value ||
                          std::is_same<typename std::remove_reference<decltype(*begin)>::type, std::pair<const Coord, unsigned>>::value,
                      "Iterator value type must be std::pair<Coord, unsigned>");

        int min_x, min_y, max_x, max_y;
        min_x = min_y = std::numeric_limits<int>::max();
        max_x = max_y = std::numeric_limits<int>::min();
        bool empty = true;
        // Première passe pour calculer les positions minimales et maximales
        for (auto it = begin; it != end; ++it)
        {
            // on ignore les entrées avec des cellules zéro, elles sont implicites (cela permet d'économiser pas mal de mémoire)
            if (it->second == 0)
                continue;

            empty = false;

            if (it->first.x > max_x) max_x = it->first.x;
            if (it->first.x < min_x) min_x = it->first.x;
            if (it->first.y > max_y) max_y = it->first.y;
            if (it->first.y < min_y) min_y = it->first.y;
        }

        if (empty)
            top_left = {0, 0};
        else
        {
            top_left.x = min_x; top_left.y = min_y;
        }

        m_width = max_x - min_x + 1;
        m_height = max_y - min_y + 1;

        m_cells.clear();
        // Seconde passe, on ajoute les éléments avec une position normalisée (élément le plus à gauche <=> x=0, élément le plus en haut <=> y=0)
        for (auto it = begin; it != end; ++it)
        {
            if (it->second == 0)
                continue;
            else
                m_cells[it->first - top_left] = it->second;
        }
    }
    //! \brief Fonction vidant la structure, la laissant vide.
    void clear()
    {
        m_cells.clear();
    }

    //! \brief retourne le nombre de cellules présentes dans la structure
    size_t size() const
    { return m_cells.size(); }

    //! \brief retourne la largeur (en cellules) de la structure
    size_t width() const
    { return m_width; }

    //! \brief retourne la hauteur (en cellules) de la structure
    size_t height() const
    { return m_height; }

    //! \class Structure::iterator
    //! \brief iterator représentant un itérateur constant vers le contenu de la structure.
    class iterator
    {
        friend class Structure;
    private:
        iterator(std::map<Coord, unsigned>::const_iterator it)
            : m_it(it) {}

    public:
        // iterator traits
        using difference_type = long;
        using value_type = std::pair<const Coord, unsigned>;
        using pointer = const std::pair<const Coord, unsigned>*;
        using reference = const std::pair<const Coord, unsigned>&;
        using iterator_category = std::forward_iterator_tag;

        iterator& operator++() { ++m_it; return *this;}
        iterator operator++(int) {iterator retval = *this; ++(*this); return retval;}
        bool operator==(iterator other) const {return m_it == other.m_it;}
        bool operator!=(iterator other) const {return !(*this == other);}
        std::pair<Coord, unsigned> operator*() const {return *m_it;}
        pointer operator->() const { return &*m_it;}

    private:
        std::map<Coord, unsigned>::const_iterator m_it;
    };

    //! \brief Retourne un itérateur vers le début de la liste de cellules.
    //! \post Chaque élément itérable est de coordonnée unique.
    iterator begin() const { return iterator(m_cells.begin()); }
    //! \brief Retourne un itérateur vers la fin de la liste de cellules.
    //! \post Chaque élément itérable est de coordonnée unique.
    iterator end() const { return iterator(m_cells.end()); }
private:
    std::map<Coord, unsigned> m_cells;
    size_t m_width = 0, m_height = 0;
};

#endif // STRUCTURE_HPP
