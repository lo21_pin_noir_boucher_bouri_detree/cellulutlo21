/**
\file transitionrule.hpp
\brief TransitionRule

Cette classe représente une règle de transition.

**/

#ifndef TRANSITIONRULE_HPP
#define TRANSITIONRULE_HPP

#include <vector>
#include "coord.hpp"
#include "neighborhood.hpp"
#include "factory.hpp"
#include "property.hpp"

struct NeighborhoodFormat;

//! \brief Représente une règle de transition.
class TransitionRule : public HasUserProperties {
private:


public:
    TransitionRule(){}
    virtual ~TransitionRule(){}

    //! \brief Vérifie que les formats de voisinage proposés sont compatibles
    //! \param Vector des NeighborhoodFormat à vérifier
    //! \return vrai si compatible, faux sinon
    virtual bool acceptFormat(const std::vector<NeighborhoodFormat>&) const =0;

    //! \brief Effectue la transition
    //! \param Etat de la cellule courante
    //! \param Voisinage de la cellule
    //! \return Nouvel état de la cellule
    virtual unsigned int getState(unsigned int, const Neighborhood&) const =0;
};

#endif // TRANSITIONRULE_HPP
