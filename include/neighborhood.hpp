/**
\file neighborhood.hpp
\date 17/04/2021
\author Eugene Pin
\version 1
\brief Neighborhood

Cette classe représente le voisinage d'une cellule.

        **/

#ifndef NEIGHBOURHOOD_HPP
#define NEIGHBOURHOOD_HPP

#include <map>
#include <vector>
#include <utility>
#include <string>
#include <algorithm>
#include <cstring>
#include <cstdint>

#include "coord.hpp"
#include "state.hpp"
#include "constantes.hpp"

    using namespace std;

//! \brief Exception lancée lors d'une erreur de manipulation d'un voisinage.
class NeighborhoodException : public std::exception
{
    std::string _msg;
public:
    NeighborhoodException(const std::string& msg) : _msg(msg){}

    virtual const char* what() const noexcept override
    {
        return _msg.c_str();
    }
};

//! \brief Classe représentant un voisinage, c'est à dire une liste de cellules dont leur état et leur position.
class Neighborhood {
    //! \brief struct nécéssaire pour éviter l'initalisation par zéro coûteuse d'un std::pair
    struct Cell
    {
        int8_t pos_x, pos_y;
        uint8_t state;
    };

    //! \brief Contient le nombre de voisins d'état i : counts[i] contient le nb de voisins d'état i, ou 0 si i >= MAX_ETATS
    unsigned char counts[MAX_ETATS];
    //! \brief Nombre de voisins
    unsigned int neighbors;
    mutable bool sorted;
    //! \brief Tableau contenant la liste des voisins
    mutable Cell neighborPositions[MAX_VOISINS];

public:
    //! \brief Initialisation du Neighborhood
    Neighborhood()
    {
        memset(counts, 0, MAX_ETATS*sizeof(unsigned char));
        neighbors = 0;
        sorted = false;
    }

    //! \brief Initialisation par copie
    Neighborhood(const Neighborhood& other)
    {
        *this = other;
    }
    //! \brief Affectation par copie
    Neighborhood& operator=(const Neighborhood& other)
    {
        memcpy(counts, other.counts, MAX_ETATS*sizeof(unsigned char));
        memcpy(neighborPositions, other.neighborPositions, other.neighbors*sizeof(Cell));
        neighbors = other.neighbors;
        sorted = other.sorted;
        return *this;
    }

    //! \brief Retourne le nombre de voisin ayant l'état définit en paramètre
    //! \param State : Etat des voisins à rechercher
    //! \return Le nombre de voisin
    unsigned int getNb(unsigned int s) const
    {
        if (s >= MAX_ETATS)
            return 0;
        else
            return counts[s];
    }


    //! \brief Ajoute un voisin au vecteur des voisins. Prend une coordonnée et un état en paramètre
    //! \param Coord, State : coordonée relative et état du voisin
    //! \pre La coord ne doit pas déjà exister
    void addNeighbor(Coord c, unsigned char s) {
        if (s >= MAX_ETATS)
            throw NeighborhoodException("Invalid state value (>= MAX_ETATS)");

        sorted = false;
        neighborPositions[neighbors++] = {(int8_t)c.x, (int8_t)c.y, s};
        ++counts[s];
    }

    //! \brief Retourne une copie du voisinage, miroir selon l'axe vertical.
    Neighborhood flip_vertically() const;
    //! \brief Retourne une copie du voisinage, miroir selon l'axe horizontal.
    Neighborhood flip_horizontally() const;
    //! \brief Retourne une copie du voisinage, tournée de 90° dans le sens des aiguilles d'une montre.
    Neighborhood rotate90() const;

    //! \brief Retourne le nombre de voisins dans le Neighborhood.
    unsigned size() const
    { return neighbors; }

    //! \brief Retourne la cellule numéro i (index allant de 0 à size()-1, permettant d'indexer les cellules indépendamment de leur position
    pair<Coord, unsigned char> neighbor_at_index(unsigned i) const
    {
        if (i >= size())
            throw NeighborhoodException("Invalid index");
        if (!sorted)
        {
            std::sort(std::begin(neighborPositions), std::begin(neighborPositions) + neighbors,
                      [](const Cell& lhs, const Cell& rhs)
                      {
                          return Coord{(int)lhs.pos_x, (int)lhs.pos_y} < Coord{(int)rhs.pos_x, (int)rhs.pos_y};
                      });
            sorted = true;
        }
        return {Coord{(int)neighborPositions[i].pos_x,(int)neighborPositions[i].pos_y}, neighborPositions[i].state};
    }
};

#endif // NEIGHBOURHOOD_HPP
