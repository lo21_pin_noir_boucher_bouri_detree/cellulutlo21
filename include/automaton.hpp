/**
\file automaton.hpp
\brief Automaton

Cette classe représente un automate cellulaire.
**/

#ifndef AUTOMATON_HPP
#define AUTOMATON_HPP

#include "neighborhoodrule.hpp"
#include "transitionrule.hpp"
#include "grid.hpp"
#include "alphabet.hpp"

//! \brief Représente une configuration d'un automate à un instant t : ses règles de voisinage et transition et son réseau.
class Automaton {
private:
    Alphabet alphabet;
    Grid grid;
    BoundaryPolicy border_policy;
    NeighborhoodRule* neighbourhoodRule;
    TransitionRule* transitionRule;

public:
    Automaton();
    virtual ~Automaton();

    //! \brief Définit l'aplhabet de l'automate
    //!
    //! Remet à 0 toutes les cases de la grille
    //! \param A le nouvel alphabet
    void setAlphabet(const Alphabet&);

    //! \brief Accesseur sur l'alphabet de l'automate
    //!
    //! \return Référence constante sur l'alphabet
    const Alphabet& getAlphabet() const {return alphabet;}

    //! \brief Configure la stratégie à adopter par rapport aux frontières du réseau.
    void set_boundary_policy(BoundaryPolicy p)
    { border_policy = p; }

    //! \brief Définit la règle de voisinage
    //!
    //! Ne crée pas de copie de la règle passée en paramètre. L'automate se charge de sa destruction.
    //! \param NR L'adresse de la nouvelle règle de voisinage
    void setNeighborhoodRule(NeighborhoodRule*);

    //! \brief Accesseur sur l'adresse de la règle de voisinage
    //!
    //! \return Adresse de la règle de voisinage
    NeighborhoodRule* getNeighborhoodRule() const;



    //! \brief Définit la règle de transition
    //!
    //! Ne crée pas de copie de la règle passée en paramètre. L'automate se charge de sa destruction.
    //! \param TR L'adresse de la nouvelle règle de transition
    void setTransitionRule(TransitionRule*);

    //! \brief Retourne l'adresse de la règle de transition
    //!
    //! \return Adresse de la règle de transition
    TransitionRule* getTransitionRule() const;



    //! \brief Permet de définir la valeur d'une cellule
    //!
    //! Si la valeur est trop importante, l'automate effectue un modulo selon la taille de l'alphabet
    //! \param coord Les coordonnées de la cellule à définir
    //! \param val La valeur que doit prendre la cellule
    void setCell(const Coord&, unsigned int);

    //! \brief Accesseur sur la grille de l'automate
    //!
    //! \return Grille de l'automate
    const Grid& getGrid() const;

    //! \brief Définit la grille
    //!
    //! \param G la nouvelle grille
    void setGrid(const Grid&);



    //! \brief Fait avancer l'automate
    void runOnce();
};

#endif // AUTOMATON_HPP
