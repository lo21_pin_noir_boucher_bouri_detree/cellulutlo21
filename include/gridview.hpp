/**
\file gridview.hpp
\date 28/04/2021
\author Yann Boucher
\version 1
\brief GridView

Cette classe représente le widget utilisé pour l'affichage et l'interaction avec la grille actuelle.
**/

#ifndef GRIDVIEW_HPP
#define GRIDVIEW_HPP

#include <map>
#include <set>

#include <QFrame>
#include <QGraphicsView>
#include <QLabel>

#include <QGraphicsRectItem>

#include "coord.hpp"
#include "structure.hpp"
#include "alphabet.hpp"

#include "grid.hpp"
#include "history.hpp"

namespace detail
{
//! \class Graphics_view_zoom
//! Classe permettant l'implémentation d'un zoom au travers d'un event filter.
//! Détail d'implémentation.
//! based on https://stackoverflow.com/questions/19113532/qgraphicsview-zooming-in-and-out-under-mouse-position-using-mouse-wheel
class Graphics_view_zoom : public QObject {
    Q_OBJECT
public:
    Graphics_view_zoom(QGraphicsView* view);
    void gentle_zoom(double factor);
    void set_zoom_factor_base(double value);

private:
    QGraphicsView* _view;
    double _zoom_factor_base;
    QPointF target_scene_pos, target_viewport_pos;
    bool eventFilter(QObject* object, QEvent* event);

signals:
    void zoomed();
};
}

class GridView;
class GridGraphicsView;
class DragDropHandlerItem;

/**
 * \class GridView
 * \brief Widget affichant la grille en cours
 *
 * Cette classe, hérité de QFrame, représente l'état de la grille à l'instant actuel. Il affiche les différentes cellules avec la couleur choisie par l'alphabet,
 * permet l'édition, la suppression, le copier-couper-coller, et le glisser-déposer de structures depuis d'autres widget.
 */
class GridView : public QFrame
{
    Q_OBJECT

    friend class GridGraphicsView;
public:
    //! \brief Initialise le GridView comme n'importe quel autre QWidget avec son parent.
    GridView(QWidget *parent = nullptr);
public:
    //! \brief Associe l'alphabet alph à l'objet GridView afin d'afficher les bonnes couleurs et étiquettes.
    //! \param alph L'alphabet à associer.
    void set_alphabet(const Alphabet& alph);
    //! \brief Retourne l'alphabet actuel utilisé par le GridView.
    //! \returns L'alphabet actuellement utilisé
    const Alphabet& alphabet() const;

    //! \brief Paramètre l'état à assigner aux cellules lors de l'édition (le 'stylo' actuel)
    //! \param state L'état à assigner à chaque cellule éditée.
    void set_current_pen(unsigned state);
    //! \brief Retoure l'état actuel du stylo.
    //! \returns L'état actuel de l'édition.
    unsigned current_pen() const;

    //! \brief Active le mode "toggle", qui permet de changer l'état d'une cellule de manière cyclique en cliquant dessus, passant d'un état check à uncheck, et inversement si cliqué à nouveau.
    //! \param checked_state L'état lorsqu'une cellule est sélectionnée
    //! \param unchecked_state L'état d'une cellule non sélectionnée
    void enable_toggle_mode(unsigned checked_state, unsigned unchecked_state = 0);
    //! \brief Ajoute une règle au mode "toggle", qui permet de changer l'état d'une cellule de manière cyclique en cliquant dessus, passant d'un état check à uncheck, et inversement si cliqué à nouveau.
    //! \brief Cette fonction permet d'ajouter une nouvellle paire d'états cycliques, pour pouvoir par exemple permettre de passer entre 0-1 et 2-3, indépendamment.
    //! \param checked_state L'état d'une cellule sélectionnée
    //! \param unchecked_state L'état d'une cellule non sélectionnée
    void add_toggle_rule(unsigned checked_state, unsigned unchecked_state = 0);
    //! \brief Désactive le mode "toggle".
    void disable_toggle_mode();
    //! \brief Retourne un booléen indiquant si le mode actuel est le mode "toggle".
    bool in_toggle_mode() const
    { return m_in_toggle_mode; }

    //! \brief Insère une structure à une position donnée.
    //! \param origin La coordonnée où placer la structure.
    //! \param s La structure à placer.
    void paste_structure_at(Coord origin, const Structure& s);

    //! \brief Utilise une structure passée en argument comme structure à coller lors d'une opération de collage, par exemple avec Ctrl+V.
    //! \param s La structure désirée.
    void set_clipboard(const Structure& s);

    //! \brief Retourne la taille du côté en pixels d'une cellule au niveau de zoom actuel.
    //! \returns Taille en pixel du côté d'une cellule.
    unsigned cell_screen_size() const;

    //! \brief Retourne la taille de la grille, dans un QSize.
    //! \returns La taille de la Grid.
    QSize grid_size() const;

    //! \brief Charge la Grid grid dans le GridView afin de l'afficher et de pouvoir l'éditer.
    //! \param grid La Grid à copier vers l'affichage du GridView.
    void copy_grid(const Grid& grid);
    //! \brief Retourne une Grid représentant l'état actuel affiché par la GridView.
    //! \returns Une référence constante vers une Grid correspondante.
    const Grid &get_grid() const;

    //! \brief Réinitialise la sélection.
    void clear_selection();
    //! \brief Remplit les cellules sélectionnées avec un état choisi.
    //! \param state L'état à assigner.
    void fill_selection(unsigned state);
    //! \brief Retourne une structure représentant la sélection actuelle.
    //! \returns Une structure représentant la sélection actuelle.
    Structure selected_cells() const;

    //! \brief Sauvegarde l'état de la grille avant une opération de modification, pour pouvoir Ctrl-Z.
    void push_history();

    //! \brief Annule la dernière opération (effectue un Ctrl-Z).
    void undo();

    //! \brief Retourne une QImage représentant la grille actuelle.
    const QImage &grid_image() const;

    //! \brief Place le GridView en plein écran
    void enter_fullscreen();

signals:
    //! \brief Signal émis quand le zoom change.
    //! \param cell_size la nouvelle taille à l'écran en pixels d'une cellule
    void zoom_changed(unsigned cell_size);

    //! \brief Signal émis lorsque l'utilisateur désire quitter le mode plein écran.
    void exit_fullscreen();

private:
    void load_grid(const Grid& grid);

    void set_cell_state(Coord pos, unsigned state);
    void update_gridview();

    QGraphicsRectItem* create_selection_rect(const QRectF &rect = QRectF());
    void copy_selection();
    void paste_clipboard();
    void delete_selection();
    void select_all();

    void click_on(Coord coord);
    void update_current_mouse_pos(Coord coord);

private slots:
    void handle_rubberband(QRect, QPointF, QPointF);

protected:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

private:
    GridGraphicsView* m_view;
    QGraphicsScene* m_scene;
    detail::Graphics_view_zoom* m_zoom;
    History m_undo_history;
    std::vector<uint32_t> m_image_data;
    QImage m_grid_image;
    QWidget* m_info_section;
    QLabel* m_mouse_pos_label;
    Grid m_grid;
    Coord m_last_mouse_pos;
    bool m_in_toggle_mode;
    std::vector<std::pair<unsigned, unsigned>> m_toggle_states;
    std::vector<QGraphicsRectItem*> m_selection_rects;
    bool m_handling_rubberband;
    DragDropHandlerItem* m_drag_drop_handler;

    unsigned m_width;
    unsigned m_height;
    unsigned m_pen;
    Structure m_copy_paste_buffer;
    Alphabet m_alph;
};

#endif // GRIDVIEW_HPP
