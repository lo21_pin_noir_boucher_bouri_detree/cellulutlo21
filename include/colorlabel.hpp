#ifndef COLORLABEL_H
#define COLORLABEL_H

#include <QDialog>
#include "state.hpp"
#include "stateColor.hpp"
#include "alphabet.hpp"
#include <QListWidget>

namespace Ui {
class ColorLabel;
}

//! \brief Classe permettant l'édition des états de l'alphabet.
class ColorLabel : public QDialog
{
    Q_OBJECT

public:
    explicit ColorLabel(const Alphabet& a, QWidget *parent = nullptr);
    ~ColorLabel();

public:
    //! \brief Retourne l'alphabet tel qu'édité par l'utilisateur.
    Alphabet getAlphabet() const
    { return a; }

private slots:
    void generateState(); // Génère notre état en cliquant sur le bouton "Generate"
    void randomGenerator(); // Génère un état aléatoire
    void daltonianMode(); // Active le Mode Daltonien : Génère autant d'états que spécifié dans l'interface de façon adaptée aux Daltoniens
    void resetMode(); // La fonction resetMode permet de remettre la liste et l'alphabet "à zéro" (on garde tout de même l'état par défaut)
    void removeState(); // La fonction removeState permet de retirer le dernier état de l'alphabet
    void loadStateID(unsigned int); // La fonction idPath permet de parcourir la liste des états et d'avoir un affiche du nom et de la couleur sur l'interface
    void pickColor();

    void on_state_label_textEdited(const QString &arg1);

private:
    Ui::ColorLabel *ui;
    Alphabet a;
    unsigned current_id;
};

#endif // COLORLABEL_H
