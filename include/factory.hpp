/**
\file structure.hpp
\date 18/04/2021
\author Yann Boucher
\version 1
\brief Factory

Fichier contenant la class singleton template Factory, à qui on peut associer diverses classes constructibles à partir d'un nom sous forme de chaîne de caractères.
**/


#ifndef FACTORY_HPP
#define FACTORY_HPP

#include <unordered_map>
#include <memory>
#include <vector>
#include <type_traits>

namespace detail
{
template <typename _Base, typename Child>
bool register_to_factory(const char* name);
}

/**
\class Factory
\brief Représente une fabrique fabriquant des objets de classe mère Base

Cette classe permet de lister l'ensemble des objects constructibles par cette dernière, ainsi que la construction d'un objet à partir d'un nom.
**/
template <typename Base>
class Factory
{
    template <typename _Base, typename Child>
    friend bool detail::register_to_factory(const char* name);

public:
    //! \brief Retourne un objet créé à partir de choice
    //!
    //! Fabrique un objet du type représenté par choice.
    //! \param choice Le nom de la classe à fabriquer, tel que présent dans la liste retournée par list_choices().
    //! \return Un unique_pointer de type Base pointant vers l'objet construit, ou nullptr si choice est invalide.
    static std::unique_ptr<Base> make(const std::string& choice)
    {
        if (!get().child_register.count(choice))
            return nullptr;

        return get().child_register.at(choice)();
    }

    //! \brief Retourne l'ensemble des choix possibles.
    //!
    //! Retourne dans un std::vector l'ensemble des objets fabriquables par la Factory selon le nom auquel ils ont été enregistrés.
    //! Pour créer un objet d'un type donné, il suffit de passer le nom tel que retourné par list_choices() à make().
    //! \return La liste des noms d'objets fabriquables sous forme d'un std::vector.
    static std::vector<std::string> list_choices()
    {
        std::vector<std::string> labels;
        for (const auto& pair : get().child_register)
            labels.emplace_back(pair.first);
        return labels;
    }

private:
    static Factory<Base>& get()
    {
        static Factory<Base> instance;
        return instance;
    }

    template <typename Child>
    bool register_child(const char* name)
    {
        child_register[name] = []() -> std::unique_ptr<Base>{ return std::make_unique<Child>(); };
        return true;
    }

private:
    std::unordered_map<std::string,  std::unique_ptr<Base>(*)()> child_register;
};

//! \brief Ajoute la classe child à la liste des objets fabriquables par Factory<base>.
//!
//! \param base La classe de base de child.
//! \param child Le type de classe dérivée de base que la fabrique devra pouvoir construire.
//! Ajoute la classe child à la liste des objets fabriquables par Factory<base>.
//! Le nom de la classe tel que considéré par factory est obtenu par un appel à Child::name(), il est donc nécéssaire que l'enfant possède une méthode statique de signature 'static std::string name()'.
#define REGISTER_FACTORY_ENTRY(base, child, name) \
namespace detail { \
/* nécéssaire pour que la transition soit enregistrée parmis la liste de classes de transitions*/ \
/* variable globale static permettant seulement de forcer l'enregistrement d'une telle classe dans la factory */ \
bool const fact_##base##_##child##_registered = register_to_factory<base, child>(name); \
}

//! \internal namespace contenant les détails d'implémentation de Factory
namespace detail
{
template <typename Base, typename Child>
inline bool register_to_factory(const char* name)
{
    static_assert (std::is_base_of<Base, Child>::value, "Le type Child doit être dérivé de Base");
    return Factory<Base>::get().template register_child<Child>(name);
}
}


#endif // FACTORY_HPP
