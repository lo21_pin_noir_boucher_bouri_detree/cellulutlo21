/**
\file state.hpp
\date 01/05/2021
\author Arthur Detree
\version 1
\brief state

Représente les données graphiques associées à un état d'un alphabet.
        **/

#ifndef STATE_H
#define STATE_H

#include <string>
#include "stateColor.hpp"

//! \brief Représente les données cosmétiques associées à un état.
class state{
    //! \brief Couleur de l'état --> C'est un objet stateColor dont les attributs sont rouge, vert, bleu
    stateColor color;
    //! \brief Nom de l'état
    std::string label;

public:
    //! \brief Constructeur de l'état
    state(stateColor c, const std::string& l = "unnamed"):color(c),label(l){}

    //! \brief Accesseur en lecture de la couleur
    const stateColor& getColor() const { return color; }
    //! \brief Accesseur en lecture du label
    std::string getStateLabel() const { return label; }

    //! \brief Accesseur en écriture de la couleur
    void setColor(stateColor c){ color=c; }
    //! \brief Accesseur en écriture du label
    void setStateLabel(const std::string& l){ label=l; }
};

#endif // STATE_H
