/**
\file propertyvisitors.hpp
\date 17/04/2021
\author Yann Boucher
\version 1
\brief Classes filles de PropertyVisitor

Fichier contenant diverses classes filles de PropertyVisitor permettant l'affichage sur l'interface, le chargement et la sauvegarder en JSON d'objets Property.
**/


#ifndef VISITORS_HPP
#define VISITORS_HPP

#include <stack>
#include <QVariant>

#include "property.hpp"
#include "neighborhoodDialog.hpp"

//! \brief Exception lancée lors d'une erreur liée aux visiteurs de propriétés.
class PropertyVisitorException : public std::exception
{
    std::string _msg;
public:
    PropertyVisitorException(const std::string& msg) : _msg(msg){}

    virtual const char* what() const noexcept override
    {
        return _msg.c_str();
    }
};

/**
\class UIBuilderVisitor

\brief Cette classe permet de remplir un widget fourni en argument avec une représentation interactive de Property.
**/


class UIBuilderVisitor : public PropertyVisitor
{
public:
    //! Construit le UIBuilderVisitor à partir d'un base_widget.
    //! \param base_widget le widget.
    //! \param destructive indique si l'on détruit le contenu déjà présent dans base_widget ou non.
    UIBuilderVisitor(QWidget* base_widget, bool destructive = true);

private:
    void add_widget(const std::string& prop_name, QWidget* ptr);
    void clear_layout(QLayout *layout);
    QWidget* current_widget();
    void push_array_widget(const Property& prop);
    QWidget *pop_widget();

private:
    void visit(StringProperty& str);
    void visit(IntegerProperty& prop);
    void visit(CoordinateProperty& prop);
    void visit(PropertyList& list);
    void visit(HelpProperty&);

private:
    std::stack<QWidget*> m_widget_hierarchy;
};

/**
\class PropertySaverVisitor

\brief Cette classe permet de sauvegarder un ensemble de Property dans un object JSON donné.
**/
class PropertySaverVisitor : public PropertyVisitor
{
public:
    //! Construit le  PropertySaverVisitor.
    PropertySaverVisitor();
    //! Sauvegarde les données des Property visitées au format JSON dans un QJsonObject.
    QJsonObject save();

private:
    QVariant& current();
    void save_value(Property& prop, const QJsonValue& val);
    void push_object();
    void push_array();
    QJsonValue pop_value();

    void visit(StringProperty& prop);
    void visit(IntegerProperty& prop);
    void visit(CoordinateProperty& prop);
    void visit(PropertyList& list);
    void visit(HelpProperty&) {}

private:
    std::stack<QVariant> m_current_hierarchy;
};

/**
\class PropertyLoaderVisitor

\brief Cette classe permet de charger un ensemble de Property depuis un pbject JSON donné.
**/
class PropertyLoaderVisitor : public PropertyVisitor
{
public:
    //! Construit le PropertyLoaderVisitor à partir d'un QJsonObject.
    PropertyLoaderVisitor(const QJsonObject& root);

private:
    QJsonValue& current();
    void enter_value(const QJsonValue& value);
    QJsonValue exit_value();
    QJsonValue fetch_value(Property& prop);

    void visit(StringProperty& prop);
    void visit(IntegerProperty& prop);
    void visit(CoordinateProperty& prop);
    void visit(PropertyList& prop);
    void visit(HelpProperty&) {}

private:
    std::stack<QJsonValue> m_current_hierarchy;
};


#endif // VISITORS_HPP
