/**
\file stateColor.hpp
\date 01/05/2021
\author Arthur Detree
\version 1
\brief stateColor

Représente la couleur associée à un état.
**/


#ifndef STATECOLOR_H
#define STATECOLOR_H

//! \brief Représente la couleur d'un état.
class stateColor{ // Chaque objet stateColor (couleur) est décrit par trois attributs (RGB)
    unsigned char redColor;
    unsigned char greenColor;
    unsigned char blueColor;

public:
    //! Constructeur d'un objet stateColor prenant en compte 3 arguments (RGB)
    stateColor(unsigned char r, unsigned char g, unsigned char b):redColor(r),greenColor(g),blueColor(b){}

    //! Méthode de redéfinition d'une couleur prenant en compte 3 arguments (RGB)
    void setColor(unsigned char r, unsigned char g, unsigned char b)
    {redColor = r; greenColor = g; blueColor = b;}

    //! Accesseur en lecture de la couleur Rouge
    unsigned char getRed() const{ return redColor; }
    //! Accesseur en lecture de la couleur Verte
    unsigned char getGreen() const{ return greenColor; }
    //! Accesseur en lecture de la couleur Bleue
    unsigned char getBlue() const{ return blueColor; }
};

#endif // STATECOLOR_H
