#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>


#include <QGraphicsScene>
#include <QFileDialog>

#include "property.hpp"
#include "simulation.hpp"

class Structure;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

//! \brief Fenêtre principale de l'application.
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    //! \brief Connection entre le slider et le spinBox pour la vitesse d'exectution de la simulation
    void on_simSpeedSpinbox_valueChanged(int arg1);

    //! \brief Connection entre le slider et le spinBox pour la vitesse d'exectution de la simulation
    void on_simSpeedSlider_valueChanged(int value);

    //! \brief Ouverture d'un pattern depuis le navigateur de fichier
    void on_openPatternButton_clicked();

    //! \brief Ouverture d'une règle depuis le navigateur de fichier
    void on_openRuleButton_clicked();

    //! \brief Envoie un signal lors du changement de voisinage.
    void on_neighborhood_list_currentTextChanged(const QString &);

    //! \brief Envoie le signal que la dimension de la grille a été modifié
    void on_validateGridDim_clicked();

    //! \brief Active le bouton pour valider la dimension de la grille lors d'une modification dans le Line Edit
    void on_widthSpinBox_valueChanged(int arg1);

    //! \brief Active le bouton pour valider la dimension de la grille lors d'une modification dans le Line Edit
    void on_heightSpinBox_valueChanged(int arg1);

    //! \brief Affiche l'interface pour sauvegarder la sélection actuelle comme une structure
    //! \returns Le nom du fichier choisi
    QString afficher_interface_sauvegarde_structure();

    //! \brief Sauvegarde la grille actuelle en tant qu'image.
    void save_as_image();

    //! \brief Sauvegarde l'historique d'exécution en tant que GIF animé.
    void save_as_gif();

    void on_nbrStateComboBox_currentTextChanged(const QString &arg1);

    //! \brief Créer une nouvelle grille ayant des états aléatoire
    void on_randomPatternButton_clicked();

    //! \brief Applique l'état sélectionné aux cellules sélectionnées de la GridView
    void on_drawCellButton_clicked();

    //! \brief Copie la structure choisie dans la bibliothèque dans le presse-papier du GridView
    void copy_structure_clicked(const Structure& s);

    //! \brief Sauvegarde des Règles de transitions
    void on_saveRuleButton_clicked();

    //! \brief Bouton pour paramétrer la règle de transition et les voisins
    void on_customize_button_clicked();

    //! \brief Sauvegarde l'état du pattern
    void on_savePatternButton_clicked();

    //! \brief Passe à l'itération suivante de la simulation
    void on_nextButton_clicked();

    //! \brief Passe à l'itération suivante de la simulation
    void on_prevButton_clicked();

    //! \brief Lecture/Pause de la simulation
    void on_playPauseButton_clicked();

    //! \brief Réinitialise la simulation à la grille initiale
    void on_resetButton_clicked();

    //! \brief Change la profondeur de l'historique de simulation
    void on_recordSpinBox_valueChanged(int arg1);

    //! \brief Place le GridView en plein écran
    void enter_fullscreen();

    //! \brief Quitte le mode plein écran
    void exit_fullscreen();

    //! \brief Bouton permettant de réintialiser la grille à zéro
    void on_pushButton_clicked();

    //! \brief Mets à jour la vitesse de simulation.
    void on_simSpeedSlider_sliderMoved(int position);

    //! \brief Mets à jour la stratégie de frontière.
    void load_boundary_policy(int index);

private:
    //! \brief Initialiser la liste des transitions et voisinages disponibles
    void init_transition_neighborhood_list();

    //! \brief Mets à jour le widget contenant les paramètres pour la règle de transition choisie
    void update_transition_settings();

    //! \brief Mets à jour le widget contenant les paramètres pour la règle de voisinage choisie
    void update_neighborhood_settings();

    //! \brief Configure pour activer la customisation d'une règle actuelle
    void enable_rule_customization();

    //! \brief Configure pour désactiver la customisation d'une règle actuelle
    void disable_rule_customization();

    //! \brief Charge un modèle d'automate à partir d'un objet JSON
    void load_model(const QJsonObject& obj);

    //! \brief Déclenche la confirmation et la sauvegarde du modèle édité actuel
    void save_model();

    //! \brief Charge la grille depuis une image.
    void load_from_image();

    //! \brief Charge un nouvel alphabet et met à jour l'UI.
    void ui_update_alphabet(const Alphabet& alph);

    //! \brief Essaie de charger l'état de l'application si il a été sauvegardé.
    //! \returns True si le chargement a été réussi, false sinon.
    bool try_load_saved_state();

    //! \brief Sauvegarde la configuration actuelle.
    void save_grid_configuration();

    //! \brief Charge une configuration liée au modèle donné.
    void load_grid_configuration(const QJsonObject &configuration);

    //! \brief Retourne le modèle par défaut à charger
    QJsonObject default_model() const;

    //! \brief Retourne la configuration par défaut à charger
    QJsonObject default_configuration() const;

    //! \brief Fonction secrète !
    void play_bad_apple();

    //! \brief Surprise aussi...
    void play_snake();

protected:
    void closeEvent(QCloseEvent* e) override;
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

private:
    Ui::MainWindow *ui;
    Simulation simulation;
    TransitionRule* m_transition_rule;
    NeighborhoodRule* m_neighborhood_rule;
    QJsonObject m_loaded_model;
    QTimer* timer;
    bool m_arrow_key_state[4];
    bool m_customizing;
};
#endif // MAINWINDOW_HPP
