#ifndef SAVINGDIALOG_H
#define SAVINGDIALOG_H

#include <QDialog>

namespace Ui {
class SavingDialog;
}

//! \brief Fenêtre de dialogue permettant d'obtenir des informations d'enregistrement comme l'auteur, un titre ou une date.
class SavingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SavingDialog(QWidget *parent = nullptr);
    ~SavingDialog();

    //! \brief Retourne l'auteur choisi.
    QString auteur() const;
    //! \brief Retourne le titre choisi.
    QString titre() const;
    //! \brief Retourne la description choisie.
    QString desc() const;
    //! \brief Retourne la date choisie.
    QDate date() const;

protected:
    void done(int r);

private:
    Ui::SavingDialog *ui;
};

#endif // SAVINGDIALOG_H
