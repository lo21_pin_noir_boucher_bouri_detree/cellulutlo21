/**
\file history.h
\date 24/04/2021
\author Merwane Bouri
\version 1
\brief History

Cette classe représente un historique de Grids.

        **/

#ifndef HISTORY_H
#define HISTORY_H

#include "grid.hpp"
#include <string>
#include <deque>
#include <exception>

//! \brief Exception lancée lors d'une erreur de manipulation de l'historique.
class HistoryException : public std::exception
{
    std::string _msg;
public:
    HistoryException(const std::string& msg) : _msg(msg){}

    virtual const char* what() const noexcept override
    {
        return _msg.c_str();
    }
};

/**
\struct History
\brief Représente l'historique des grilles.

Cette structure représente l'historique des grilles.
**/

class History{
    unsigned int nbMax;
    std::deque<Grid> tab;
public:
    //! \brief Constructeur par défaut, génère un historique capable de stocker un nombre max de grilles.
    History(unsigned int nbM);

    //! \brief Accesseur sur le nombre max
    //! \return Retourne le nombre de lignes de la Grille
    unsigned int get_nbMax()const{return nbMax;}

    //! \brief Retourne le nombre de grilles contenues dans l'historique.
    unsigned int size() const
    { return tab.size(); }

    //! \brief Accède à la grille d'indice i.
    Grid at(unsigned int i) const
    {
        if (i >= size())
            throw HistoryException("The stack is empty.");
        return tab[i];
    }

    //! \brief Ajoute une grille dans l'historique
    void pushGrid(const Grid& g);

    //! \brief Fonction qui retire la dernière grille de l'historique, et la renvoie
    //! \return Retourne la dernière grille ajoutée de l'historique
    Grid popGrid();

    //! \brief Fonction qui retourne la dernière grille sans la retirer
    //! \return Retourne la dernière grille ajoutée de l'historique
    Grid topGrid()
    {
        if(tab.size()>0)
            return tab.back();
        throw HistoryException("The stack is empty.");
    }
    //! \brief Fonction qui vérifie si l'historique est vide ou pas
    //! \return Retourne un booléen selon si le prédicat "l'historique est vide"
    bool isEmpty()
    {
        return tab.empty();
    }

};

#endif // HISTORY_H
