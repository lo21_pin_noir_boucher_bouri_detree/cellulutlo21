#ifndef CONFIGURATIONLOADINGDIALOG_HPP
#define CONFIGURATIONLOADINGDIALOG_HPP

#include <QDialog>

#include <QDir>
#include <QTreeWidgetItem>
#include <QFileSystemWatcher>
#include <QJsonObject>

namespace Ui {
class ConfigurationLoadingDialog;
}

//! \brief Exception lancée lors d'une erreur de chargement de configuration.
class ConfigurationLoadingException : public std::exception
{
    std::string _msg;
public:
    ConfigurationLoadingException(const std::string& msg) : _msg(msg){}

    virtual const char* what() const noexcept override
    {
        return _msg.c_str();
    }
};

//! \brief Fenêtre de dialogue permettant de charger une configuration.
class ConfigurationLoadingDialog : public QDialog
{
    Q_OBJECT

public:
    //! Construit le ConfigurationLoadingDialog à partir d'un parent et du nom du modèle actuel.
    explicit ConfigurationLoadingDialog(const QString& current_model, QWidget *parent = nullptr);
    ~ConfigurationLoadingDialog();

    //! \brief Retourne la configuration choisie en tant qu'objet JSON.
    QJsonObject configuration() const;

protected:
    void done(int r);

private:
    QJsonObject load_configuration(const QString& filename);
    void load_configurations();
    QTreeWidgetItem *add_directory_contents(const QDir& dir);

private slots:
    void update_info(QTreeWidgetItem* item, int);
    void on_include_other_models_stateChanged(int);

private:
    Ui::ConfigurationLoadingDialog *ui;
    QFileSystemWatcher m_watcher;
    QJsonObject m_current_configuration;
    QString m_current_model;
};

#endif // CONFIGURATIONLOADINGDIALOG_HPP
