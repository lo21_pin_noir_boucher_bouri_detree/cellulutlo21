/**
\file mathexpr.hpp
\date 25/05/2021
\author Yann Boucher
\version 1
\brief MathExpr

Ce fichier contient une fonction permettant d'exécuter une expression mathématique à partir d'une chaîne de caractères et d'une liste de variables.
        **/

#ifndef MATHEXPR_HPP
#define MATHEXPR_HPP

#include <map>
#include <exception>
#include <vector>
#include <climits>

namespace detail
{
struct rpl_token_t
{
    enum : uint8_t
    {
        LITERAL,
        VARIABLE,
        OP
    } type;
    union
    {
        int8_t value;
        uint8_t var_alphabet_idx;
        char op;
    };
};
}

//! \brief Exception lancée lors de l'évaluation d'une expression mathématique.
class MathExprException : public std::exception
{
public:
    MathExprException(const std::string& str)
        : m_what(str)
    {}

    const char * what() const noexcept override
    { return m_what.c_str(); }

private:
    std::string m_what;
};

//! \brief Représente une expression mathématique parsée et mise en cache.
class MathExpr
{
public:
    //! \brief Initialise l'expression à partir d'une chaîne.
    MathExpr(const std::string& expr = "0");

    //! \brief Retourne une évaluation de l'expression mathématique.
    int eval() const;

    //! \brief Permet de spécifier la valeur d'une variable.
    //! La constance symbolise ici l'immutabilité de l'expression mathématique en elle-même, pas son contexte d'évaluation.
    void set_var(char var, int val) const
    {
        if (var < 'a' || var > 'z')
            throw MathExprException("Invalid variable name");
        variable_map[var - 'a'] = val;
    }

private:
    std::vector<detail::rpl_token_t> rpl_stack;
    mutable std::array<int8_t, 26> variable_map;
    bool is_constant; // true if the expression contains no variables
    int cached_result;
};

//! Evalue une expression mathématique selon une liste de variables fournies, et retourne un entier.
//! Les opérateurs supportés sont les parenthèses, +, -, *, /, %.
//! \param expr L'expression mathématique, en tant que std::string
//! \param variables un std::map associant à chaque nom de variable une valeur entière
//! \returns La valeur de résultat de l'expression mathématique évaluée.
int eval_math(const std::string& expr, const std::map<char, int> &variables = {});

#endif // MATHEXPR_HPP
